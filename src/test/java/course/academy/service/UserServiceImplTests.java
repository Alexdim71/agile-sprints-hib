package course.academy.service;

import static course.academy.Helpers.*;

import course.academy.dao.RoleRepository;
import course.academy.dao.StatusUserRepository;
import course.academy.dao.UserRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Role;
import course.academy.model.User;
import course.academy.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;


@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository userRepository;
    @Mock
    RoleRepository roleRepository;
    @Mock
    StatusUserRepository statusUserRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    void getAll_should_callRepository() {
        //Arrange
        Mockito.when(userRepository.getAll()).thenReturn(new ArrayList<>());

        //Act
        userService.getAll();

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).getAll();

    }

    @Test
    void getById_should_returnUser_when_userExists() {

        //Arrange
        User mockUser = createMockDeveloper();
        Mockito.when(userRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        //Act
        User result = userService.getById(mockUser.getId());

        //Assert
        Assertions.assertEquals(result, mockUser);
    }

    @Test
    void findByUserName_should_returnUser_whenUsernameExists() {
        User mockUser = createMockDeveloper();
        Mockito.when(userRepository.getByField("username", mockUser.getUsername()))
                .thenReturn(mockUser);

        Optional<User> result = userService.findByUsername(mockUser.getUsername());

        Assertions.assertEquals(result, Optional.of(mockUser));
    }

    @Test
    void findByEmail_should_returnUser_whenEmailExists() {
        User mockUser = createMockDeveloper();
        Mockito.when(userRepository.getByField("email", mockUser.getEmail()))
                .thenReturn(mockUser);

        Optional<User> result = userService.findByEmail(mockUser.getEmail());

        Assertions.assertEquals(result.get(), mockUser);
    }

    @Test
    void getById_should_throw_when_userNotExists() {

        //Arrange
        Mockito.when(userRepository.getById(2))
                .thenThrow(new NonexistingEntityException());

        //Act, Assert
        Assertions.assertThrows(NonexistingEntityException.class, ()-> userService.getById(2));
    }

    @Test
    void add_should_throw_when_userWithSameUsernameExists() {
        //Arrange
        User mockUser = createMockDeveloper();
        Mockito.when(userRepository.getByField("username", mockUser.getUsername()))
                .thenReturn(mockUser);

        //Act, Assert
        Assertions.assertThrows(InvalidEntityDataException.class, ()-> userService.add(mockUser));
    }


    @Test
    void add_should_throw_when_userWithSameEmailExists() {
        //Arrange
        User mockUser = createMockDeveloper();
        Mockito.when(userRepository.getByField("username", mockUser.getUsername()))
                .thenReturn(null);
        Mockito.when(userRepository.getByField("email", mockUser.getEmail()))
                .thenReturn(mockUser);

        //Act, Assert
        Assertions.assertThrows(InvalidEntityDataException.class, ()-> userService.add(mockUser));
    }


    @Test
    void add_should_addUser_when_validInput() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userRepository.getByField("username", mockUser.getUsername()))
                .thenReturn(null);
        Mockito.when(userRepository.getByField("email", mockUser.getEmail()))
                .thenReturn(null);

        //Act
        userService.add(mockUser);

        //Assert
        Mockito.verify(statusUserRepository, Mockito.times(1)).getById(Mockito.anyLong());
        Mockito.verify(roleRepository, Mockito.times(1)).getById(Mockito.anyLong());
        Mockito.verify(userRepository, Mockito.times(1)).create(Mockito.any(User.class));
    }

    @Test
    void update_should_throw_when_userIsNotOwnerAndIsNotAdmin() {
        //Arrange
        User mockUser = createMockDeveloper();
        mockUser.setId(2);
        User anotherMockUser = createMockDeveloper();
//        Mockito.when(userRepository.getById(Mockito.anyLong()))
//                .thenReturn(anotherMockUser);

        //Act, Assert
        Assertions.assertThrows(UnauthorisedOperationException.class, ()-> userService.update(mockUser, anotherMockUser));
    }

    @Test
    void update_should_throw_when_updatedEmailIsTaken(){
        User mockUser = createMockDeveloper();
        mockUser.setId(2);
        User anotherMockUser = createMockDeveloper();
        Mockito.when(userRepository.getByField("email", mockUser.getEmail()))
                .thenReturn(anotherMockUser);

        Assertions.assertThrows(InvalidEntityDataException.class, ()-> userService.update(mockUser, mockUser));
    }

    @Test
    void update_should_CallRepository_when_userIsOwnerAndEmailIsNotTaken(){
        //Arrange
        User mockUser = createMockDeveloper();
        Mockito.when(userRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(NonexistingEntityException.class);

        //Act
        userService.update(mockUser, mockUser);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).update(mockUser);
    }

    @Test
    void update_should_CallRepository_when_userIsAdminAndEmailIsNotTaken(){
        //Arrange
        User mockUser = createMockDeveloper();
        mockUser.setId(2);
        User mockAdmin = createMockAdmin();
        Mockito.when(userRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(NonexistingEntityException.class);

        //Act
        userService.update(mockAdmin, mockUser);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).update(mockUser);
    }

    @Test
    void update_should_updateUser_when_userIsOwnerAndEmailIsNotTaken(){
        //Arrange
        User mockUser = createMockDeveloper();
        mockUser.setEmail("mock2@user.com");
        Mockito.when(userRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(NonexistingEntityException.class);

        //Act
        User result = userService.update(mockUser, mockUser);

        //Assert
        Assertions.assertEquals(mockUser.getId(), result.getId());
        Assertions.assertEquals("mock2@user.com", result.getEmail());
    }

    @Test
    void delete_should_throw_when_userIsNotOwnerAndIsNotAdmin() {
        //Arrange
        User mockUser = createMockDeveloper();

        //Act, Assert
        Assertions.assertThrows(UnauthorisedOperationException.class, ()-> userService.deleteById(mockUser, 2));
    }

    @Test
    void delete_should_callRepository_when_userIsOwnerAndIsNotAdmin() {
        //Arrange
        User mockUser = createMockDeveloper();

        Mockito.when(userRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        //Act
        userService.deleteById(mockUser, mockUser.getId());

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).delete(mockUser.getId());
    }

    @Test
    void delete_should_callRepository_when_userIsNotOwnerAndIsAdmin() {
        //Arrange
        User mockUser = createMockDeveloper();
        mockUser.setId(2);
        User mockAdmin = createMockAdmin();

        Mockito.when(userRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        //Act
        userService.deleteById(mockAdmin, mockUser.getId());

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).delete(mockUser.getId());
    }

    @Test
    void addRole_should_addRole_when_loggedUserIsAdmin(){
        //Arrange
        User mockUser=createMockUser();
        mockUser.setId(2);
        User mockAdmin = createMockAdmin();
        Role mockRole = createMockRole("PRODUCT_OWNER");
        Mockito.when(userRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(NonexistingEntityException.class);

        //Act
        userService.addUserRole(mockAdmin, mockUser, mockRole);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).update(mockUser);
        Assertions.assertTrue(mockUser.getRoles().contains(mockRole));
    }

    @Test
    void addRole_should_throw_when_loggedUserIsNotAdmin(){
        //Arrange
        User mockUser=createMockDeveloper();
        Role mockRole = createMockRole("PRODUCT_OWNER");

        //Act, Assert
        Assertions.assertThrows(UnauthorisedOperationException.class,
                ()-> userService.addUserRole(mockUser, mockUser, mockRole));
    }

    @Test
    void removeRole_should_removeRole_when_loggedUserIsAdmin(){
        //Arrange
        User mockUser=createMockUser();
        mockUser.setId(2);
        User mockAdmin = createMockAdmin();
        Role mockRole2 = createMockRole("PRODUCT_OWNER");
        mockRole2.setId(2);
        Role mockRole1 = createMockRole("DEVELOPER");
        mockUser.getRoles().add(mockRole1);
        mockUser.getRoles().add(mockRole2);

        Mockito.when(userRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(NonexistingEntityException.class);

        //Act
        userService.removeUserRole(mockAdmin, mockUser, mockRole1);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).update(mockUser);
        Assertions.assertEquals(mockUser.getId(),2);
        Assertions.assertFalse(mockUser.getRoles().contains(mockRole1));
        Assertions.assertTrue(mockUser.getRoles().contains(mockRole2));

    }

    @Test
    void removeRole_should_throw_when_userDoesNotContainRole(){
        //Arrange
        User mockUser=createMockUser();
        mockUser.setId(2);
        User mockAdmin = createMockAdmin();
        Role mockRole2 = createMockRole("PRODUCT_OWNER");
        mockRole2.setId(2);
        Role mockRole1 = createMockRole("DEVELOPER");
        mockUser.getRoles().add(mockRole1);

        Mockito.when(userRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(NonexistingEntityException.class);

        //Act
        userService.removeUserRole(mockAdmin, mockUser, mockRole1);
        //Assert
        Assertions.assertThrows(InvalidEntityDataException.class,
                ()->userService.removeUserRole(mockAdmin, mockUser, mockRole2));
    }



}
