package course.academy;


import course.academy.model.Role;
import course.academy.model.StatusUser;
import course.academy.model.User;
import course.academy.model.dto.UserDto;

import java.util.Set;

public class Helpers {

    public static User createMockDeveloper() {
        return createMockUser("DEVELOPER");
    }

    public static User createMockProductOwner() {
        return createMockUser("PRODUCT_OWNER");
    }

    public static User createMockAdmin() {
        return createMockUser("ADMIN");
    }

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setLastName("MockLastName");
        mockUser.setPassword("MockPassword1$");
        mockUser.setFirstName("MockFirstName");
        mockUser.setContacts("MockContacts");
        mockUser.setStatusUser(createMockStatusUser());
        return mockUser;
    }

    private static User createMockUser(String role) {
        User mockUser = createMockUser();
        mockUser.setRoles(Set.of(createMockRole(role)));
        return mockUser;
    }


    public static Role createMockRole(String role) {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setName(role);
        return mockRole;
    }

    public static StatusUser createMockStatusUser() {
        var mockStatusUser = new StatusUser();
        mockStatusUser.setId(1);
        mockStatusUser.setStatusUser("Active");
        return mockStatusUser;
    }

    public static UserDto createUserDto() {
        UserDto dto = new UserDto();
        dto.setEmail("mock@user.com");
        dto.setFirstName("mockFirstName");
        dto.setLastName("mockLastName");
        dto.setContacts("mockContacts");
        return dto;
    }


}
