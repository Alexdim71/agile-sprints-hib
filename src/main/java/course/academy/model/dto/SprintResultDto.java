package course.academy.model.dto;

import course.academy.model.SprintResult;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MIN_LENGTH;

public class SprintResultDto {

    @Positive(message = "Sprint Id must be a positive number.")
    private long sprintId;

    @Size(min = RESULTS_DESCRIPTION_MIN_LENGTH, max = RESULTS_DESCRIPTION_MAX_LENGTH,
            message = "Results Description must be between 10 and 2500 characters.")
    private String resultsDescription;


    public SprintResultDto() {
    }

    public SprintResultDto(long sprintId) {
        this.sprintId = sprintId;
    }

    public SprintResultDto(long sprintId, String resultsDescription) {
        this.sprintId = sprintId;
        this.resultsDescription = resultsDescription;
    }

    public long getSprintId() {
        return sprintId;
    }

    public void setSprintId(long sprintId) {
        this.sprintId = sprintId;
    }

    public String getResultsDescription() {
        return resultsDescription;
    }

    public void setResultsDescription(String resultsDescription) {
        this.resultsDescription = resultsDescription;
    }
}
