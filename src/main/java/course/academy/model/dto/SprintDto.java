package course.academy.model.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public class SprintDto {

    @Positive(message = "Project Id must be a positive number.")
    private long projectId;

    @DateTimeFormat
    private LocalDate startDate;

    @Positive(message = "Duration must be a positive number.")
    private int duration;

    private Set<Long> developerIds;

    public SprintDto() {
    }

    public SprintDto(long projectId, LocalDate startDate, int duration) {
        this.projectId = projectId;
        this.startDate = startDate;
        this.duration = duration;
    }

    public SprintDto(long projectId, LocalDate startDate, int duration, Set<Long> developerIds) {
        this.projectId = projectId;
        this.startDate = startDate;
        this.duration = duration;
        this.developerIds = developerIds;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Set<Long> getDeveloperIds() {
        return developerIds;
    }

    public void setDeveloperIds(Set<Long> developerIds) {
        this.developerIds = developerIds;
    }
}

