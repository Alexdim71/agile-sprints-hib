package course.academy.model.dto;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static course.academy.model.Task.*;

public class TaskDto {

    @Positive(message = "Project Id must be a positive number.")
    private long projectId;

    @Positive(message = "Kind Id must be a positive number.")
    private long kindId;

    @NotBlank
    @Size(min = TITLE_MIN_LENGTH, max = TITLE_MAX_LENGTH, message = "Title length must be between 2 amd 120 characters.")
    private String title;

    @Positive(message = "Estimated effort must be a positive number.")
    private int estimatedEffort;

    @Size(max = 200, message = "Tags must be maximum 200 characters")
    private String tags;

    @Size(min = DESCRIPTION_MIN_LENGTH, max = DESCRIPTION_MAX_LENGTH, message = "Description length must be between 15 amd 2500 characters." )
    private String description;

    public TaskDto() {
    }

    public TaskDto(int projectId, int KindId, String title, int estimatedEffort, String tags, String description) {
        this.projectId = projectId;
        this.kindId = kindId;
        this.title = title;
        this.estimatedEffort = estimatedEffort;
        this.tags = tags;
        this.description = description;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public long getKindId() {
        return kindId;
    }

    public void setKindId(long kindId) {
        this.kindId = kindId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getEstimatedEffort() {
        return estimatedEffort;
    }

    public void setEstimatedEffort(int estimatedEffort) {
        this.estimatedEffort = estimatedEffort;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}