package course.academy.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static course.academy.model.Project.*;

public class ProjectDto {

    @NotBlank
    @Size(min = TITLE_MIN_LENGTH, max = TITLE_MAX_LENGTH, message = "Title length must be between 2 and 120 characters.")
    private String title;

    @NotNull
    private LocalDate startDate;

    @NotNull
    @Positive(message = "Owner Id must be a positive number.")
    private long ownerId;

    private Set<Long> developerIds = new HashSet<>();

    private String tags;

    @Size(min = DESCRIPTION_MIN_LENGTH, max = DESCRIPTION_MAX_LENGTH, message = "Description length must be between 10 and 2500 characters.")
    private String description;

    public ProjectDto() {
    }

    public ProjectDto(String title, LocalDate startDate, long ownerId, Set<Long> developerIds, String tags) {
        this.title = title;
        this.startDate = startDate;
        this.ownerId = ownerId;
        this.developerIds = developerIds;
        this.tags = tags;
    }

    public ProjectDto(String title, LocalDate startDate, long ownerId, Set<Long> developerIds, String tags, String description) {
        this.title = title;
        this.startDate = startDate;
        this.ownerId = ownerId;
        this.developerIds = developerIds;
        this.tags = tags;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Set<Long> getDeveloperIds() {
        return developerIds;
    }

    public void setDeveloperIds(Set<Long> developerIds) {
        this.developerIds = developerIds;
    }

}
