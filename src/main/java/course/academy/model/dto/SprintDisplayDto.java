package course.academy.model.dto;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class SprintDisplayDto{

    private long id;
    private long projectId;
    private LocalDate startDate;
    private int duration;
    private LocalDate endDate;
    private long ownerId;
    private LocalDateTime created = LocalDateTime.now();
    private LocalDateTime modified = LocalDateTime.now();

    public SprintDisplayDto() {
    }

    public SprintDisplayDto(long id, long projectId, LocalDate startDate, int duration,
                            LocalDate endDate, long ownerId, LocalDateTime created, LocalDateTime modified) {
        this.id = id;
        this.projectId = projectId;
        this.startDate = startDate;
        this.duration = duration;
        this.endDate = endDate;
        this.ownerId = ownerId;
        this.created = created;
        this.modified = modified;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SprintDisplayDto)) return false;

        SprintDisplayDto sprint = (SprintDisplayDto) o;

        return id == sprint.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

}
