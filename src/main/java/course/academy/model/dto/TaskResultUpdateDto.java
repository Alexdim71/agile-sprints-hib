package course.academy.model.dto;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MIN_LENGTH;

public class TaskResultUpdateDto {

    @Positive(message = "Actual Effort should be a positive number.")
    private int actualEffort;

    @Size(min = RESULT_DESCRIPTION_MIN_LENGTH, max = RESULT_DESCRIPTION_MAX_LENGTH,
            message = "Result Description must be between 10 and 2500 characters.")
    private String resultDescription;

    public TaskResultUpdateDto() {
    }

    public TaskResultUpdateDto(int actualEffort, String resultDescription) {
        this.actualEffort = actualEffort;
        this.resultDescription = resultDescription;
    }


    public int getActualEffort() {
        return actualEffort;
    }

    public void setActualEffort(int actualEffort) {
        this.actualEffort = actualEffort;
    }

    public String getResultDescription() {
        return resultDescription;
    }

    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }
}