package course.academy.model.dto;

import course.academy.model.Identifiable;
import course.academy.model.Sprint;
import course.academy.model.TaskResult;
import course.academy.utils.ParsingHelpers;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

import static course.academy.utils.ParsingHelpers.dtf;


public class SprintResultDisplayDto {


    private long id;

    private SprintDisplayDto sprintDisplayDto;

    private int teamVelocity;

    private String resultsDescription;


    private LocalDateTime created = LocalDateTime.now();


    private LocalDateTime modified = LocalDateTime.now();

    public SprintResultDisplayDto() {
    }

    public SprintResultDisplayDto(long id, SprintDisplayDto sprintDisplayDto, int teamVelocity,
                                  String resultsDescription, LocalDateTime created, LocalDateTime modified) {
        this.id = id;
        this.sprintDisplayDto = sprintDisplayDto;
        this.teamVelocity = teamVelocity;
        this.resultsDescription = resultsDescription;
        this.created = created;
        this.modified = modified;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SprintDisplayDto getSprintDisplayDto() {
        return sprintDisplayDto;
    }

    public void setSprintDisplayDto(SprintDisplayDto sprintDisplayDto) {
        this.sprintDisplayDto = sprintDisplayDto;
    }

    public int getTeamVelocity() {
        return teamVelocity;
    }

    public void setTeamVelocity(int teamVelocity) {
        this.teamVelocity = teamVelocity;
    }

    public String getResultsDescription() {
        return resultsDescription;
    }

    public void setResultsDescription(String resultsDescription) {
        this.resultsDescription = resultsDescription;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SprintResultDisplayDto)) return false;

        SprintResultDisplayDto that = (SprintResultDisplayDto) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }


}
