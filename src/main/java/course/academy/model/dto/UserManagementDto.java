package course.academy.model.dto;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.Set;

import static course.academy.model.User.*;
import static course.academy.model.User.CONTACTS_MAX_LENGTH;

public class UserManagementDto {

    @NotBlank
    @Size(min = FIRST_NAME_MIN_LENGTH, max = FIRST_NAME_MAX_LENGTH, message = "First Name must be between 2 and 15 characters.")
    private String firstName;

    @NotBlank
    @Size(min = LAST_NAME_MIN_LENGTH, max = LAST_NAME_MAX_LENGTH, message = "Last Name must be between 2 and 15 characters.")
    private String lastName;

    @NotBlank
    @Email(message = "Please enter a valid mail address.")
    private String email;

    @Size(min = CONTACTS_MIN_LENGTH, max = CONTACTS_MAX_LENGTH, message = "Contacts must be 10 between and 250 characters.")
    private String contacts;

    private Set<Long> roleIds;

    @Positive(message = "User Status Id must be a positive number.")
    private long statusUserId;


    public UserManagementDto() {
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public Set<Long> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(Set<Long> roleIds) {
        this.roleIds = roleIds;
    }

    public long getStatusUserId() {
        return statusUserId;
    }

    public void setStatusUserId(long statusUserId) {
        this.statusUserId = statusUserId;
    }
}









