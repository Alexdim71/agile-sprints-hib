package course.academy.model.dto;

import course.academy.model.TaskResult;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MIN_LENGTH;
import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MIN_LENGTH;

public class TaskResultDto {

    @Positive(message = "Task Id should be a positive number.")
    private long taskId;

    @Positive(message = "Actual Effort should be a positive number.")
    private int actualEffort;

    @Size(min = RESULT_DESCRIPTION_MIN_LENGTH, max = RESULT_DESCRIPTION_MAX_LENGTH,
            message = "Result Description must be between 10 and 2500 characters.")
    private String resultDescription;

    public TaskResultDto() {
    }

    public TaskResultDto(long taskId, int actualEffort) {
        this.taskId = taskId;
        this.actualEffort = actualEffort;
    }

    public TaskResultDto(long taskId, int actualEffort, String resultDescription) {
        this.taskId = taskId;
        this.actualEffort = actualEffort;
        this.resultDescription = resultDescription;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public int getActualEffort() {
        return actualEffort;
    }

    public void setActualEffort(int actualEffort) {
        this.actualEffort = actualEffort;
    }

    public String getResultDescription() {
        return resultDescription;
    }

    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }
}