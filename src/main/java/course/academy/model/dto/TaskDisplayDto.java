package course.academy.model.dto;

import course.academy.model.*;
import java.time.LocalDateTime;

public class TaskDisplayDto {

    private long id;

    private long projectId;

    private Kind kind;

    private String title;

    private long addedById;

    private int estimatedEffort;

    private StatusTask statusTask;

    private long sprintId;

    private String description;

    private String tags;

    private LocalDateTime created = LocalDateTime.now();

    private LocalDateTime modified = LocalDateTime.now();

    public TaskDisplayDto() {
    }

    public TaskDisplayDto(long id, long projectId, Kind kind, String title, long addedById,
                          int estimatedEffort, StatusTask statusTask, long sprintId,
                          String description, String tags, LocalDateTime created, LocalDateTime modified) {
        this.id = id;
        this.projectId = projectId;
        this.kind = kind;
        this.title = title;
        this.addedById = addedById;
        this.estimatedEffort = estimatedEffort;
        this.statusTask = statusTask;
        this.sprintId = sprintId;
        this.description = description;
        this.tags = tags;
        this.created = created;
        this.modified = modified;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public Kind getKind() {
        return kind;
    }

    public void setKind(Kind kind) {
        this.kind = kind;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getAddedById() {
        return addedById;
    }

    public void setAddedById(long addedById) {
        this.addedById = addedById;
    }

    public int getEstimatedEffort() {
        return estimatedEffort;
    }

    public void setEstimatedEffort(int estimatedEffort) {
        this.estimatedEffort = estimatedEffort;
    }

    public StatusTask getStatusTask() {
        return statusTask;
    }

    public void setStatusTask(StatusTask statusTask) {
        this.statusTask = statusTask;
    }

    public long getSprintId() {
        return sprintId;
    }

    public void setSprintId(long sprintId) {
        this.sprintId = sprintId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskDisplayDto)) return false;

        TaskDisplayDto task = (TaskDisplayDto) o;

        return id == task.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

}
