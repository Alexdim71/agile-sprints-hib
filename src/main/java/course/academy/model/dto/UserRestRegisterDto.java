package course.academy.model.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static course.academy.model.User.*;
import static course.academy.model.User.CONTACTS_MAX_LENGTH;

public class UserRestRegisterDto {

    @NotBlank
    @Size(min = FIRST_NAME_MIN_LENGTH, max = FIRST_NAME_MAX_LENGTH, message = "First Name must be between 2 and 15 characters.")
    private String firstName;

    @NotBlank
    @Size(min = LAST_NAME_MIN_LENGTH, max = LAST_NAME_MAX_LENGTH, message = "Last Name must be between 2 and 15 characters.")
    private String lastName;

    @NotBlank
    @Email(message = "Please enter a valid mail address.")
    private String email;

    @Size(min = CONTACTS_MIN_LENGTH, max = CONTACTS_MAX_LENGTH, message = "Contacts must be 10 between and 250 characters.")
    private String contacts;

    @NotBlank
    @Size(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH, message = "Username must be between 2 and 15 characters.")
    private String username;

    @NotBlank
    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH, message = "Password must be between 2 and 15 characters.")
    private String password;



    public UserRestRegisterDto() {
    }

    public UserRestRegisterDto(String username, String password, String firstName,
                               String lastName, String email, String contacts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
        this.contacts = contacts;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }
}









