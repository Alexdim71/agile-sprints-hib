package course.academy.model.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Positive;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public class SprintUpdateDto {


    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate startDate;

    @Positive(message = "Duration must be a positive number.")
    private int duration;

    public SprintUpdateDto() {
    }


    public SprintUpdateDto(long projectId, LocalDate startDate, int duration) {
        this.startDate = startDate;
        this.duration = duration;
    }


    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

}
