package course.academy.model.dto;

import course.academy.model.ProjectResult;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDate;

import static course.academy.model.ProjectResult.RESULTS_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.ProjectResult.RESULTS_DESCRIPTION_MIN_LENGTH;

public class ProjectResultDto {

    @Positive(message = "Project Id must be a positive number.")
    private long projectId;

    @NotNull
    @DateTimeFormat
    private LocalDate endDate;

    @Size(min = RESULTS_DESCRIPTION_MIN_LENGTH, max=RESULTS_DESCRIPTION_MAX_LENGTH,
            message = "Results Description must be between 10 and 2500 characters.")
    private String resultsDescription;

    public ProjectResultDto() {
    }

    public ProjectResultDto(long projectId, LocalDate endDate) {
        this.projectId = projectId;
        this.endDate = endDate;
    }

    public ProjectResultDto(long projectId, LocalDate endDate, String resultsDescription) {
        this.projectId = projectId;
        this.endDate = endDate;
        this.resultsDescription = resultsDescription;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getResultsDescription() {
        return resultsDescription;
    }

    public void setResultsDescription(String resultsDescription) {
        this.resultsDescription = resultsDescription;
    }
}
