package course.academy.model.dto;

import java.time.LocalDateTime;

public class TaskResultDisplayDto {

    private long id;

    private long taskId;

    private long projectId;

    private long sprintId;

    private int actualEffort;

    private long verifiedBy;

    private String resultDescription;

    private LocalDateTime created = LocalDateTime.now();

    private LocalDateTime modified = LocalDateTime.now();

    public TaskResultDisplayDto() {
    }

    public TaskResultDisplayDto(long id, long taskId, long projectId, long sprintId, int actualEffort, long verifiedBy,
                                String resultDescription, LocalDateTime created, LocalDateTime modified) {
        this.id = id;
        this.taskId = taskId;
        this.projectId = projectId;
        this.sprintId = sprintId;
        this.actualEffort = actualEffort;
        this.verifiedBy = verifiedBy;
        this.resultDescription = resultDescription;
        this.created = created;
        this.modified = modified;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public long getSprintId() {
        return sprintId;
    }

    public void setSprintId(long sprintId) {
        this.sprintId = sprintId;
    }

    public int getActualEffort() {
        return actualEffort;
    }

    public void setActualEffort(int actualEffort) {
        this.actualEffort = actualEffort;
    }

    public long getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(long verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public String getResultDescription() {
        return resultDescription;
    }

    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskResultDisplayDto)) return false;

        TaskResultDisplayDto that = (TaskResultDisplayDto) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }


}
