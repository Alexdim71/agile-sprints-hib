package course.academy.model.dto;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MIN_LENGTH;

public class SprintResultUpdateDto {

    @Size(min = RESULTS_DESCRIPTION_MIN_LENGTH, max = RESULTS_DESCRIPTION_MAX_LENGTH,
            message = "Results Description must be between 10 and 2500 characters.")
    private String resultsDescription;


    public SprintResultUpdateDto() {
    }


    public SprintResultUpdateDto(String resultsDescription) {
        this.resultsDescription = resultsDescription;
    }


    public String getResultsDescription() {
        return resultsDescription;
    }

    public void setResultsDescription(String resultsDescription) {
        this.resultsDescription = resultsDescription;
    }
}
