package course.academy.model;

public enum StatusUsers {
    ACTIVE, CHANGE_PASSWORD, SUSPENDED, DEACTIVATED
}
