package course.academy.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static course.academy.utils.ParsingHelpers.df;
import static course.academy.utils.ParsingHelpers.dtf;
import static java.time.temporal.ChronoUnit.DAYS;

@Entity
@Table(name = "project_results")
public class ProjectResult implements Identifiable<Long>, Serializable {
    public static final int RESULTS_DESCRIPTION_MIN_LENGTH = 10;
    public static final int RESULTS_DESCRIPTION_MAX_LENGTH = 2500;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name ="project_id")
    private Project project;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "duration")
    private int duration;

    @Column(name = "results_description")
    private String resultsDescription;

    @Column(name = "created")
    private final LocalDateTime created = LocalDateTime.now();

    @Column(name = "modified")
    private LocalDateTime modified = LocalDateTime.now();

    public ProjectResult() {
    }

    public ProjectResult(Project project, LocalDate endDate) {
        this.project = project;
        this.endDate = endDate;
        setDuration(endDate);
    }

    public ProjectResult(Project project, LocalDate endDate, String resultsDescription) {
        this(project, endDate);
        this.resultsDescription = resultsDescription;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(LocalDate endDate) {
        if (project != null) {
            LocalDate startDate = getProject().getStartDate();
            this.duration = (int) DAYS.between(startDate, endDate);
        }
    }

    public String getResultsDescription() {
        return resultsDescription;
    }

    public void setResultsDescription(String resultsDescription) {
        this.resultsDescription = resultsDescription;
    }


    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProjectResult)) return false;

        ProjectResult that = (ProjectResult) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder("ProjectResult {");
        sb.append("id=").append(id);
        sb.append(", project= {id=").append(project.getId()).append(", title='").append(project.getTitle()).append("}");
        sb.append(", endDate=").append(endDate.format(df));
        sb.append(", duration=").append(duration).append(" days, ");
        sb.append(System.lineSeparator()).append("Sprint results: ");
        for (SprintResult result : getProject().getPreviousSprintResults()) {
            sb.append("{id=").append(result.getId())
                    .append(", team velocity=").append(result.getTeamVelocity()).append(" effort units per day}, ");
        }
        sb.append(System.lineSeparator()).append("created=").append(created.format(dtf));
        sb.append(", modified=").append(modified.format(dtf));
        sb.append('}');
        return sb.toString();
    }

}
