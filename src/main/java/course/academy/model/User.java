package course.academy.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static course.academy.utils.ParsingHelpers.dtf;

@Entity
@Table(name = "users")
public class User implements Identifiable<Long>, Serializable {
    public static String checkUsername = "^[\\w]{2,15}$";
    public static String checkPassword = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#?!@$%^&*-])[A-Za-z\\d#?!@$%^&*-]{8,15}$";
    public static String checkEmail = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
    public static final int PASSWORD_MIN_LENGTH = 8;
    public static final int PASSWORD_MAX_LENGTH = 15;
    public static final int FIRST_NAME_MIN_LENGTH = 2;
    public static final int FIRST_NAME_MAX_LENGTH = 15;
    public static final int LAST_NAME_MIN_LENGTH = 2;
    public static final int LAST_NAME_MAX_LENGTH = 15;
    public static final int USERNAME_MIN_LENGTH = 2;
    public static final int USERNAME_MAX_LENGTH = 15;
    public static final int CONTACTS_MIN_LENGTH = 10;
    public static final int CONTACTS_MAX_LENGTH = 250;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "username")
    private String username;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @Column(name = "contacts")
    private String contacts;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "status_id")
    private StatusUser statusUser;

    @Column(name = "created")
    private final LocalDateTime created = LocalDateTime.now();

    @Column(name = "modified")
    private LocalDateTime modified = LocalDateTime.now();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_tasks",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "task_id"))
    public Set<Task> assignedTasks = new HashSet<>();

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_projects",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "project_id"))
    public Set<Project> projects = new HashSet<>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_task_results",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "task_result_id"))
    public Set<TaskResult> completedTaskResults = new HashSet<>();

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_project_results",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "project_result_id"))
    public Set<ProjectResult> completedProjectResult = new HashSet<>();

    public User() {
    }

    public User(String firstName, String lastName, String email, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
//        roles.add(Role.DEVELOPER);
//        setRoles(roles);
    }

    public User(long id, String firstName, String lastName, String email, String username, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
    }


    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    public StatusUser getStatusUser() {
        return statusUser;
    }

    public void setStatusUser(StatusUser statusUser) {
        this.statusUser = statusUser;
    }

    public String getPassword() {
        return password;
    }

    @JsonIgnore
    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Task> getAssignedTasks() {
        return assignedTasks;
    }

    public void setAssignedTasks(Set<Task> assignedTasks) {
        this.assignedTasks = assignedTasks;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public Set<TaskResult> getCompletedTaskResults() {
        return completedTaskResults;
    }

    public void setCompletedTaskResults(Set<TaskResult> completedTaskResults) {
        this.completedTaskResults = completedTaskResults;
    }

    public Set<ProjectResult> getCompletedProjectResult() {
        return completedProjectResult;
    }

    public void setCompletedProjectResult(Set<ProjectResult> completedProjectResult) {
        this.completedProjectResult = completedProjectResult;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonIgnore
    public boolean isAdmin() {
        return getRoles().stream().map(Role::getName).anyMatch(name -> name.equals("ADMIN"));
    }

    @JsonIgnore
    public boolean hasRoleOwner() {
        return getRoles().stream().map(Role::getName).anyMatch(name -> name.equals("PRODUCT_OWNER"));
    }

    @JsonIgnore
    public boolean hasRoleDeveloper() {
        return getRoles().stream().map(Role::getName).anyMatch(name -> name.equals("DEVELOPER"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        return getId() != null ? getId().equals(user.getId()) : user.getId() == null;
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder("User {");
        sb.append("id=").append(id);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", roles=");
        for (Role role : getRoles())
            sb.append(role.getName()).append(",");
        if ((getRoles().stream().map(Role::getName).anyMatch(r -> r.equals("DEVELOPER")))) {
            sb.append(System.lineSeparator()).append("Active tasks: ");
            if (getAssignedTasks().isEmpty()) {
                sb.append("no tasks,");
            } else {
                for (Task task : getAssignedTasks()) {
                    sb.append("{id=").append(task.getId());
                    sb.append(", title='").append(task.getTitle()).append("'}, ");
                }
            }
            sb.append(System.lineSeparator()).append("Completed tasks results: ");
            if (getCompletedTaskResults().isEmpty()) {
                sb.append("no task results,");
            } else {
                for (TaskResult taskResult : getCompletedTaskResults()) {
                    sb.append("{task result id =").append(taskResult.getId());
                    sb.append(", task id=").append(taskResult.getTask().getId());
                    sb.append(", task title='").append(taskResult.getTask().getTitle()).append("'}, ");
                }
            }
        }
        if ((getRoles().stream().map(Role::getName).anyMatch(r -> r.equals("PRODUCT_OWNER")))) {
            sb.append(System.lineSeparator()).append("Currently assigned projects: ");
            if (getProjects().isEmpty()) {
                sb.append("no projects,");
            } else {
                for (Project project : getProjects()) {
                    sb.append("{id=").append(project.getId());
                    sb.append(", title='").append(project.getTitle()).append("'}, ");
                }
            }
            sb.append(System.lineSeparator()).append("Completed projects: ");
            if (getCompletedProjectResult().isEmpty()) {
                sb.append("no projects,");
            } else {
                for (ProjectResult projectResult : getCompletedProjectResult()) {
                    sb.append("{project result id =").append(projectResult.getId());
                    sb.append(", project id=").append(projectResult.getProject().getId());
                    sb.append(", project title='").append(projectResult.getProject().getTitle()).append("'}, ");
                }
            }
        }
        sb.append(System.lineSeparator()).append("created=").append(created.format(dtf));
        sb.append(", modified=").append(modified.format(dtf));
        sb.append('}');
        return sb.toString();
    }

}










