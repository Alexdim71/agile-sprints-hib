package course.academy.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static course.academy.utils.ParsingHelpers.dtf;

@Entity
@Table(name="tasks")
public class Task implements Identifiable<Long>, Serializable {
    public static final int TITLE_MIN_LENGTH = 2;
    public static final int TITLE_MAX_LENGTH = 120;
    public static final int DESCRIPTION_MIN_LENGTH = 15; //150 by document, decreased for test
    public static final int DESCRIPTION_MAX_LENGTH = 2500;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name ="project_id")
    private Project project; //adding in addition to the task Template

    @ManyToOne
    @JoinColumn(name ="kind_id")
    private Kind kind;

    @Column(name = "title")
    private String title;

    @ManyToOne
    @JoinColumn(name ="added_by_id")
    private User addedBy;

    @Column(name = "estimated_effort")
    private int estimatedEffort;

    @ManyToOne
    @JoinColumn(name ="status_task")
    private StatusTask statusTask;

    @ManyToOne
    @JoinColumn(name ="sprint_id")
    private Sprint sprint;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "tasks_developers",
            joinColumns = @JoinColumn(name = "task_id"),
            inverseJoinColumns = @JoinColumn(name = "developer_id"))
    private Set<User> developersAssigned = new HashSet<>();

    @Column(name = "description")
    private String description;

    @Column(name = "tags")
    private String tags;

    @Column(name = "created")
    private final LocalDateTime created = LocalDateTime.now();

    @Column(name = "modified")
    private LocalDateTime modified = LocalDateTime.now();

    public Task() {
    }

    public Task(Kind kind, Project project, String title, int estimatedEffort, String tags) {
        this.kind = kind;
        this.project = project;
        this.title = title;
        this.estimatedEffort = estimatedEffort;
        this.tags = tags;
    }

    public Task(Kind kind, Project project, String title, int estimatedEffort,
                String tags, String description) {
        this(kind, project, title, estimatedEffort, tags);
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(User addedBy) {
        this.addedBy = addedBy;
    }

    public int getEstimatedEffort() {
        return estimatedEffort;
    }

    public void setEstimatedEffort(int estimatedEffort) {
        this.estimatedEffort = estimatedEffort;
    }


    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    public Kind getKind() {
        return kind;
    }

    public void setKind(Kind kind) {
        this.kind = kind;
    }

    public StatusTask getStatusTask() {
        return statusTask;
    }

    public void setStatusTask(StatusTask statusTask) {
        this.statusTask = statusTask;
    }

    public Set<User> getDevelopersAssigned() {
        return developersAssigned;
    }

    public void setDevelopersAssigned(Set<User> developersAssigned) {
        this.developersAssigned = developersAssigned;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;

        Task task = (Task) o;

        return id == task.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder("Task {");
        sb.append("id=").append(id);
        sb.append(", project={id=").append(project.getId()).append(", title=").append(project.getTitle()).append("}");
        sb.append(", kind=").append(kind.getKind());
        sb.append(", title='").append(title).append('\'');
        sb.append(", addedBy={id=").append(addedBy.getId()).append(", username=").append(addedBy.getUsername()).append("}");
        sb.append(", estimatedEffort=").append(estimatedEffort).append(" effort units");
        sb.append(", statusTask=").append(statusTask.getStatusTask());
        if (sprint !=null) {
            sb.append(", sprint={id=").append(sprint.getId()).append("},");
        } else sb.append(", sprint='not assigned',");
        sb.append(System.lineSeparator()).append("Developers assigned:");
        if (developersAssigned.isEmpty()) {
            sb.append(" no developers assigned");
        } else {
            for (User developer : developersAssigned) {
                sb.append(" {id=").append(developer.getId()).append(", username=").append(developer.getUsername()).append("},");
            }
        }
        sb.append(System.lineSeparator()).append("created=").append(created.format(dtf)).append(", modified=").append(modified.format(dtf));
        sb.append('}');
        return sb.toString();
    }
}
