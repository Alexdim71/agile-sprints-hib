package course.academy.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static course.academy.utils.ParsingHelpers.df;
import static course.academy.utils.ParsingHelpers.dtf;

@Entity
@Table(name="projects")
public class Project implements Identifiable<Long>, Serializable {
    public static final int TITLE_MIN_LENGTH = 2;
    public static final int TITLE_MAX_LENGTH = 120;
    public static final int DESCRIPTION_MIN_LENGTH = 10;
    public static final int DESCRIPTION_MAX_LENGTH = 2500;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "title")
    private String title;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name ="owner_id")
    private User owner;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "projects_developers",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "developer_id"))
    private Set<User> developers;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name ="current_sprint_id")
    private Sprint currentSprint;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "projects_sprint_results",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "sprint_result_id"))
    private Set<SprintResult> previousSprintResults = new HashSet<>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "projects_tasks",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "task_id"))
    private Set<Task> tasksBacklog = new HashSet<>();

    @Column(name = "tags")
    private String tags;

    @Column(name = "created")
    private final LocalDateTime created = LocalDateTime.now();
    @Column(name = "modified")
    private LocalDateTime modified = LocalDateTime.now();

    public Project() {
    }

    public Project(String title, LocalDate startDate, User owner, String tags) {
        this.title = title;
        this.startDate = startDate;
        this.owner = owner;
        this.tags = tags;
    }

    public Project(String title, LocalDate startDate, String description, User owner, String tags) {
        this(title,startDate,owner,tags);
        this.description = description;
    }


    public Project(String title, LocalDate startDate, User owner,
                   Set<User> developers, Set<Task> tasksBacklog, String tags) {
        this(title,startDate,owner,tags);
        this.developers = developers;
        this.tasksBacklog = tasksBacklog;
    }

    public Project(String title, LocalDate startDate, String description, User owner,
                   Set<User> developers, Set<Task> tasksBacklog, String tags) {
        this(title,startDate,owner,tags);
        this.developers = developers;
        this.tasksBacklog = tasksBacklog;
        this.description = description;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Sprint getCurrentSprint() {
        return currentSprint;
    }

    public void setCurrentSprint(Sprint currentSprint) {
        this.currentSprint = currentSprint;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Project)) return false;

        Project project = (Project) o;

        return id == project.id;
    }

    public Set<User> getDevelopers() {
        return developers;
    }

    public void setDevelopers(Set<User> developers) {
        this.developers = developers;
    }

    public Set<SprintResult> getPreviousSprintResults() {
        return previousSprintResults;
    }

    public void setPreviousSprintResults(Set<SprintResult> previousSprintResults) {
        this.previousSprintResults = previousSprintResults;
    }

    public Set<Task> getTasksBacklog() {
        return tasksBacklog;
    }

    public void setTasksBacklog(Set<Task> tasksBacklog) {
        this.tasksBacklog = tasksBacklog;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder("Project {");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", startDate=").append(startDate.format(df));
        sb.append(", owner= {id=").append(owner.getId()).append(", username=").append(owner.getUsername()).append("},");
        sb.append(System.lineSeparator()).append("Developers assigned: ");
        if (developers.isEmpty()) {
            sb.append("no developers assigned yet,");
        } else {
            for (User developer : developers) {
                sb.append("{id=").append(developer.getId()).append(", username=").append(developer.getUsername()).append("}, ");
            }
        }
        sb.append(System.lineSeparator()).append("Current/last sprint: ");
        if (currentSprint != null) {
            sb.append("{id=").append(currentSprint.getId()).append(", start date=").append(startDate.format(df)).append("}, ");
        } else {
            sb.append("no sprint started yet,");
        }
        sb.append(System.lineSeparator()).append("Previous sprint results: ");
        if (previousSprintResults.isEmpty()) {
            sb.append("no previous sprints available,");
            } else {
            for (SprintResult result : previousSprintResults) {
                sb.append(System.lineSeparator());
                sb.append("{id=").append(result.getId())
                        .append(", actual effort=").append(result.getTeamVelocity()).append(" effort units per day");
                sb.append(", completed tasks: ");
                for (Task task: result.getSprint().getTasks()) {
                    sb.append("{id=").append(task.getId());
                    sb.append(", title='").append(task.getTitle()).append("'}, ");
                }
            }
        }
        sb.append(System.lineSeparator()).append("Tasks backlog: ");
        if (tasksBacklog.isEmpty()) {
            sb.append(" no uncompleted tasks");
        } else {
            for (Task task : tasksBacklog) {
                sb.append("{id =").append(task.getId());
                sb.append(", title='").append(task.getTitle()).append("'}, ");
            }
        }
        sb.append(System.lineSeparator()).append("created=").append(created.format(dtf));
        sb.append(", modified=").append(modified.format(dtf));
        sb.append('}');
        return sb.toString();
    }
}





