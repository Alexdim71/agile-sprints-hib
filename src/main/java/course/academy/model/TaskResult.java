package course.academy.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

import static course.academy.utils.ParsingHelpers.dtf;

@Entity
@Table(name = "task_results")
public class TaskResult implements Identifiable<Long>, Serializable {
    public static final int RESULT_DESCRIPTION_MIN_LENGTH = 10;
    public static final int RESULT_DESCRIPTION_MAX_LENGTH = 2500;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "task_id")
    private Task task;

    @Column(name = "actual_effort")
    private int actualEffort;

    @ManyToOne
    @JoinColumn(name = "verified_by_id")
    private User verifiedBy;

    @Column(name = "result_description")
    private String resultDescription;

    @Column(name = "created")
    private final LocalDateTime created = LocalDateTime.now();

    @Column(name = "modified")
    private LocalDateTime modified = LocalDateTime.now();

    public TaskResult() {
    }

    public TaskResult(Task task, int actualEffort) {
        this.task = task;
        this.actualEffort = actualEffort;
    }

    public TaskResult(Task task, int actualEffort, String resultDescription) {
        this(task, actualEffort);
        this.resultDescription = resultDescription;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public int getActualEffort() {
        return actualEffort;
    }

    public void setActualEffort(int actualEffort) {
        this.actualEffort = actualEffort;
    }

    public User getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(User verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public String getResultDescription() {
        return resultDescription;
    }

    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskResult)) return false;

        TaskResult that = (TaskResult) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder("TaskResult {");
        sb.append("id=").append(id);
        sb.append(", Task={task id=").append(task.getId());
        sb.append(", task title=").append(task.getTitle()).append("}");
        sb.append(", actualEffort=").append(actualEffort).append(" effort units");
        sb.append(", verifiedBy User={id=").append(verifiedBy.getId());
        sb.append(", username=").append(verifiedBy.getUsername()).append("}");
        sb.append(", created=").append(created.format(dtf));
        sb.append(", modified=").append(modified.format(dtf));
        sb.append("}");
        return sb.toString();
    }
}
