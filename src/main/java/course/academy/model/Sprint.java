package course.academy.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static course.academy.utils.ParsingHelpers.df;
import static course.academy.utils.ParsingHelpers.dtf;

@Entity
@Table(name="sprints")
public class Sprint implements Identifiable<Long>, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name ="project_id")
    private Project project; //added additionally to the template

    @Column(name ="start_date")
    private LocalDate startDate;

    @Column(name ="duration")
    private int duration;

    @Column(name ="end_date")
    private LocalDate endDate;

    @ManyToOne
    @JoinColumn(name ="owner_id")
    private User owner;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "sprints_developers",
            joinColumns = @JoinColumn(name = "sprint_id"),
            inverseJoinColumns = @JoinColumn(name = "developer_id"))
    private Set<User> developers = new HashSet<>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "sprints_tasks",
            joinColumns = @JoinColumn(name = "sprint_id"),
            inverseJoinColumns = @JoinColumn(name = "task_id"))
    private Set<Task> tasks = new HashSet<>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "sprints_task_results",
            joinColumns = @JoinColumn(name = "sprint_id"),
            inverseJoinColumns = @JoinColumn(name = "task_result_id"))
    private Set<TaskResult> completedTaskResults = new HashSet<>();

    @Column(name = "created")
    private final LocalDateTime created = LocalDateTime.now();

    @Column(name = "modified")
    private LocalDateTime modified = LocalDateTime.now();

    public Sprint() {
    }

    public Sprint(Project project, LocalDate startDate, int duration) {
        this.project = project;
        this.startDate = startDate;
        this.duration = duration;
        this.endDate = startDate.plusDays(duration-1);
    }

    public Sprint(Project project, LocalDate startDate, int duration, Set<User> developers) {
        this.project = project;
        this.startDate = startDate;
        this.duration = duration;
        this.developers = developers;
        this.endDate = startDate.plusDays(duration-1);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Set<User> getDevelopers() {
        return developers;
    }

    public void setDevelopers(Set<User> developers) {
        this.developers = developers;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public Set<TaskResult> getCompletedTaskResults() {
        return completedTaskResults;
    }

    public void setCompletedTaskResults(Set<TaskResult> completedTaskResults) {
        this.completedTaskResults = completedTaskResults;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sprint)) return false;

        Sprint sprint = (Sprint) o;

        return id == sprint.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder("Sprint {");
        sb.append("id=").append(id);
        sb.append(", project={id=").append(project.getId()).append(", title=").append(project.getTitle()).append("}");
        sb.append(", startDate=").append(startDate.format(df));
        sb.append(", endDate=").append(endDate.format(df));
        sb.append(", duration=").append(duration).append(" days");
        sb.append(", owner={id=").append(owner.getId());
        sb.append(", username =").append(owner.getUsername()).append("}, ");
        sb.append(System.lineSeparator()).append("developers: ");
        if (!developers.isEmpty()) {
            for (User developer : developers) {
                sb.append("{id=").append(developer.getId());
                sb.append(", username=").append(developer.getUsername()).append("}, ");
            }
        }
        sb.append(System.lineSeparator()).append("tasks: ");
        for (Task task: tasks) {
            sb.append("{id=").append(task.getId());
            sb.append(", title='").append(task.getTitle()).append("'}, ");
        }
        sb.append(System.lineSeparator()).append("completed task results: ");
        if (completedTaskResults.isEmpty()) {
            sb.append("No completed tasks results available yet, ");
        } else {
            for (TaskResult result : completedTaskResults) {
                sb.append("{id =").append(result.getId());
                sb.append(", verified by={id=").append(result.getVerifiedBy().getId())
                        .append(", actual effort =").append(result.getActualEffort()).append(" effort units")
                        .append(", username=").append(result.getVerifiedBy().getUsername()).append("}, ");
            }
        }
        sb.append(System.lineSeparator()).append("created=").append(created.format(dtf)).append(", modified=").append(modified.format(dtf));
        sb.append('}');

        return sb.toString();
    }
}
