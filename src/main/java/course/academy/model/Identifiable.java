package course.academy.model;

public interface Identifiable<K> {
    K getId();
    void setId(K id);
}
