package course.academy.model;

public enum StatusTasks {
    PLANNED, ACTIVE, COMPLETED
}
