package course.academy.model.mapper;

import course.academy.model.Sprint;
import course.academy.model.SprintResult;
import course.academy.model.dto.SprintResultDisplayDto;
import course.academy.model.dto.SprintResultDto;
import course.academy.model.dto.SprintResultUpdateDto;
import course.academy.service.SprintResultService;
import course.academy.service.SprintService;
import org.springframework.stereotype.Component;

@Component
public class SprintResultMapper {
    private final SprintResultService sprintResultService;
    private final SprintService sprintService;
    private final SprintMapper sprintMapper;

    public SprintResultMapper(SprintResultService sprintResultService, SprintService sprintService, SprintMapper sprintMapper) {
        this.sprintResultService = sprintResultService;
        this.sprintService = sprintService;
        this.sprintMapper = sprintMapper;
    }

    public SprintResult fromDto(SprintResultDto dto) {
        SprintResult sprintResult = new SprintResult();
        dtoToObject(dto, sprintResult);
        return sprintResult;
    }

    public SprintResult fromDto(SprintResultDto dto, long id) {
        SprintResult sprintResult = sprintResultService.getById(id);
        dtoToObject(dto, sprintResult);
        return sprintResult;
    }

    public SprintResult fromUpdateDto(SprintResultUpdateDto dto, long id) {
        SprintResult sprintResult = sprintResultService.getById(id);
        updateDtoToObject(dto, sprintResult);
        return sprintResult;
    }

    private void dtoToObject(SprintResultDto dto, SprintResult sprintResult) {
        Sprint sprint = sprintService.getById(dto.getSprintId());
        sprintResult.setSprint(sprint);
        sprintResult.setResultsDescription(dto.getResultsDescription());
    }

    private void updateDtoToObject(SprintResultUpdateDto dto, SprintResult sprintResult) {
        sprintResult.setSprint(sprintResult.getSprint());
        sprintResult.setResultsDescription(dto.getResultsDescription());
    }

    public SprintResultDisplayDto sprintResultToDisplayDto(SprintResult sprintResult) {
        SprintResultDisplayDto dto = new SprintResultDisplayDto();
        dto.setId(sprintResult.getId());
        dto.setSprintDisplayDto(sprintMapper.sprintToDisplayDto(sprintResult.getSprint()));
        dto.setTeamVelocity(sprintResult.getTeamVelocity());
        dto.setResultsDescription(sprintResult.getResultsDescription());
        dto.setCreated(sprintResult.getCreated());
        dto.setModified(sprintResult.getModified());
        return dto;
    }

}
