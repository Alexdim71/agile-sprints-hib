package course.academy.model.mapper;

import course.academy.exception.NonexistingEntityException;
import course.academy.model.Project;
import course.academy.model.Task;
import course.academy.model.User;
import course.academy.model.dto.ProjectDto;
import course.academy.service.ProjectService;
import course.academy.service.TaskService;
import course.academy.service.UserService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class ProjectMapper {
    private UserService userService;
    private ProjectService projectService;
    private TaskService taskService;

    public ProjectMapper(UserService userService, ProjectService projectService, TaskService taskService) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    public Project fromDto(ProjectDto dto) throws NonexistingEntityException {
        Project project = new Project();
        dtoToObject(dto, project);
        return project;
    }

    public Project fromDto(ProjectDto dto, long id) throws NonexistingEntityException {
        Project project = projectService.getById(id);
        dtoToObject(dto, project);
        return project;
    }


    private void dtoToObject(ProjectDto dto, Project project) throws NonexistingEntityException {
        User owner = userService.getById(dto.getOwnerId());
        project.setOwner(owner);
        project.setTitle(dto.getTitle());
        project.setStartDate(dto.getStartDate());
        project.setTags(dto.getTags());
        project.setDescription(dto.getDescription());
        Set<User> developers = new HashSet<>();
        if (dto.getDeveloperIds() != null) {
            for (long id : dto.getDeveloperIds()) {
                developers.add(userService.getById(id));
            }
        }
        project.setDevelopers(developers);
    }

}
