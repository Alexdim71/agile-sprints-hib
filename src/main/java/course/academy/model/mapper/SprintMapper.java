package course.academy.model.mapper;

import course.academy.model.Project;
import course.academy.model.Sprint;
import course.academy.model.User;
import course.academy.model.dto.SprintDisplayDto;
import course.academy.model.dto.SprintDto;
import course.academy.model.dto.SprintUpdateDto;
import course.academy.service.ProjectService;
import course.academy.service.SprintService;
import course.academy.service.UserService;
import org.springframework.stereotype.Component;


import java.util.HashSet;
import java.util.Set;

@Component
public class SprintMapper {
    private UserService userService;
    private ProjectService projectService;
    private SprintService sprintService;

    public SprintMapper(UserService userService, ProjectService projectService, SprintService sprintService) {
        this.userService = userService;
        this.projectService = projectService;
        this.sprintService = sprintService;
    }

    public Sprint fromDto(SprintDto dto) {
        Sprint sprint = new Sprint();
        dtoToObject(dto, sprint);
        return sprint;
    }

    public Sprint fromDto(SprintDto dto, long id) {
        Sprint sprint = sprintService.getById(id);
        dtoToObject(dto, sprint);
        return sprint;
    }

    public Sprint fromUpdateDto(SprintUpdateDto dto, long id) {
        Sprint sprint = sprintService.getById(id);
        updateDtoToObject(dto, sprint);
        return sprint;
    }

    private void dtoToObject(SprintDto dto, Sprint sprint) {
        Project project = projectService.getById(dto.getProjectId());
        sprint.setProject(project);
        sprint.setStartDate(dto.getStartDate());
        sprint.setEndDate(dto.getStartDate().plusDays(dto.getDuration()-1));
        sprint.setDuration(dto.getDuration());
        Set<User> developers = new HashSet<>();
        if (dto.getDeveloperIds() != null) {
            for (long id : dto.getDeveloperIds()) {
                developers.add(userService.getById(id));
            }
        }
        sprint.setDevelopers(developers);
    }

    private void updateDtoToObject(SprintUpdateDto dto, Sprint sprint) {
        sprint.setProject(sprint.getProject());
        sprint.setStartDate(dto.getStartDate());
        sprint.setEndDate(dto.getStartDate().plusDays(dto.getDuration()-1));
        sprint.setDuration(dto.getDuration());
        sprint.setDevelopers(sprint.getDevelopers());
    }

    public SprintDisplayDto sprintToDisplayDto(Sprint sprint) {
        SprintDisplayDto dto = new SprintDisplayDto();
        dto.setId(sprint.getId());
        dto.setProjectId(sprint.getProject().getId());
        dto.setStartDate(sprint.getStartDate());
        dto.setDuration(sprint.getDuration());
        dto.setEndDate(sprint.getEndDate());
        dto.setOwnerId(sprint.getOwner().getId());
        dto.setCreated(sprint.getCreated());
        dto.setModified(sprint.getModified());
        return dto;
    }

    }

