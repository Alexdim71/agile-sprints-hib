package course.academy.model.mapper;

import course.academy.dao.KindRepository;
import course.academy.model.Kind;
import course.academy.model.Project;
import course.academy.model.Task;
import course.academy.model.dto.TaskDisplayDto;
import course.academy.model.dto.TaskDto;
import course.academy.model.dto.TaskUpdateDto;
import course.academy.service.ProjectService;
import course.academy.service.TaskService;
import org.springframework.stereotype.Component;

@Component
public class TaskMapper {
    private TaskService taskService;
    private ProjectService projectService;
    private KindRepository kindRepository;


    public TaskMapper(TaskService taskService, ProjectService projectService, KindRepository kindRepository) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.kindRepository = kindRepository;
    }


    public Task fromDto(TaskDto dto) {
        Task task = new Task();
        dtoToObject(dto, task);
        return task;
    }

    public Task fromDto(TaskDto dto, long id) {
        Task task = taskService.getById(id);
        dtoToObject(dto, task);
        return task;
    }

    public Task fromUpdateDto(TaskUpdateDto dto, long id) {
        Task task = taskService.getById(id);
        updateDtoToObject(dto, task);
        return task;
    }


    private void dtoToObject(TaskDto dto, Task task) {
        Project project = projectService.getById(dto.getProjectId());
        Kind kind = kindRepository.getById(dto.getKindId());
        task.setProject(project);
        task.setKind(kind);
        task.setTitle(dto.getTitle());
        task.setEstimatedEffort(dto.getEstimatedEffort());
        task.setTags(dto.getTags());
        task.setDescription(dto.getDescription());
    }

    private void updateDtoToObject(TaskUpdateDto dto, Task task) {
        Kind kind = kindRepository.getById(dto.getKindId());
        task.setProject(task.getProject());
        task.setKind(kind);
        task.setTitle(dto.getTitle());
        task.setEstimatedEffort(dto.getEstimatedEffort());
        task.setTags(dto.getTags());
        task.setDescription(dto.getDescription());
    }

    public TaskDisplayDto taskToDisplayDto(Task task) {
        TaskDisplayDto dto = new TaskDisplayDto();
        dto.setId(task.getId());
        dto.setProjectId(task.getProject().getId());
        dto.setKind(task.getKind());
        dto.setStatusTask(task.getStatusTask());
        dto.setTitle(task.getTitle());
        dto.setAddedById(task.getAddedBy().getId());
        dto.setEstimatedEffort(task.getEstimatedEffort());
        dto.setSprintId(task.getSprint().getId());
        dto.setDescription(task.getDescription());
        dto.setTags(task.getTags());
        dto.setCreated(task.getCreated());
        dto.setModified(task.getModified());
        return dto;
    }





}
