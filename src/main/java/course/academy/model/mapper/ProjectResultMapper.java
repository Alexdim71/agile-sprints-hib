package course.academy.model.mapper;

import course.academy.model.Project;
import course.academy.model.ProjectResult;
import course.academy.model.dto.ProjectResultDto;
import course.academy.model.dto.ProjectResultUpdateDto;
import course.academy.service.ProjectResultService;
import course.academy.service.ProjectService;
import org.springframework.stereotype.Component;

@Component
public class ProjectResultMapper {
    private ProjectResultService projectResultService;
    private ProjectService projectService;

    public ProjectResultMapper(ProjectResultService projectResultService, ProjectService projectService) {
        this.projectResultService = projectResultService;
        this.projectService = projectService;
    }

    public ProjectResult fromDto(ProjectResultDto dto) {
        ProjectResult projectResult = new ProjectResult();
        dtoToObject(dto, projectResult);
        return projectResult;
    }

    public ProjectResult fromDto(ProjectResultDto dto, long id) {
        ProjectResult projectResult = projectResultService.getById(id);
        dtoToObject(dto, projectResult);
        return projectResult;
    }

    public ProjectResult fromUpdateDto(ProjectResultUpdateDto dto, long id) {
        ProjectResult projectResult = projectResultService.getById(id);
        updateDtoToObject(dto, projectResult);
        return projectResult;
    }

    private void dtoToObject(ProjectResultDto dto, ProjectResult projectResult) {
        Project project = projectService.getById(dto.getProjectId());
        projectResult.setProject(project);
        projectResult.setEndDate(dto.getEndDate());
        projectResult.setDuration(dto.getEndDate());
        projectResult.setResultsDescription(dto.getResultsDescription());
    }

    private void updateDtoToObject(ProjectResultUpdateDto dto, ProjectResult projectResult) {
        projectResult.setProject(projectResult.getProject());
        projectResult.setEndDate(dto.getEndDate());
        projectResult.setDuration(dto.getEndDate());
        projectResult.setResultsDescription(dto.getResultsDescription());
    }

}
