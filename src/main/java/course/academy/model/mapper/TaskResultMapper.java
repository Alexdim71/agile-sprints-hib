package course.academy.model.mapper;

import course.academy.model.Task;
import course.academy.model.TaskResult;
import course.academy.model.dto.TaskDisplayDto;
import course.academy.model.dto.TaskResultDisplayDto;
import course.academy.model.dto.TaskResultDto;
import course.academy.model.dto.TaskResultUpdateDto;
import course.academy.service.TaskResultService;
import course.academy.service.TaskService;
import org.springframework.stereotype.Component;

@Component
public class TaskResultMapper {
    private TaskService taskService;
    private TaskResultService taskResultService;

    public TaskResultMapper(TaskService taskService, TaskResultService taskResultService) {
        this.taskService = taskService;
        this.taskResultService = taskResultService;
    }

    public TaskResult fromDto(TaskResultDto dto) {
        TaskResult taskResult = new TaskResult();
        dtoToObject(dto, taskResult);
        return taskResult;
    }

    public TaskResult fromDto(TaskResultDto dto, long id) {
        TaskResult taskResult = taskResultService.getById(id);
        dtoToObject(dto, taskResult);
        return taskResult;
    }

    public TaskResult fromUpdateDto(TaskResultUpdateDto dto, long id) {
        TaskResult taskResult = taskResultService.getById(id);
        updateDtoToObject(dto, taskResult);
        return taskResult;
    }

    private void dtoToObject(TaskResultDto dto, TaskResult taskResult)  {
       Task task = taskService.getById(dto.getTaskId());
       taskResult.setTask(task);
       taskResult.setActualEffort(dto.getActualEffort());
       taskResult.setResultDescription(dto.getResultDescription());
    }

    private void updateDtoToObject(TaskResultUpdateDto dto, TaskResult taskResult) {
        taskResult.setTask(taskResult.getTask());
        taskResult.setActualEffort(dto.getActualEffort());
        taskResult.setResultDescription(dto.getResultDescription());
    }

    public TaskResultDisplayDto taskResultToDisplayDto(TaskResult taskResult) {
        TaskResultDisplayDto dto = new TaskResultDisplayDto();
        dto.setId(taskResult.getId());
        dto.setTaskId(taskResult.getTask().getId());
        dto.setProjectId(taskResult.getTask().getProject().getId());
        dto.setSprintId(taskResult.getTask().getSprint().getId());
        dto.setActualEffort(taskResult.getActualEffort());
        dto.setVerifiedBy(taskResult.getVerifiedBy().getId());
        dto.setResultDescription(taskResult.getResultDescription());
        dto.setCreated(taskResult.getCreated());
        dto.setModified(taskResult.getModified());
        return dto;
    }

}
