package course.academy.model.mapper;

import course.academy.dao.RoleRepository;
import course.academy.dao.StatusUserRepository;
import course.academy.model.Role;
import course.academy.model.StatusUser;
import course.academy.model.User;
import course.academy.model.dto.UserDto;
import course.academy.model.dto.UserManagementDto;
import course.academy.model.dto.UserRestRegisterDto;
import course.academy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class UserMapper {
    private UserService userService;
    private StatusUserRepository statusUserRepository;
    private RoleRepository roleRepository;

    @Autowired
    public UserMapper(UserService userService, StatusUserRepository statusUserRepository, RoleRepository roleRepository) {
        this.userService = userService;
        this.statusUserRepository = statusUserRepository;
        this.roleRepository = roleRepository;
    }

    public UserDto userToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setContacts(user.getContacts());
        return userDto;
    }

    public UserManagementDto userToManagementDto(User user) {
        UserManagementDto userDto = new UserManagementDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setContacts(user.getContacts());
        userDto.setRoleIds(user.getRoles().stream().map(Role::getId).collect(Collectors.toSet()));
        userDto.setStatusUserId(user.getStatusUser().getId());
        return userDto;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    public User fromDto(UserDto userDto, long id) {
        User user = userService.getById(id);
        dtoToObject(userDto, user);
        return user;
    }

    public User fromManagementDto(UserManagementDto userManagementDto, long id)  {
        User user = userService.getById(id);
        managementDtoToObject(userManagementDto, user);
        return user;
    }

    private void dtoToObject(UserDto userDto, User user) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setContacts(userDto.getContacts());
        user.setUsername(user.getUsername());
        user.setPassword(user.getPassword());
        user.setStatusUser(user.getStatusUser());
    }



    private void managementDtoToObject(UserManagementDto userManagementDto, User user) {
        StatusUser statusUser = statusUserRepository.getById(userManagementDto.getStatusUserId());
        var roles = userManagementDto.getRoleIds().stream().map(id -> roleRepository.getById(id)).collect(Collectors.toSet());
        user.setFirstName(userManagementDto.getFirstName());
        user.setLastName(userManagementDto.getLastName());
        user.setEmail(userManagementDto.getEmail());
        user.setContacts(userManagementDto.getContacts());
        user.setStatusUser(statusUser);
        user.setRoles(roles);
        user.setUsername(user.getUsername());
        user.setPassword(user.getPassword());
    }


    public User createUserFromRestRegisterDto(UserRestRegisterDto dto) {
        User user = new User();
        restRegisterDtoToObject(dto, user);
        return user;
    }

    private void restRegisterDtoToObject(UserRestRegisterDto dto, User user) {
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());
        user.setUsername(dto.getUsername());
        user.setPassword(dto.getPassword());
        user.setContacts(dto.getContacts());
    }

}
