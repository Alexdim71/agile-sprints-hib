package course.academy.model;

import course.academy.utils.ParsingHelpers;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

import static course.academy.utils.ParsingHelpers.dtf;


@Entity
@Table(name = "sprint_results")
public class SprintResult implements Identifiable<Long>, Serializable {
    public static final int RESULTS_DESCRIPTION_MIN_LENGTH = 10;
    public static final int RESULTS_DESCRIPTION_MAX_LENGTH = 2500;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "sprint_id")
    private Sprint sprint;

    @Column(name = "team_velocity")
    private int teamVelocity;

    @Column(name = "results_description")
    private String resultsDescription;

//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(
//            name = "sprints_task_results",
//            joinColumns = @JoinColumn(name = "sprint_id"),
//            inverseJoinColumns = @JoinColumn(name = "task_result_id"))
//    private Set<TaskResult> taskResults = new HashSet<>();

    @Column(name = "created")
    private final LocalDateTime created = LocalDateTime.now();

    @Column(name = "modified")
    private LocalDateTime modified = LocalDateTime.now();

    public SprintResult() {
    }

    public SprintResult(Sprint sprint) {
        this.sprint = sprint;
    }

    public SprintResult(Sprint sprint, String resultsDescription) {
        this.sprint = sprint;
        this.resultsDescription = resultsDescription;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public int getTeamVelocity() {
        return teamVelocity;
    }

    public void setTeamVelocity(int teamVelocity) {
        this.teamVelocity = teamVelocity;
    }

    public String getResultsDescription() {
        return resultsDescription;
    }

    public void setResultsDescription(String resultsDescription) {
        this.resultsDescription = resultsDescription;
    }

//    public Set<TaskResult> getTaskResults() {
//        return taskResults;
//    }
//
//    public void setTaskResults(Set<TaskResult> taskResults) {
//        this.taskResults = taskResults;
//    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SprintResult)) return false;

        SprintResult that = (SprintResult) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder("SprintResult {");
        sb.append("id=").append(id);
        sb.append(", sprint={id=").append(sprint.getId()).append(", end date=").append(sprint.getEndDate().format(ParsingHelpers.df)).append("}");
        sb.append(", teamVelocity=").append(teamVelocity).append(" effort units per day,");
        sb.append(System.lineSeparator()).append("task results: ");
        for (TaskResult result : getSprint().getCompletedTaskResults()) {
            sb.append("{id=").append(result.getId()).append(", task title='").append(result.getTask().getTitle()).append("'")
                    .append(", actual effort=").append(result.getActualEffort()).append(" effort units").append("}, ");
        }
        sb.append(System.lineSeparator()).append("created=").append(created.format(dtf));
        sb.append(", modified=").append(modified.format(dtf));
        sb.append('}');
        return sb.toString();
    }
}
