package course.academy;

import course.academy.model.mapper.*;
import course.academy.service.*;
import course.academy.service.impl.ServiceFactoryImpl;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import course.academy.controller.console.LoginController;
import course.academy.dao.*;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class SpringBootConsoleApplication implements CommandLineRunner {

    final UserRepository userRepository;
    final ProjectRepository projectRepository;
    final ProjectResultRepository projectResultRepository;
    final TaskRepository taskRepository;
    final TaskResultRepository taskResultRepository;
    final SprintRepository sprintRepository;
    final SprintResultRepository sprintResultRepository;
    final KindRepository kindRepository;
    final RoleRepository roleRepository;
    final StatusUserRepository statusUserRepository;
    final StatusTaskRepository statusTaskRepository;

    public SpringBootConsoleApplication(ProjectRepository projectRepository, UserRepository userRepository, ProjectResultRepository projectResultRepository, TaskRepository taskRepository, TaskResultRepository taskResultRepository, SprintRepository sprintRepository, SprintResultRepository sprintResultRepository, KindRepository kindRepository, RoleRepository roleRepository, StatusUserRepository statusUserRepository, StatusTaskRepository statusTaskRepository) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
        this.projectResultRepository = projectResultRepository;
        this.taskRepository = taskRepository;
        this.taskResultRepository = taskResultRepository;
        this.sprintRepository = sprintRepository;
        this.sprintResultRepository = sprintResultRepository;
        this.kindRepository = kindRepository;
        this.roleRepository = roleRepository;
        this.statusUserRepository = statusUserRepository;
        this.statusTaskRepository = statusTaskRepository;
    }


    public static void main(String[] args) throws Exception {

        SpringApplication app = new SpringApplication(SpringBootConsoleApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);

    }

    @Override
    public void run(String... args) throws Exception {

        ServiceFactory serviceFactory = new ServiceFactoryImpl();
        UserService userService = serviceFactory.createUserService(userRepository, statusUserRepository, roleRepository);
        ProjectService projectService = serviceFactory.createProjectService(projectRepository, userRepository,
                taskRepository, statusTaskRepository);
        TaskService taskService = serviceFactory.createTaskService(taskRepository, projectRepository, statusTaskRepository);
        TaskResultService taskResultService = serviceFactory.createTaskResultService(taskResultRepository, sprintRepository, userRepository,
                taskRepository, statusTaskRepository);
        SprintService sprintService = serviceFactory.createSprintService(sprintRepository, taskRepository, statusTaskRepository, userRepository, projectRepository);
        SprintResultService sprintResultService = serviceFactory.createSprintResultService(sprintResultRepository, projectRepository);
        ProjectResultService projectResultService = serviceFactory.createProjectResultService(projectResultRepository, userRepository);

        UserMapper userMapper = new UserMapper(userService, statusUserRepository, roleRepository);
        var projectMapper = new ProjectMapper(userService, projectService, taskService);
        var taskMapper = new TaskMapper(taskService, projectService, kindRepository);
        var sprintMapper = new SprintMapper(userService, projectService, sprintService);
        var projectResultMapper = new ProjectResultMapper(projectResultService, projectService);
        var taskResultMapper = new TaskResultMapper(taskService, taskResultService);
        var sprintResultMapper = new SprintResultMapper(sprintResultService, sprintService, sprintMapper);

        var loginController = new LoginController(userService, userMapper, projectService, projectMapper,
                taskService, taskMapper, sprintService, sprintMapper, projectResultService, projectResultMapper,
                taskResultService, taskResultMapper, sprintResultService, sprintResultMapper);

        loginController.init();


    }

}


