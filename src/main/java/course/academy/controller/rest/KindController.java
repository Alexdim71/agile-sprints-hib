package course.academy.controller.rest;

import course.academy.dao.KindRepository;
import course.academy.model.Kind;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/api/kinds")
public class KindController {

    private final KindRepository kindRepository;
    private final AuthenticationHelper authenticationHelper;


    public KindController(KindRepository kindRepository, AuthenticationHelper authenticationHelper) {
        this.kindRepository = kindRepository;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public Collection<Kind> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return kindRepository.getAll();
    }


}
