package course.academy.controller.rest;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.SprintResult;
import course.academy.model.User;
import course.academy.model.dto.SprintResultDisplayDto;
import course.academy.model.dto.SprintResultDto;
import course.academy.model.dto.SprintResultUpdateDto;
import course.academy.model.mapper.SprintResultMapper;
import course.academy.service.SprintResultService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/sprint-results")
public class SprintResultRestController {

    private final SprintResultService sprintResultService;
    private final SprintResultMapper sprintResultMapper;
    private final AuthenticationHelper authenticationHelper;

    public SprintResultRestController(SprintResultService sprintResultService, SprintResultMapper sprintResultMapper,
                                      AuthenticationHelper authenticationHelper) {
        this.sprintResultService = sprintResultService;
        this.sprintResultMapper = sprintResultMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("{id}")
    public SprintResult getById(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return sprintResultService.getById(id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    public Collection<SprintResultDisplayDto> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return sprintResultService.getAll().stream()
                .map(sprintResultMapper::sprintResultToDisplayDto)
                .collect(Collectors.toList());
    }

    @PostMapping
    public SprintResult create(@RequestHeader HttpHeaders headers, @Valid @RequestBody SprintResultDto dto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            SprintResult sprintResult = sprintResultMapper.fromDto(dto);
            sprintResultService.add(loggedUser, sprintResult);
            return sprintResult;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public SprintResult update(@RequestHeader HttpHeaders headers, @PathVariable long id, @Valid @RequestBody SprintResultUpdateDto dto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            SprintResult sprintResult = sprintResultMapper.fromUpdateDto(dto, id);
            sprintResultService.update(loggedUser, sprintResult);
            return sprintResult;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            sprintResultService.deleteById(loggedUser, id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}