package course.academy.controller.rest;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.ProjectResult;
import course.academy.model.User;
import course.academy.model.dto.ProjectResultDto;
import course.academy.model.dto.ProjectResultUpdateDto;
import course.academy.model.mapper.ProjectResultMapper;
import course.academy.service.ProjectResultService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/api/project-results")
public class ProjectResultRestController {

    private final ProjectResultService projectResultService;
    private final ProjectResultMapper projectResultMapper;
    private final AuthenticationHelper authenticationHelper;

    public ProjectResultRestController(ProjectResultService projectResultService, ProjectResultMapper projectResultMapper,
                                       AuthenticationHelper authenticationHelper) {
        this.projectResultService = projectResultService;
        this.projectResultMapper = projectResultMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("{id}")
    public ProjectResult getById(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return projectResultService.getById(id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    public Collection<ProjectResult> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return projectResultService.getAll();
    }

    @PostMapping
    public ProjectResult create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ProjectResultDto dto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            ProjectResult projectResult = projectResultMapper.fromDto(dto);
            projectResultService.add(loggedUser, projectResult);
            return projectResult;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ProjectResult update(@RequestHeader HttpHeaders headers, @PathVariable long id, @Valid @RequestBody ProjectResultUpdateDto dto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            ProjectResult projectResult = projectResultMapper.fromUpdateDto(dto, id);
            projectResultService.update(loggedUser, projectResult);
            return projectResult;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            projectResultService.deleteById(loggedUser, id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}