package course.academy.controller.rest;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Task;
import course.academy.model.User;
import course.academy.model.dto.TaskDisplayDto;
import course.academy.model.dto.TaskDto;
import course.academy.model.dto.TaskUpdateDto;
import course.academy.model.mapper.TaskMapper;
import course.academy.service.TaskService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestController {

    private final TaskService taskService;
    private final TaskMapper taskMapper;
    private final AuthenticationHelper authenticationHelper;

    public TaskRestController(TaskService taskService, TaskMapper taskMapper, AuthenticationHelper authenticationHelper) {
        this.taskService = taskService;
        this.taskMapper = taskMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("{id}")
    public Task getById(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return taskService.getById(id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("{id}/developers")
    public Collection<User> getDevelopers(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        return getById(headers, id).getDevelopersAssigned();
    }

    @GetMapping
    public Collection<TaskDisplayDto> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return taskService.getAll().stream().map(taskMapper::taskToDisplayDto)
                .collect(Collectors.toList());
    }

    @GetMapping("search")
    public Collection<TaskDisplayDto> searchTask(@RequestHeader HttpHeaders headers, @RequestParam String search) {
        authenticationHelper.tryGetUser(headers);
        return taskService.searchTask(search).stream()
                .map(taskMapper::taskToDisplayDto)
                .collect(Collectors.toList());
    }

    @GetMapping("filter")
    public List<TaskDisplayDto> filterTasks(@RequestHeader HttpHeaders headers, @RequestParam(required = false) Optional<String> title,
                                            Optional<Long> projectId, Optional<String> kind, Optional<String> status,
                                            Optional<Integer> minEstimatedEffort, Optional<String> sortBy) {
        authenticationHelper.tryGetUser(headers);
        try {
            return taskService.filterTasks(title, projectId, kind, status, minEstimatedEffort, sortBy).stream()
                    .map(taskMapper::taskToDisplayDto)
                    .collect(Collectors.toList());
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping
    public Task create(@RequestHeader HttpHeaders headers, @Valid @RequestBody TaskDto dto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Task task = taskMapper.fromDto(dto);
            taskService.add(loggedUser, task);
            return task;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Task update(@RequestHeader HttpHeaders headers, @PathVariable long id, @Valid @RequestBody TaskUpdateDto dto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Task task = taskMapper.fromUpdateDto(dto, id);
            taskService.update(loggedUser, task);
            return task;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            taskService.deleteById(loggedUser, id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
