package course.academy.controller.rest;

import course.academy.dao.StatusTaskRepository;
import course.academy.model.StatusTask;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/api/task-statuses")
public class TaskStatusController {

    private final StatusTaskRepository statusTaskRepository;
    private final AuthenticationHelper authenticationHelper;


    public TaskStatusController(StatusTaskRepository statusTaskRepository, AuthenticationHelper authenticationHelper) {
        this.statusTaskRepository = statusTaskRepository;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public Collection<StatusTask> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return statusTaskRepository.getAll();
    }


}
