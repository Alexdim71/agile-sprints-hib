package course.academy.controller.rest;

import course.academy.dao.RoleRepository;
import course.academy.model.Role;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api/roles")
public class RoleController {

    private final RoleRepository roleRepository;
    private final AuthenticationHelper authenticationHelper;


    public RoleController(RoleRepository roleRepository, AuthenticationHelper authenticationHelper) {
        this.roleRepository = roleRepository;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public Collection<Role> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return roleRepository.getAll();
    }


}
