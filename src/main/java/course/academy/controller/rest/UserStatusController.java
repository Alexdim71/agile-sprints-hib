package course.academy.controller.rest;

import course.academy.dao.StatusTaskRepository;
import course.academy.dao.StatusUserRepository;
import course.academy.model.StatusTask;
import course.academy.model.StatusUser;
import course.academy.model.User;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@RestController
@RequestMapping("/api/user-statuses")
public class UserStatusController {

    private final StatusUserRepository statusUserRepository;
    private final AuthenticationHelper authenticationHelper;


    public UserStatusController(StatusUserRepository statusUserRepository, AuthenticationHelper authenticationHelper) {
        this.statusUserRepository = statusUserRepository;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public Collection<StatusUser> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        if (!user.isAdmin()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "The requested resource requires admin authorisation.");
        }
        return statusUserRepository.getAll();
    }


}
