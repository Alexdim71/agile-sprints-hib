package course.academy.controller.rest;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.model.dto.*;
import course.academy.model.mapper.SprintMapper;
import course.academy.model.mapper.TaskMapper;
import course.academy.model.mapper.TaskResultMapper;
import course.academy.service.SprintService;
import course.academy.service.TaskService;
import course.academy.service.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/sprints")
public class SprintRestController {

    private final SprintService sprintService;
    private final SprintMapper sprintMapper;
    private final UserService userService;
    private final TaskService taskService;
    private final TaskMapper taskMapper;
    private final TaskResultMapper taskResultMapper;
    private final AuthenticationHelper authenticationHelper;

    public SprintRestController(SprintService sprintService, SprintMapper sprintMapper, UserService userService,
                                TaskService taskService, TaskMapper taskMapper, TaskResultMapper taskResultMapper,
                                AuthenticationHelper authenticationHelper) {
        this.sprintService = sprintService;
        this.sprintMapper = sprintMapper;
        this.userService = userService;
        this.taskService = taskService;
        this.taskMapper = taskMapper;
        this.taskResultMapper = taskResultMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("{id}")
    public Sprint getById(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return sprintService.getById(id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("{id}/developers")
    public Collection<User> getDevelopers(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        return getById(headers, id).getDevelopers();
    }

    @GetMapping("{id}/tasks")
    public Collection<TaskDisplayDto> getTasks(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        return getById(headers, id).getTasks().stream()
                .map(taskMapper::taskToDisplayDto)
                .collect(Collectors.toList());
    }

    @GetMapping("{id}/task-results")
    public Collection<TaskResultDisplayDto> getTaskResults(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        return getById(headers, id).getCompletedTaskResults().stream()
                .map(taskResultMapper::taskResultToDisplayDto)
                .collect(Collectors.toList());
    }

    @GetMapping("{id}/project")
    public Project getProject(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        return getById(headers, id).getProject();
    }

    @GetMapping
    public Collection<SprintDisplayDto> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return sprintService.getAll().stream()
                .map(sprintMapper::sprintToDisplayDto)
                .collect(Collectors.toList());
    }

    @PostMapping
    public Sprint create(@RequestHeader HttpHeaders headers, @Valid @RequestBody SprintDto dto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Sprint sprint = sprintMapper.fromDto(dto);
            sprintService.add(loggedUser, sprint);
            return sprint;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Sprint update(@RequestHeader HttpHeaders headers, @PathVariable long id, @Valid @RequestBody SprintUpdateDto dto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Sprint sprint = sprintMapper.fromUpdateDto(dto, id);
            sprintService.update(loggedUser, sprint);
            return sprint;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            sprintService.deleteById(loggedUser, id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}/developers/add/{userId}")
    public User addDeveloper(@RequestHeader HttpHeaders headers, @PathVariable long id, @PathVariable long userId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Sprint sprint = sprintService.getById(id);
            User developerToAdd = userService.getById(userId);
            sprintService.addDeveloperToSprint(loggedUser, sprint, developerToAdd);
            return developerToAdd;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/developers/remove/{userId}")
    public User removeDeveloper(@RequestHeader HttpHeaders headers, @PathVariable long id, @PathVariable long userId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Sprint sprint = sprintService.getById(id);
            User developerToRemove = userService.getById(userId);
            sprintService.removeDeveloperFromSprint(loggedUser, sprint, developerToRemove);
            return developerToRemove;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/tasks/add/{taskId}")
    public Task addTask(@RequestHeader HttpHeaders headers, @PathVariable long id, @PathVariable long taskId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Sprint sprint = sprintService.getById(id);
            Task taskToAdd = taskService.getById(taskId);
            sprintService.addTaskToSprint(loggedUser, sprint, taskToAdd);
            return taskToAdd;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/tasks/remove/{taskId}")
    public Task removeTask(@RequestHeader HttpHeaders headers, @PathVariable long id, @PathVariable long taskId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Sprint sprint = sprintService.getById(id);
            Task taskToRemove = taskService.getById(taskId);
            sprintService.removeTaskFromSprint(loggedUser, sprint, taskToRemove);
            return taskToRemove;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}