package course.academy.controller.rest;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.TaskResult;
import course.academy.model.User;
import course.academy.model.dto.TaskResultDisplayDto;
import course.academy.model.dto.TaskResultDto;
import course.academy.model.dto.TaskResultUpdateDto;
import course.academy.model.mapper.TaskResultMapper;
import course.academy.service.TaskResultService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/task-results")
public class TaskResultRestController {

    private final TaskResultService taskResultService;
    private final TaskResultMapper taskResultMapper;
    private final AuthenticationHelper authenticationHelper;

    public TaskResultRestController(TaskResultService taskResultService, TaskResultMapper taskResultMapper,
                                    AuthenticationHelper authenticationHelper) {
        this.taskResultService = taskResultService;
        this.taskResultMapper = taskResultMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("{id}")
    public TaskResult getById(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return taskResultService.getById(id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    public Collection<TaskResultDisplayDto> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return taskResultService.getAll().stream()
                .map(taskResultMapper::taskResultToDisplayDto)
                .collect(Collectors.toList());
    }

    @PostMapping
    public TaskResult create(@RequestHeader HttpHeaders headers, @Valid @RequestBody TaskResultDto dto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            TaskResult taskResult = taskResultMapper.fromDto(dto);
            taskResultService.add(loggedUser, taskResult);
            return taskResult;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public TaskResult update(@RequestHeader HttpHeaders headers, @PathVariable long id, @Valid @RequestBody TaskResultUpdateDto dto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            TaskResult taskResult = taskResultMapper.fromUpdateDto(dto, id);
            taskResultService.update(loggedUser,taskResult);
            return taskResult;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            taskResultService.deleteById(loggedUser, id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
