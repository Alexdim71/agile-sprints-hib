package course.academy.controller.rest;

import course.academy.dao.RoleRepository;
import course.academy.dao.StatusUserRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.model.dto.*;
import course.academy.model.mapper.TaskMapper;
import course.academy.model.mapper.TaskResultMapper;
import course.academy.model.mapper.UserMapper;
import course.academy.service.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final RoleRepository roleRepository;
    private final StatusUserRepository statusUserRepository;
    private final TaskMapper taskMapper;
    private final TaskResultMapper taskResultMapper;
    private final AuthenticationHelper authenticationHelper;

    public UserRestController(UserService userService, UserMapper userMapper,
                              RoleRepository roleRepository, StatusUserRepository statusUserRepository,
                              TaskMapper taskMapper, TaskResultMapper taskResultMapper,
                              AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.roleRepository = roleRepository;
        this.statusUserRepository = statusUserRepository;
        this.taskMapper = taskMapper;
        this.taskResultMapper = taskResultMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("{id}")
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return userService.getById(id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("{id}/tasks")
    public Collection<TaskDisplayDto> getTasks(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        return getById(headers, id).getAssignedTasks().stream()
                .map(taskMapper::taskToDisplayDto)
                .collect(Collectors.toList());
    }

    @GetMapping("{id}/task-results")
    public Collection<TaskResultDisplayDto> getTaskResults(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        return getById(headers, id).getCompletedTaskResults().stream()
                .map(taskResultMapper::taskResultToDisplayDto)
                .collect(Collectors.toList());
    }

    @GetMapping("{id}/projects")
    public Collection<Project> getProject(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        return getById(headers, id).getProjects();
    }

    @GetMapping("{id}/project-results")
    public Collection<ProjectResult> getProjectResults(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        return getById(headers, id).getCompletedProjectResult();
    }

    @GetMapping
    public Collection<User> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return userService.getAll();
    }

    @GetMapping("/search")
    public Collection<User> searchByName(@RequestHeader HttpHeaders headers, @RequestParam String name) {
        authenticationHelper.tryGetUser(headers);
        return userService.searchByName(name);
    }



    @PostMapping
    public User create(@Valid @RequestBody UserRestRegisterDto dto) {
        try {
            User user = userMapper.createUserFromRestRegisterDto(dto);
            userService.add(user);
            return user;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers, @PathVariable long id, @Valid @RequestBody UserDto userDto) {
        try {
            User user = userMapper.fromDto(userDto, id);
            User loggedUser = authenticationHelper.tryGetUser(headers);
            userService.update(loggedUser, user);
            return user;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            userService.deleteById(loggedUser, id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/role/add/{roleId}")
    public User addRole(@RequestHeader HttpHeaders headers, @PathVariable long id, @PathVariable long roleId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            User user = userService.getById(id);
            Role role = roleRepository.getById(roleId);
            userService.addUserRole(loggedUser, user, role);
            return user;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/role/remove/{roleId}")
    public User removeRole(@RequestHeader HttpHeaders headers, @PathVariable long id, @PathVariable long roleId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            User user = userService.getById(id);
            Role role = roleRepository.getById(roleId);
            userService.removeUserRole(loggedUser, user, role);
            return user;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/status/{statusId}")
    public User setStatus(@RequestHeader HttpHeaders headers, @PathVariable long id, @PathVariable long statusId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            User user = userService.getById(id);
            StatusUser status = statusUserRepository.getById(statusId);
            userService.setUserStatus(loggedUser, user, status);
            return user;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("admin/{id}")
    public User updateByAdmin(@RequestHeader HttpHeaders headers, @PathVariable long id, @Valid @RequestBody UserManagementDto userDto) {
        try {
            User user = userMapper.fromManagementDto(userDto, id);
            User loggedUser = authenticationHelper.tryGetUser(headers);
            userService.updateByAdmin(loggedUser, user);
            return user;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}

//    @PutMapping("/{id}/photo")
//    public User uploadPhoto(@RequestHeader HttpHeaders headers, @PathVariable long id,
//                            @RequestParam(value = "file", required = false) MultipartFile multipartFile
//    ) throws IOException {
//        try {
//            User loggedUser = authenticationHelper.tryGetUser(headers);
//            User user = userService.getById(id);
//            if (!multipartFile.isEmpty()) {
//                String fileName = StringUtils.cleanPath(Objects.requireNonNull(multipartFile.getOriginalFilename()));
//                user.setPhotoName(fileName);
//                String uploadDir = "src/main/resources/user-photos/" + user.getId();
//                FileUploadHelper.saveFile(uploadDir, fileName, multipartFile);
//            }
//            userService.update(user, loggedUser);
//            return user;
//        } catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }
//    }