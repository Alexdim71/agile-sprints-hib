package course.academy.controller.rest;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.model.dto.ProjectDto;
import course.academy.model.dto.SprintDisplayDto;
import course.academy.model.dto.SprintResultDisplayDto;
import course.academy.model.dto.TaskDisplayDto;
import course.academy.model.mapper.ProjectMapper;
import course.academy.model.mapper.SprintMapper;
import course.academy.model.mapper.SprintResultMapper;
import course.academy.model.mapper.TaskMapper;
import course.academy.service.ProjectService;
import course.academy.service.SprintService;
import course.academy.service.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestController {

    private final ProjectService projectService;
    private final ProjectMapper projectMapper;
    private final UserService userService;
    private final SprintService sprintService;
    private final TaskMapper taskMapper;
    private final SprintMapper sprintMapper;
    private final SprintResultMapper sprintResultMapper;
    private final AuthenticationHelper authenticationHelper;

    public ProjectRestController(ProjectService projectService, ProjectMapper projectMapper, UserService userService,
                                 SprintService sprintService, TaskMapper taskMapper, SprintMapper sprintMapper,
                                 SprintResultMapper sprintResultMapper, AuthenticationHelper authenticationHelper) {
        this.projectService = projectService;
        this.projectMapper = projectMapper;
        this.userService = userService;
        this.sprintService = sprintService;
        this.taskMapper = taskMapper;
        this.sprintMapper = sprintMapper;
        this.sprintResultMapper = sprintResultMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("{id}")
    public Project getById(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return projectService.getById(id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("{id}/developers")
    public Collection<User> getDevelopers (@RequestHeader HttpHeaders headers, @PathVariable long id) {
        return getById(headers, id).getDevelopers();
    }

    @GetMapping("{id}/tasks")
    public Collection<TaskDisplayDto> getTasks (@RequestHeader HttpHeaders headers, @PathVariable long id) {
        return getById(headers, id).getTasksBacklog().stream()
                .map(taskMapper::taskToDisplayDto)
                .collect(Collectors.toList());
    }

    @GetMapping("{id}/sprintResults")
    public Collection<SprintResultDisplayDto> getSprintResults (@RequestHeader HttpHeaders headers, @PathVariable long id) {
        return getById(headers, id).getPreviousSprintResults().stream()
                .map(sprintResultMapper::sprintResultToDisplayDto)
                .collect(Collectors.toList());
    }

    @GetMapping("{id}/currentSprint")
    public SprintDisplayDto getCurrentSprint (@RequestHeader HttpHeaders headers, @PathVariable long id) {
        Sprint sprint = getById(headers, id).getCurrentSprint();
        return sprintMapper.sprintToDisplayDto(sprint);
    }


    @GetMapping
    public Collection<Project> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return projectService.getAll();
    }

    @GetMapping("/filter")
    public Collection<Project> filterProjects(@RequestHeader HttpHeaders headers, @RequestParam(required = false) Optional<String> title, Optional<Date> startDate, Optional<Integer> ownerId, Optional<String> sortBy) {
        authenticationHelper.tryGetUser(headers);
        try {
            return projectService.filterProjects(title, startDate, ownerId, sortBy);
        } catch (InvalidEntityDataException e) {
           throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/search")
    public Collection<Project> searchProjects(@RequestHeader HttpHeaders headers, @RequestParam String search) {
        authenticationHelper.tryGetUser(headers);
        try {
            return projectService.searchProject(search);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }



    @PostMapping
    public Project create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ProjectDto dto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Project project = projectMapper.fromDto(dto);
            projectService.add(loggedUser, project);
            return project;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Project update(@RequestHeader HttpHeaders headers, @PathVariable long id, @Valid @RequestBody ProjectDto dto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Project project = projectMapper.fromDto(dto, id);
            User oldOwner = projectService.getById(id).getOwner();
            projectService.update(loggedUser, project, oldOwner);
            return project;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            projectService.deleteById(loggedUser, id);
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/developers/add/{userId}")
    public User addDeveloper(@RequestHeader HttpHeaders headers, @PathVariable long id, @PathVariable long userId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Project project = projectService.getById(id);
            User developerToAdd = userService.getById(userId);
            projectService.addDeveloperToProject(loggedUser, project, developerToAdd);
            return developerToAdd;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/developers/remove/{userId}")
    public User removeDeveloper(@RequestHeader HttpHeaders headers, @PathVariable long id, @PathVariable long userId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Project project = projectService.getById(id);
            User developerToRemove = userService.getById(userId);
            projectService.removeDeveloperFromProject(loggedUser, project, developerToRemove);
            return developerToRemove;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/sprints/{sprintId}")
    public Sprint setCurrentSprint(@RequestHeader HttpHeaders headers, @PathVariable long id, @PathVariable long sprintId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Project project = projectService.getById(id);
            Sprint sprint = sprintService.getById(sprintId);
            projectService.setCurrentSprint(loggedUser, project, sprint);
            return sprint;
        } catch (InvalidEntityDataException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NonexistingEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}

