package course.academy.controller.console;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.User;
import course.academy.model.dto.UserManagementDto;
import course.academy.model.mapper.UserMapper;
import course.academy.service.UserService;
import course.academy.view.Menu;
import course.academy.view.UserAdminUpdateDialog;

import java.util.List;

public class UserAdminUpdateController {

    private UserService userService;
    private UserMapper userMapper;

    public UserAdminUpdateController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }


    public void init(User loggedUser) {
        var menu = new Menu("USER ADMIN MENU", List.of(
                new Menu.Option("Update User Details", () -> {
                    var id = new UserAdminUpdateDialog().update();
                    User toBeUpdated;
                    try {
                        toBeUpdated = userService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }

                    UserManagementDto dto = userMapper.userToManagementDto(toBeUpdated);
                    var updatedDto = new UserAdminUpdateDialog(dto, toBeUpdated).input();
                    User updatedUser;
                    try {
                        updatedUser = userMapper.fromManagementDto(updatedDto, id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed";
                    }

                    try {
                        updatedUser = userService.update(loggedUser, updatedUser);
                    } catch (NonexistingEntityException | InvalidEntityDataException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return String.format("User with username '%s' was not updated.", updatedUser.getUsername());
                    }

                    return String.format("User ID:%d with username '%s' updated successfully.",
                            updatedUser.getId(), updatedUser.getUsername());
                })

        ));
        menu.show();
    }


}
