package course.academy.controller.console;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Task;
import course.academy.model.User;
import course.academy.model.mapper.TaskMapper;
import course.academy.service.TaskService;
import course.academy.view.Menu;
import course.academy.view.ProjectDialog;
import course.academy.view.TaskDialog;
import course.academy.view.TaskUpdateDialog;

import java.util.List;


public class TaskController {

    private final TaskService taskService;
    private final TaskMapper taskMapper;

    public TaskController(TaskService taskService, TaskMapper taskMapper) {
        this.taskService = taskService;
        this.taskMapper = taskMapper;
    }

    public void init(User loggedUser) {
//        taskService.loadData();

        var menu = new Menu("Tasks CRUD Menu", List.of(
//                new Menu.Option("Load Tasks", () -> {
//                    System.out.println("Loading tasks ...");
//                    taskService.loadData();
//                    return "Tasks loaded successfully.";
//                }),

                new Menu.Option("Print All Tasks", () -> {
                    var tasks = taskService.getAll();
                    tasks.forEach(System.out::println);
                    return "Total task count: " + tasks.size();
                }),

                new Menu.Option("Enter New Task", () -> {
                    var dto = new TaskDialog().input();
                    Task task;
                    try {
                        task = taskMapper.fromDto(dto);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Task with title '%s' was not added.", dto.getTitle());
                    }
                    Task created;
                    try {
                        created = taskService.add(loggedUser, task);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return String.format("Task with title '%s' was not added.", dto.getTitle());
                    }

                    return String.format("Task ID:%d with title '%s' was added successfully.",
                            created.getId(), created.getTitle());
                }),

                new Menu.Option("Update Existing Task", () -> {
                    var id = new TaskDialog().update();
                    Task toBeUpdated;
                    try {
                        toBeUpdated = taskService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }

                    var dto = new TaskUpdateDialog().input(toBeUpdated);
                    try {
                        toBeUpdated = taskMapper.fromUpdateDto(dto, id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Task with title '%s' was not updated.", dto.getTitle());
                    }
                    Task updated;
                    try {
                        updated = taskService.update(loggedUser, toBeUpdated);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Task with title '%s' was not updated.", dto.getTitle());
                    }
                    return String.format("Task ID:%d with title '%s' was updated successfully.",
                            updated.getId(), updated.getTitle());
                }),

                new Menu.Option("Delete Existing Task", () -> {
                    var id = new TaskDialog().delete();
                    Task deleted;
                    try {
                        deleted = taskService.deleteById(loggedUser, id);
                    } catch (NonexistingEntityException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    return String.format("Task ID:%d with title '%s' was deleted successfully.",
                            deleted.getId(), deleted.getTitle());
                }),

                new Menu.Option("Print Single Task by ID", () -> {
                    var id = new TaskDialog().printById();
                    Task task;
                    try {
                        task = taskService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another Task ID.";
                    }
                    System.out.println(task);
                    return "";
                }),

//                new Menu.Option("Update Task Status", () -> {
//                    var id = new TaskDialog().printById();
//                    Task task;
//                    try {
//                        task = taskService.getById(id);
//                    } catch (NonexistingEntityException e) {
//                        System.out.println(e.getMessage());
//                        return "Try another Task ID.";
//                    }
//                    StatusTasks statusTasks = new TaskDialog().enterStatusTask();
//                    try {
//                        taskService.updateStatusTask(loggedUser, task, statusTask);
//                    } catch (UnauthorisedOperationException e) {
//                        System.out.println(e.getMessage());
//                        return "Operation was not executed.";
//                    }
//                    return String.format("The task status of task ID:%d with title '%s' was updated successfully to %s.",
//                            task.getId(), task.getTitle(), task.getStatusTask());
//                }),

                new Menu.Option("Search Tasks", () -> {
                    var searchString = new ProjectDialog().inputSearchString();
                    var tasks = taskService.searchTask(searchString);
                    tasks.forEach(System.out::println);
                    return "Total task count: " + tasks.size();
                })

        ));
        menu.show();
    }


}
