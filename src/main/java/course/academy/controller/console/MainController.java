package course.academy.controller.console;

import course.academy.model.Role;
import course.academy.model.User;
import course.academy.model.mapper.*;
import course.academy.service.*;
import course.academy.utils.MessageHelpers;
import course.academy.view.Menu;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MainController {

    private UserService userService;
    private UserMapper userMapper;
    private ProjectService projectService;
    private ProjectMapper projectMapper;
    private TaskService taskService;
    private TaskMapper taskMapper;
    private SprintService sprintService;
    private SprintMapper sprintMapper;
    private ProjectResultService projectResultService;
    private ProjectResultMapper projectResultMapper;
    private TaskResultService taskResultService;
    private TaskResultMapper taskResultMapper;
    private SprintResultService sprintResultService;
    private SprintResultMapper sprintResultMapper;

    public MainController() {
    }

    public MainController(UserService userService,
                          UserMapper userMapper, ProjectService projectService, ProjectMapper projectMapper,
                          TaskService taskService, TaskMapper taskMapper,
                          SprintService sprintService, SprintMapper sprintMapper,
                          ProjectResultService projectResultService, ProjectResultMapper projectResultMapper,
                          TaskResultService taskResultService, TaskResultMapper taskResultMapper,
                          SprintResultService sprintResultService, SprintResultMapper sprintResultMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.projectService = projectService;
        this.projectMapper = projectMapper;
        this.taskService = taskService;
        this.taskMapper = taskMapper;
        this.sprintService = sprintService;
        this.sprintMapper = sprintMapper;
        this.projectResultService = projectResultService;
        this.projectResultMapper = projectResultMapper;
        this.taskResultService = taskResultService;
        this.taskResultMapper = taskResultMapper;
        this.sprintResultService = sprintResultService;
        this.sprintResultMapper = sprintResultMapper;
    }

//    public User getLoggedUser() {
//        return loginController.init();
//    }

    public void init(User loggedUser) {
        var optionsList = List.of(
                new Menu.Option("Users Menu", () -> {
                    var userController = new UserController(userService);
                    userController.init(loggedUser);
                    return "";
                }),

                new Menu.Option("Project CRUD Menu", () -> {
                    var projectController = new ProjectController(projectService, projectMapper, userService, sprintService);
                    projectController.init(loggedUser);
                    return "";
                }),

                new Menu.Option("Task CRUD Menu", () -> {
                    var taskController = new TaskController(taskService, taskMapper);
                    taskController.init(loggedUser);
                    return "";
                }),

                new Menu.Option("Sprint CRUD Menu", () -> {
                    var sprintController = new SprintController(sprintService, sprintMapper, userService, taskService, projectService);
                    sprintController.init(loggedUser);
                    return "";
                }),

                new Menu.Option("Task Result CRUD Menu", () -> {
                    var taskResultController = new TaskResultController(taskResultService, taskResultMapper);
                    taskResultController.init(loggedUser);
                    return "";
                }),

                new Menu.Option("Sprint Result CRUD Menu", () -> {
                    var sprintResultController = new SprintResultController(sprintResultService, sprintResultMapper);
                    sprintResultController.init(loggedUser);
                    return "";
                }),

                new Menu.Option("Project Result CRUD Menu", () -> {
                    var projectResultController = new ProjectResultController(projectResultService, projectResultMapper);
                    projectResultController.init(loggedUser);
                    return "";
                }),

                new Menu.Option("Update User Profile", () -> {
                    var userPrivateUpdateController = new UserPrivateUpdateController(userService, userMapper);
                    userPrivateUpdateController.init(loggedUser);
                    return "";
                }),

                new Menu.Option("Application Description", () -> MessageHelpers.ABOUT)

        );

        List<Menu.Option> adminOptionList = new ArrayList<>();
        adminOptionList.addAll(optionsList);
        adminOptionList.add(new Menu.Option("Admin Zone (User Management)", () -> {
            var userAdminUpdateController = new UserAdminUpdateController(userService, userMapper);
            userAdminUpdateController.init(loggedUser);
            return "";
        }));

        var menu = new Menu();
        List<String> loggedUserRoles = loggedUser.getRoles().stream().map(Role::getName).collect(Collectors.toList());
        if (!loggedUserRoles.contains("ADMIN")) {
            menu = new Menu("MAIN MENU AGILE SPRINT MANAGEMENT SYSTEM", optionsList);
        } else {
            menu = new Menu("MAIN MENU AGILE SPRINT MANAGEMENT SYSTEM", adminOptionList);
        }

        menu.show();
    }
}


