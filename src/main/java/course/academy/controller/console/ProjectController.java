package course.academy.controller.console;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Project;
import course.academy.model.Sprint;
import course.academy.model.User;
import course.academy.model.mapper.ProjectMapper;
import course.academy.service.ProjectService;
import course.academy.service.SprintService;
import course.academy.service.UserService;
import course.academy.view.Menu;
import course.academy.view.ProjectDialog;
import course.academy.view.ProjectUpdateDialog;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.stream.Collectors;


public class ProjectController {

    private ProjectService projectService;
    private ProjectMapper projectMapper;
    private UserService userService;
    private SprintService sprintService;

    public ProjectController(ProjectService projectService, ProjectMapper projectMapper, UserService userService,
                             SprintService sprintService) {
        this.projectService = projectService;
        this.projectMapper = projectMapper;
        this.userService = userService;
        this.sprintService = sprintService;
    }

    public void init(User loggedUser) {
        var menu = new Menu("Projects CRUD Menu", List.of(
                new Menu.Option("Print All Projects", () -> {
                    var projects = projectService.getAll();
                    projects.forEach(System.out::println);
                    return "Total project count: " + projects.size();
                }),

                new Menu.Option("Enter New Project", () -> {
                    var dto = new ProjectDialog().input();
                    Project project;
                    try {
                        project = projectMapper.fromDto(dto);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Project with title '%s' was not added.", dto.getTitle());
                    }
                    Project created;
                    try {
                        created = projectService.add(loggedUser, project);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return String.format("Project with title '%s' was not added.", dto.getTitle());
                    }

                    return String.format("Project ID:%d with title '%s' was added successfully.",
                            created.getId(), created.getTitle());
                }),

                new Menu.Option("Update Existing Project", () -> {
                    var id = new ProjectDialog().update();
                    Project toBeUpdated;
                    try {
                        toBeUpdated = projectService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    User owner = toBeUpdated.getOwner();
                    var dto = new ProjectUpdateDialog().input(toBeUpdated);
                    try {
                        toBeUpdated = projectMapper.fromDto(dto, id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Project with title '%s' was not updated.", dto.getTitle());
                    }

                    Project updated;
                    try {
                        updated = projectService.update(loggedUser, toBeUpdated, owner);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Project with title '%s' was not updated.", dto.getTitle());
                    }
                    return String.format("Project ID:%d with title '%s' was updated successfully.%nDevelopers may be updated with add/remove developers menu functions",
                            updated.getId(), updated.getTitle());
                }),

                new Menu.Option("Delete Existing Project", () -> {
                    var id = new ProjectDialog().delete();
                    Project deleted;
                    try {
                        deleted = projectService.deleteById(loggedUser, id);
                    } catch (NonexistingEntityException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    return String.format("Project ID:%d with title '%s' was deleted successfully.",
                            deleted.getId(), deleted.getTitle());
                }),

                new Menu.Option("Print Single Project by ID", () -> {
                    var id = new ProjectDialog().printById();
                    Project project;
                    try {
                        project = projectService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another project id.";
                    }
                    System.out.println(project);
                    return "";
                }),

                new Menu.Option("Add Developer to Project", () -> {
                    var id = new ProjectDialog().printById();
                    Project project;
                    try {
                        project = projectService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another project id.";
                    }
                    var userId = new ProjectDialog().addDeveloperMessage();
                    User user;
                    try {
                        user = userService.getById(userId);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another user id.";
                    }
                    try {
                        projectService.addDeveloperToProject(loggedUser, project, user);
                    } catch (UnauthorisedOperationException | InvalidEntityDataException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }

                    return String.format("User with username '%s' was added to Project ID:%d",
                            user.getUsername(), project.getId());
                }),

                new Menu.Option("Remove Developer from Project", () -> {
                    var id = new ProjectDialog().printById();
                    Project project;
                    try {
                        project = projectService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another project id.";
                    }

                    System.out.println("Developers, assigned to project:");
                    if (project.getDevelopers().isEmpty()) {
                        System.out.print(" no developers assigned");
                    }
                    project.getDevelopers().forEach(dev -> System.out.printf("Id:%d, username: %s%n",
                            dev.getId(), dev.getUsername()));

                    var userId = new ProjectDialog().removeDeveloperMessage();
                    User user;
                    try {
                        user = userService.getById(userId);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another user id.";
                    }
                    try {
                        projectService.removeDeveloperFromProject(loggedUser, project, user);
                    } catch (UnauthorisedOperationException | InvalidEntityDataException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }

                    return String.format("User with username '%s' was removed from Project ID:%d",
                            user.getUsername(), project.getId());
                }),

                new Menu.Option("Set Current Sprint", () -> {
                    var id = new ProjectDialog().printById();
                    Project project;
                    try {
                        project = projectService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another project id.";
                    }
                    List<Sprint> projectSprints = sprintService.getAll().stream()
                            .filter(sprint -> sprint.getProject().equals(project))
                            .collect(Collectors.toList());
                    System.out.println("Available planned sprints in project Id:" + project.getId());
                    List<Sprint> availableSprints = projectSprints.stream().filter(sprint ->
                                    !project.getPreviousSprintResults().stream().anyMatch(result -> result.getSprint().equals(sprint)))
                            .collect(Collectors.toList());
                    if (project.getCurrentSprint() !=null && availableSprints.contains(project.getCurrentSprint())) {
                        availableSprints.remove(project.getCurrentSprint());
                    }
                    if (availableSprints.isEmpty()) {
                        return "No available planned Sprints";
                    } else {
                        availableSprints.forEach(System.out::println);
                        var sprintId = new ProjectDialog().setSprintMessage();
                        Sprint sprint;
                        try {
                            sprint = sprintService.getById(sprintId);
                        } catch (NonexistingEntityException e) {
                            System.out.println(e.getMessage());
                            return "Try another sprint id.";
                        }
                        try {
                            projectService.setCurrentSprint(loggedUser, project, sprint);
                        } catch (UnauthorisedOperationException | InvalidEntityDataException | NonexistingEntityException e) {
                            System.out.println(e.getMessage());
                            return "Operation was not executed.";
                        }
                        return String.format("Sprint with ID:%d was set as the current sprint to Project ID:%d",
                                sprint.getId(), project.getId());
                    }
                }),

                new Menu.Option("Search Projects", () -> {
                    var searchString = new ProjectDialog().inputSearchString();
                    var projects = projectService.searchProject(searchString);
                    projects.forEach(System.out::println);
                    return "Total project count: " + projects.size();
                })


        ));

        menu.show();
    }


}
