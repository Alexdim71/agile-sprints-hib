package course.academy.service;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Sprint;
import course.academy.model.Task;
import course.academy.model.User;

import java.util.Collection;

public interface SprintService {
    Collection<Sprint> getAll();
    Sprint getById(long id) throws NonexistingEntityException;
    Sprint add(User loggedUser, Sprint sprint);
    Sprint update(User loggedUser, Sprint sprint);
    Sprint deleteById(User loggedUser, long id);
    Task addTaskToSprint(User loggedUser, Sprint sprint, Task task);
    Task removeTaskFromSprint(User loggedUser, Sprint sprint, Task task);
    User addDeveloperToSprint(User loggedUser, Sprint sprint, User user);
    User removeDeveloperFromSprint(User loggedUser, Sprint sprint, User user);

    long count();
}
