package course.academy.service;

import course.academy.model.TaskResult;
import course.academy.model.User;

import java.util.Collection;

public interface TaskResultService {
    Collection<TaskResult> getAll();
    TaskResult getById(long id);
    TaskResult add(User loggedUser, TaskResult taskResult);
    TaskResult update(User loggedUser, TaskResult taskResult);
    TaskResult deleteById(User loggedUser, long id);
    long count();
}
