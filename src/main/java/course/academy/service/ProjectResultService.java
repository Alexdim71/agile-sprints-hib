package course.academy.service;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.ProjectResult;
import course.academy.model.User;

import java.util.Collection;

public interface ProjectResultService {
    Collection<ProjectResult> getAll();
    ProjectResult getById(long id);
    ProjectResult add(User loggedUser, ProjectResult projectResult);
    ProjectResult update(User loggedUser, ProjectResult projectResult);
    ProjectResult deleteById(User loggedUser, long id);
    long count();
}
