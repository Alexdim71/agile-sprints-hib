package course.academy.service;


import course.academy.dao.*;
//import org.springframework.security.core.parameters.P;

public interface ServiceFactory {

    UserService createUserService(UserRepository userRepository, StatusUserRepository statusUserRepository,
                                  RoleRepository roleRepository);
    TaskService createTaskService(TaskRepository taskRepository, ProjectRepository projectRepository, StatusTaskRepository statusTaskRepository);
    TaskResultService createTaskResultService(TaskResultRepository taskResultRepository, SprintRepository sprintRepository, UserRepository userRepository,
                                              TaskRepository taskRepository, StatusTaskRepository statusTaskRepository);
    SprintService createSprintService(SprintRepository sprintRepository, TaskRepository taskRepository,
                                      StatusTaskRepository statusTaskRepository, UserRepository userRepository, ProjectRepository projectRepository);
    SprintResultService createSprintResultService(SprintResultRepository sprintResultRepository, ProjectRepository projectRepository);
    ProjectService createProjectService(ProjectRepository projectRepository, UserRepository userRepository,
                                        TaskRepository taskRepository, StatusTaskRepository statusTaskRepository);
    ProjectResultService createProjectResultService(ProjectResultRepository projectResultRepository, UserRepository userRepository);

}
