package course.academy.service;


import course.academy.model.StatusTask;
import course.academy.model.Task;
import course.academy.model.User;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface TaskService {
//    void loadData();
    Collection<Task> getAll();
    Task getById(long id);
    Task add(User loggedUser, Task task);
    Task update(User loggedUser, Task task);
    Task deleteById(User loggedUser, long id);
    long count();
    StatusTask updateStatusTask(User loggedUser, Task task, StatusTask status);
    Collection<Task> searchTask(String searchString);
    List<Task> filterTasks (Optional<String> title, Optional<Long> projectId, Optional<String> kind,
                            Optional<String> status, Optional<Integer> minEstimatedEffort, Optional<String> sort);

}
