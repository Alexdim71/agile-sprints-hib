package course.academy.service.impl;

import course.academy.dao.ProjectResultRepository;
import course.academy.dao.UserRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.ProjectResult;
import course.academy.model.User;
import course.academy.service.ProjectResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;

import static course.academy.model.ProjectResult.RESULTS_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.ProjectResult.RESULTS_DESCRIPTION_MIN_LENGTH;
import static course.academy.utils.MessageHelpers.ENTITY_NOT_EXISTS;

@Service
class ProjectResultServiceImpl implements ProjectResultService {
    private final ProjectResultRepository projectResultRepository;
    private final UserRepository userRepository;

    @Autowired
    ProjectResultServiceImpl(ProjectResultRepository projectResultRepository, UserRepository userRepository) {
        this.projectResultRepository = projectResultRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Collection<ProjectResult> getAll() {
        return projectResultRepository.getAll();
    }

    @Override
    public ProjectResult getById(long id) {
        var projectResult = projectResultRepository.getById(id);
        if (projectResult == null) {
            throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "ProjectResult", id));
        }
        return projectResult;
    }

    @Override
    public ProjectResult add(User loggedUser, ProjectResult projectResult) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(projectResult.getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may create the project result.");
        }
        validateProjectResult(projectResult);
        projectResultRepository.create(projectResult);
        User user = projectResult.getProject().getOwner();
        user.getCompletedProjectResult().add(projectResult);
        user.getProjects().remove(projectResult.getProject());
        user.setModified(LocalDateTime.now());
        userRepository.update(user);
        return projectResult;
    }

    @Override
    public ProjectResult update(User loggedUser, ProjectResult projectResult) {
        validateProjectResult(projectResult);
        if (!loggedUser.isAdmin() && !loggedUser.equals(projectResult.getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may update the project result.");
        }
        projectResult.setModified(LocalDateTime.now());
        projectResultRepository.update(projectResult);
        return projectResult;
    }

    @Override
    public ProjectResult deleteById(User loggedUser, long id) {
        ProjectResult projectResult = getById(id);
        if (!loggedUser.isAdmin() && !loggedUser.equals(projectResult.getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may delete the project result.");
        }
        User user = projectResult.getProject().getOwner();
        user.getCompletedProjectResult().remove(projectResult);
        user.getProjects().add(projectResult.getProject());
        user.setModified(LocalDateTime.now());
        userRepository.update(user);

        projectResultRepository.delete(id);
        return projectResult;
    }

    @Override
    public long count() {
        return projectResultRepository.count();
    }

    private void validateProjectResult(ProjectResult projectResult) {
        if (projectResult.getResultsDescription() != null) {
            int descriptionLength = projectResult.getResultsDescription().length();
            if (descriptionLength < RESULTS_DESCRIPTION_MIN_LENGTH || descriptionLength > RESULTS_DESCRIPTION_MAX_LENGTH) {
                throw new InvalidEntityDataException(String.format("Project results description must be between %d and %d characters.",
                        RESULTS_DESCRIPTION_MIN_LENGTH, RESULTS_DESCRIPTION_MAX_LENGTH));
            }
        }
        if (projectResult.getEndDate().isBefore(projectResult.getProject().getStartDate())) {
            throw new InvalidEntityDataException("The end date must be after the project start");
        }
    }

}



