package course.academy.service.impl;

import course.academy.dao.*;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.service.TaskResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;

import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MIN_LENGTH;
import static course.academy.utils.MessageHelpers.ENTITY_NOT_EXISTS;

@Service
class TaskResultServiceImpl implements TaskResultService {
    private final TaskResultRepository taskResultRepository;
    private final SprintRepository sprintRepository;
    private final UserRepository userRepository;
    private final TaskRepository taskRepository;
    private final StatusTaskRepository statusTaskRepository;


    @Autowired
    public TaskResultServiceImpl(TaskResultRepository taskResultRepository, SprintRepository sprintRepository,
                                 UserRepository userRepository, TaskRepository taskRepository, StatusTaskRepository statusTaskRepository) {
        this.taskResultRepository = taskResultRepository;
        this.sprintRepository = sprintRepository;
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
        this.statusTaskRepository = statusTaskRepository;
    }


    @Override
    public Collection<TaskResult> getAll() {
        return taskResultRepository.getAll();
    }

    @Override
    public TaskResult getById(long id) throws NonexistingEntityException {
        var taskResult = taskResultRepository.getById(id);
        if (taskResult == null) {
            throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "TaskResult", id));
        }
        return taskResult;
    }

    @Override
    public TaskResult add(User loggedUser, TaskResult taskResult)  {
        if (!loggedUser.isAdmin() && !loggedUser.equals(taskResult.getTask().getProject().getOwner()) &&
        !taskResult.getTask().getDevelopersAssigned().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or team developer may add tasks results to projects.");
        }
        validateTaskResult(taskResult);
        Task task = taskResult.getTask();
        if (!task.getStatusTask().getStatusTask().equals("ACTIVE")) {
            throw new InvalidEntityDataException("Task should be with status 'ACTIVE', and the related sprint set as current.");
        }
        taskResult.setVerifiedBy(loggedUser);
        taskResultRepository.create(taskResult);
        Sprint sprint = taskResult.getTask().getSprint();
        sprint.getCompletedTaskResults().add(taskResult);
        sprint.setModified(LocalDateTime.now());
        sprintRepository.update(sprint);
        for (User developer: sprint.getDevelopers()) {
            developer.getCompletedTaskResults().add(taskResult);
            developer.getAssignedTasks().remove(task);
            developer.setModified(LocalDateTime.now());
            userRepository.update(developer);
        }
        task.setStatusTask(statusTaskRepository.getById(3));
        task.setModified(LocalDateTime.now());
        taskRepository.update(task);
        return taskResult;
    }

    @Override
    public TaskResult update(User loggedUser, TaskResult taskResult) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(taskResult.getTask().getProject().getOwner()) &&
                !taskResult.getTask().getDevelopersAssigned().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or team developer may add tasks results to projects.");
        }
        validateTaskResult(taskResult);
        taskResult.setModified(LocalDateTime.now());
        taskResultRepository.update(taskResult);
        return taskResult;
    }

    @Override
    public TaskResult deleteById(User loggedUser, long id) {
        TaskResult taskResult = getById(id);
        if (!loggedUser.isAdmin() && !loggedUser.equals(taskResult.getTask().getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may update tasks results.");
        }
        if (sprintResultExists(taskResult)) {
            throw new InvalidEntityDataException("This Task Result is included in a Sprint Result, please delete it first");
        }
        Sprint sprint = taskResult.getTask().getSprint();
        sprint.getCompletedTaskResults().remove(taskResult);
        sprint.setModified(LocalDateTime.now());
        sprintRepository.update(sprint);
        Task task = taskResult.getTask();
        task.setStatusTask(statusTaskRepository.getById(2));
        task.setModified(LocalDateTime.now());
        taskRepository.update(task);
        for (User developer: sprint.getDevelopers()) {
            developer.getCompletedTaskResults().remove(taskResult);
            developer.setModified(LocalDateTime.now());
            userRepository.update(developer);
        }

        taskResultRepository.delete(id);

        return taskResult;
    }

    @Override
    public long count() {
        return taskResultRepository.count();
    }

    private void validateTaskResult(TaskResult taskResult)  {
        if (taskResult.getActualEffort() < 0) {
            throw new InvalidEntityDataException("Actual effort must be positive integer value.");
        }
        if (taskResult.getResultDescription() != null) {
            int descriptionLength = taskResult.getResultDescription().length();
            if (descriptionLength < RESULT_DESCRIPTION_MIN_LENGTH || descriptionLength > RESULT_DESCRIPTION_MAX_LENGTH) {
                throw new InvalidEntityDataException(String.format("Task result description must be between %d and %d characters.",
                        RESULT_DESCRIPTION_MIN_LENGTH, RESULT_DESCRIPTION_MAX_LENGTH));
            }
        }

    }

    private boolean sprintResultExists(TaskResult taskResult) {
        return taskResult.getTask().getProject().getPreviousSprintResults()
                .stream()
                .map(SprintResult::getSprint)
                .anyMatch(sprint -> sprint.getCompletedTaskResults().contains(taskResult));

    }


}
