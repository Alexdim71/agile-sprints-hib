package course.academy.service.impl;

import course.academy.dao.ProjectRepository;
import course.academy.dao.SprintResultRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.service.SprintResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;

import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MIN_LENGTH;
import static course.academy.utils.MessageHelpers.ENTITY_NOT_EXISTS;

@Service
class SprintResultServiceImpl implements SprintResultService {
    private final SprintResultRepository sprintResultRepository;
    private final ProjectRepository projectRepository;

    @Autowired
    SprintResultServiceImpl(SprintResultRepository sprintResultRepository, ProjectRepository projectRepository) {
        this.sprintResultRepository = sprintResultRepository;
        this.projectRepository = projectRepository;
    }


    @Override
    public Collection<SprintResult> getAll() {
        return sprintResultRepository.getAll();
    }

    @Override
    public SprintResult getById(long id) throws NonexistingEntityException {
        var sprintResult = sprintResultRepository.getById(id);
        if (sprintResult == null) {
            throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "Sprint", id));
        }
        return sprintResult;
    }

    @Override
    public SprintResult add(User loggedUser, SprintResult sprintResult) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(sprintResult.getSprint().getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may add sprint results to projects.");
        }
        validateSprintResult(sprintResult);
        Project project = sprintResult.getSprint().getProject();
        if (sprintResultExists(sprintResult.getSprint())) {
            throw new InvalidEntityDataException("Sprint Result for this Sprint has already been drafted.");
        }
        for (Task task: sprintResult.getSprint().getTasks()){
            if (!task.getStatusTask().getStatusTask().equals("COMPLETED")) {
                throw new InvalidEntityDataException("All tasks in the sprints must be with Status complete before drafting a sprint result");
            }
        }
        int teamVelocity = calculateTeamVelocity(sprintResult);
        sprintResult.setTeamVelocity(teamVelocity);
        sprintResultRepository.create(sprintResult);
        project.getPreviousSprintResults().add(sprintResult);
        for (Task task : sprintResult.getSprint().getTasks()) {
            project.getTasksBacklog().remove(task);
        }
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);
        return sprintResult;
    }

    @Override
    public SprintResult update(User loggedUser, SprintResult sprintResult)  {
        validateSprintResult(sprintResult);
        if (!loggedUser.isAdmin() && !loggedUser.equals(sprintResult.getSprint().getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may update sprint results.");
        }
        int teamVelocity = calculateTeamVelocity(sprintResult);
        sprintResult.setTeamVelocity(teamVelocity);
        sprintResult.setModified(LocalDateTime.now());
        sprintResultRepository.update(sprintResult);
        return sprintResult;
    }

    @Override
    public SprintResult deleteById(User loggedUser, long id)  {
        SprintResult sprintResult = getById(id);
        if (!loggedUser.isAdmin() && !loggedUser.equals(sprintResult.getSprint().getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may delete sprint results.");
        }
        Project project = sprintResult.getSprint().getProject();
        project.getPreviousSprintResults().remove(sprintResult);
        for (Task task : sprintResult.getSprint().getTasks()) {
            project.getTasksBacklog().add(task);
        }
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);

        sprintResultRepository.delete(id);
        return sprintResult;
    }

    @Override
    public long count() {
        return sprintResultRepository.count();
    }

    private void validateSprintResult(SprintResult sprintResult) {
        if (sprintResult.getResultsDescription() != null) {
            int descriptionLength = sprintResult.getResultsDescription().length();
            if (descriptionLength < RESULTS_DESCRIPTION_MIN_LENGTH || descriptionLength > RESULTS_DESCRIPTION_MAX_LENGTH) {
                throw new InvalidEntityDataException(String.format("Project results description must be between %d and %d characters.",
                        RESULTS_DESCRIPTION_MIN_LENGTH, RESULTS_DESCRIPTION_MAX_LENGTH));
            }
        }
    }

    private int calculateTeamVelocity(SprintResult sprintResult) {
        var totalTeamEffortUnits = sprintResult.getSprint().getCompletedTaskResults()
                .stream()
                .mapToInt(TaskResult::getActualEffort)
                .sum();

        return totalTeamEffortUnits / sprintResult.getSprint().getDuration();
    }

    private boolean sprintResultExists (Sprint sprint) {
        return sprint.getProject().getPreviousSprintResults().stream()
                .anyMatch(sprintResult -> sprintResult.getSprint().equals(sprint));
    }

}
