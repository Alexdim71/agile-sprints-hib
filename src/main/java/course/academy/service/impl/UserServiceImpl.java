package course.academy.service.impl;

import course.academy.dao.RoleRepository;
import course.academy.dao.StatusUserRepository;
import course.academy.dao.UserRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import static course.academy.model.User.*;
import static course.academy.utils.MessageHelpers.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final StatusUserRepository statusUserRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, StatusUserRepository statusUserRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.statusUserRepository = statusUserRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public Collection<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public Collection<User> searchByName(String name) {
        return userRepository.searchByName(name);
    }

    @Override
    public Optional<User> findByUsername (String username) {
       return Optional.ofNullable(userRepository.getByField("username", username));
    }

    @Override
    public Optional<User> findByEmail (String email) {
        return Optional.ofNullable(userRepository.getByField("email", email));
//        return getAll().stream().filter(u -> u.getEmail().equals(email)).findFirst();
    }

    @Override
    public User getById(long id) throws NonexistingEntityException {
        var user = userRepository.getById(id);
        if (user == null) {
            throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "User", id));
        }
        return user;
    }

    @Override
    public User add(User user) {
        validateUser(user);
        Optional<User> existingUser = findByUsername(user.getUsername());
        if (existingUser.isPresent()) {
            throw new InvalidEntityDataException(String.format("User with username %s already exists", user.getUsername()));
        }
        Optional<User> existingMail = findByEmail(user.getEmail());
        if (existingMail.isPresent()) {
            throw new InvalidEntityDataException(String.format("User with email %s already exists", user.getEmail()));
        }
        user.setStatusUser(statusUserRepository.getById(1));
        Set<Role> roles = user.getRoles();
        roles.add(roleRepository.getById(3));
        user.setRoles(roles);
        userRepository.create(user);

        return user;
    }

    @Override
    public User update(User loggedUser, User user)  {
        if (loggedUser.getId() != user.getId() && !loggedUser.isAdmin()){
            throw new UnauthorisedOperationException(UNAUTHORISED_USER_MODIFICATION);
        }
        validateUser(user);

        boolean duplicateMailExists = true; //check if the updated email could belong to another user
        try {
            User existingUser = userRepository.getByField("email", user.getEmail());
            if (existingUser.getId() == user.getId()) { //if same user but other fields are updated
                duplicateMailExists = false;
            }
        } catch (NonexistingEntityException e) { //email is not in the system, update is OK
            duplicateMailExists = false;
        }
        if (duplicateMailExists) {
            throw new InvalidEntityDataException(String.format("Email %s belongs to another user", user.getEmail()));
        }

        user.setModified(LocalDateTime.now());
        userRepository.update(user);
        return user;
    }

    @Override
    public User deleteById(User loggedUser, long id)  {
        if (loggedUser.getId() != id && !loggedUser.isAdmin()) {
            throw new UnauthorisedOperationException(UNAUTHORISED_USER_MODIFICATION);
        }
        User old = userRepository.getById(id);
        userRepository.delete(id);
        return old;
    }

    @Override
    public long count() {
        return userRepository.count();
    }

    @Override
    public void addUserRole(User loggedUser, User user, Role role)  {
        if (!loggedUser.isAdmin()) {
            throw new UnauthorisedOperationException("Roles may be added or removed only by  by Administrator.");
        }
        user.getRoles().add(role);
        update(loggedUser, user);
    }

    @Override
    public void removeUserRole(User loggedUser, User user, Role role)  {
        if (!loggedUser.isAdmin()) {
            throw new UnauthorisedOperationException("Roles may be added or removed only by Administrator.");
        }
        if (!user.getRoles().contains(role))
            throw new InvalidEntityDataException(String.format("User with id %d has no role %s",
                    user.getId(), role.getName()));

        user.getRoles().remove(role);
        update(loggedUser, user);
    }

    @Override
    public void setUserStatus(User loggedUser, User user, StatusUser status)  {
        if (!loggedUser.isAdmin() && loggedUser.getId() !=1) {
            throw new UnauthorisedOperationException("The status may be changed only by Administrator.");
        }
        user.setStatusUser(status);
        update(loggedUser, user);
    }

    @Override
    public StatusUser getUserStatus(User loggedUser, User user) {
        if (!loggedUser.isAdmin() && loggedUser.getId() !=1) {
            throw new UnauthorisedOperationException("The status may be changed only by Administrator.");
        }
        return user.getStatusUser();
    }

    @Override
    public User updateByAdmin(User loggedUser, User user) {
        if (!loggedUser.isAdmin()) {
            throw new UnauthorisedOperationException("User management is accessible only by Administrator.");
        }
        return update(loggedUser,user);
//        return user;
    }

    @Override
    public void changePassword(User loggedUser, User user, String password) {
        if (!loggedUser.equals(user)) {
            throw new UnauthorisedOperationException("Only the profile owner is authorised to change password");
        }
        if (!password.matches(checkPassword)) {
            throw new InvalidEntityDataException(String.format(INVALID_PASSWORD, PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH));
        }
        user.setPassword(password);
        user.setStatusUser(statusUserRepository.getById(2));
        update(loggedUser, user);

    }

    private void validateUser(User user)  {
        int firstNameLength = user.getFirstName().length();
        int lastNameLength = user.getLastName().length();
        if (firstNameLength < FIRST_NAME_MIN_LENGTH || firstNameLength > FIRST_NAME_MAX_LENGTH) {
            throw new InvalidEntityDataException(String.format("First name must be with length between %d and %d characters",
                    FIRST_NAME_MIN_LENGTH, FIRST_NAME_MAX_LENGTH));
        }
        if (lastNameLength < LAST_NAME_MIN_LENGTH || lastNameLength > LAST_NAME_MAX_LENGTH) {
            throw new InvalidEntityDataException(String.format("Last name must be with length between %d and %d characters",
                    LAST_NAME_MIN_LENGTH, LAST_NAME_MAX_LENGTH));
        }

        if (!user.getUsername().matches(checkUsername)) {
            throw new InvalidEntityDataException(String.format("Username must be between %d and %d characters and contain only word characters",
                    USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH));
        }


        if (!user.getEmail().matches(checkEmail)) {
            throw new InvalidEntityDataException("This is not a valid mail address.");
        }

        if (!user.getPassword().matches(checkPassword)) {
            throw new InvalidEntityDataException(String.format(INVALID_PASSWORD, PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH));
        }

       if (user.getContacts() != null) {
           int contactsLength = user.getContacts().length();
           if (contactsLength < CONTACTS_MIN_LENGTH || contactsLength > CONTACTS_MAX_LENGTH) {
               throw new InvalidEntityDataException(String.format("Contacts must be between %d and %d characters.",
                       CONTACTS_MIN_LENGTH, CONTACTS_MAX_LENGTH));
           }
       }

    }

}
