package course.academy.service.impl;

import course.academy.dao.ProjectRepository;
import course.academy.dao.StatusTaskRepository;
import course.academy.dao.TaskRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static course.academy.model.Task.*;
import static course.academy.utils.MessageHelpers.ENTITY_NOT_EXISTS;

@Service
class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;
    private final StatusTaskRepository statusTaskRepository;

    @Autowired
    TaskServiceImpl(TaskRepository taskRepository, ProjectRepository projectRepository, StatusTaskRepository statusTaskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.statusTaskRepository = statusTaskRepository;
    }

    @Override
    public Collection<Task> getAll() {
        return taskRepository.getAll();
    }

    @Override
    public Task getById(long id) throws NonexistingEntityException {
        var task = taskRepository.getById(id);
        if (task == null) {
            throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "Task", id));
        }
        return task;
    }

    @Override
    public Task add(User loggedUser, Task task) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(task.getProject().getOwner()) &&
                !task.getProject().getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the project team may add tasks to projects.");
        }
        validateTask(task);
        task.setAddedBy(loggedUser);
        task.setStatusTask(statusTaskRepository.getById(1));
        taskRepository.create(task);
//        taskRepository.save(); //for file persistence purpose, doing nothing in Memory Repository
        Project project = task.getProject();
        Set<Task> taskList = project.getTasksBacklog();
        taskList.add(task);
        project.setTasksBacklog(taskList);
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);
        return task;
    }

    @Override
    public Task update(User loggedUser, Task task) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(task.getProject().getOwner()) &&
                !task.getProject().getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the project team may update tasks.");
        }
        validateTask(task);
        task.setModified(LocalDateTime.now());
        taskRepository.update(task);

//        taskRepository.save();

//        If project is not changed, which should be avoided, below check is skipped.
//        Project project = task.getProject();
//        List<Task> taskList = project.getTasksBacklog();
//        taskList.add(task);
//        project.setTasksBacklog(taskList);
//        project.setModified(LocalDateTime.now());
//        projectRepository.update(project);

        return task;
    }

    @Override
    public Task deleteById(User loggedUser, long id) {
        Task task = getById(id);
        if (!loggedUser.isAdmin() && !loggedUser.equals(task.getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may delete tasks.");
        }
        if (task.getSprint() != null) {
            throw new InvalidEntityDataException(String.format("Task is included in Sprint Id:%d, please first remove it from the sprint",
                    task.getSprint().getId()));
        }
        if (taskResultExists(task)) {
            throw new InvalidEntityDataException("Task Result for this task should be first deleted.");
        }

        Project project = task.getProject();
//        taskRepository.save();
        project.getTasksBacklog().remove(task);
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);
        taskRepository.delete(id);
        return task;
    }

    @Override
    public long count() {
        return taskRepository.count();
    }

    @Override
    public StatusTask updateStatusTask(User loggedUser, Task task, StatusTask status) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(task.getProject().getOwner()) &&
                !task.getDevelopersAssigned().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the task assigned team may update tasks.");
        }
        task.setStatusTask(status);
        task.setModified(LocalDateTime.now());
        taskRepository.update(task);
        return status;
    }

    @Override
    public Collection<Task> searchTask(String searchString) {
        String searchWord = searchString.toLowerCase();
        return getAll().stream().filter(task -> task.getTags() == null ? task.getTitle().toLowerCase().contains(searchWord) : task.getTags().contains(",") ?
                        task.getTitle().toLowerCase().contains(searchWord) || Arrays.stream(task.getTags().split(",")).anyMatch(t -> t.equalsIgnoreCase(searchWord)) :
                        task.getTitle().toLowerCase().contains(searchWord) || task.getTags().equalsIgnoreCase(searchWord))
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> filterTasks(Optional<String> title, Optional<Long> projectId, Optional<String> kind,
                                  Optional<String> status, Optional<Integer> minEstimatedEffort, Optional<String> sort) {
        return taskRepository.filterTasks(title, projectId, kind, status, minEstimatedEffort, sort);
    }

    private void validateTask(Task task) {
        int titleLength = task.getTitle().length();
        if (titleLength < TITLE_MIN_LENGTH || titleLength > TITLE_MAX_LENGTH) {
            throw new InvalidEntityDataException(String.format("Title length must be between %d and %d characters.",
                    TITLE_MIN_LENGTH, TITLE_MAX_LENGTH));
        }
        if (task.getEstimatedEffort() < 0) {
            throw new InvalidEntityDataException("Estimated effort should be positive integer value.");
        }
        if (task.getDescription() != null) {
            int descriptionLength = task.getDescription().length();
            if (descriptionLength < DESCRIPTION_MIN_LENGTH || descriptionLength > DESCRIPTION_MAX_LENGTH) {
                throw new InvalidEntityDataException(String.format("Task description must be between %d and %d characters.",
                        DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH));
            }
        }
    }

    private boolean taskResultExists(Task task) {
        if (task.getSprint() != null) {
            return task.getSprint().getCompletedTaskResults().stream()
                    .anyMatch(result -> result.getTask().equals(task));
        }
        return false;
    }

}