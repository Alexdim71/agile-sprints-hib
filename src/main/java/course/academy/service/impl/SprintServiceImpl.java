package course.academy.service.impl;

import course.academy.dao.*;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.service.SprintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;

import static course.academy.utils.MessageHelpers.ENTITY_NOT_EXISTS;

@Service
class SprintServiceImpl implements SprintService {
    private final SprintRepository sprintRepository;
    private final TaskRepository taskRepository;
    private final StatusTaskRepository statusTaskRepository;
    private final UserRepository userRepository;
    private final ProjectRepository projectRepository;

    @Autowired
    SprintServiceImpl(SprintRepository sprintRepository, TaskRepository taskRepository, StatusTaskRepository statusTaskRepository, UserRepository userRepository, ProjectRepository projectRepository) {
        this.sprintRepository = sprintRepository;
        this.taskRepository = taskRepository;
        this.statusTaskRepository = statusTaskRepository;
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Collection<Sprint> getAll() {
        return sprintRepository.getAll();
    }

    @Override
    public Sprint getById(long id) throws NonexistingEntityException {
        var sprint = sprintRepository.getById(id);
        if (sprint == null) {
            throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "Sprint", id));
        }
        return sprint;
    }

    @Override
    public Sprint add(User loggedUser, Sprint sprint) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(sprint.getProject().getOwner())
                && !sprint.getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner or assigned developers may add sprints.");
        }
        validateSprintUsersAndTasks(sprint);
        sprint.setOwner(loggedUser);
        sprintRepository.create(sprint);
        return sprint;
    }

    @Override
    public Sprint update(User loggedUser, Sprint sprint) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(sprint.getProject().getOwner())
                && !sprint.getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner or assigned developers may update sprints.");
        }
        sprint.getDevelopers().forEach(dev->validateSprintUser(sprint,dev));
        sprint.setModified(LocalDateTime.now());
        sprintRepository.update(sprint);
        return sprint;
    }

    @Override
    public Sprint deleteById(User loggedUser, long id) {
        Sprint sprint = getById(id);
        Project project = sprint.getProject();
        if (!loggedUser.isAdmin() && !loggedUser.equals(sprint.getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may delete sprints.");
        }
        if (!sprint.getTasks().isEmpty()){
            throw new InvalidEntityDataException("Please remove all tasks from the sprint before deleting");
        }
        if (project.getCurrentSprint() != null && project.getCurrentSprint().equals(sprint)) {
            project.setCurrentSprint(null);
            project.setModified(LocalDateTime.now());
            projectRepository.update(project);
        }
        sprintRepository.delete(id);
        return sprint;
    }

    @Override
    public Task addTaskToSprint(User loggedUser, Sprint sprint, Task task) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(task.getProject().getOwner()) &&
                !task.getProject().getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the project team may add tasks to sprints.");
        }

        validateSprintTask(sprint, task);
        if (sprint.getTasks().contains(task)) {
            throw new InvalidEntityDataException(
                    String.format("Task with ID %d is already included in the sprint", task.getId()));
        }

        sprint.getTasks().add(task);
        for (User developer : sprint.getDevelopers()) {
            Set<Task> assignedTasks = developer.getAssignedTasks();
            assignedTasks.add(task);
            developer.setAssignedTasks(assignedTasks);
            developer.setModified(LocalDateTime.now());
            userRepository.update(developer);
        }
        task.setSprint(sprint);
        task.getDevelopersAssigned().addAll(sprint.getDevelopers());
        task.setModified(LocalDateTime.now());
        if (sprint.getProject().getCurrentSprint() != null && sprint.getProject().getCurrentSprint() == sprint) {
            task.setStatusTask(statusTaskRepository.getById(2));
        }
        taskRepository.update(task);
        sprint.setModified(LocalDateTime.now());
        sprintRepository.update(sprint);
        return task;
    }

    @Override
    public Task removeTaskFromSprint(User loggedUser, Sprint sprint, Task task) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(task.getProject().getOwner()) &&
                !task.getProject().getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the project team may remove tasks from sprints.");
        }
        if (!sprint.getTasks().contains(task)) {
            throw new InvalidEntityDataException("The task is not included in the sprint");
        }
        if (taskResultExists(task)) {
            throw new InvalidEntityDataException("Task result for related task should be deleted first.");
        }

        sprint.getTasks().remove(task);

        task.setSprint(null);
        task.getDevelopersAssigned().removeAll(sprint.getDevelopers());
        task.setStatusTask(statusTaskRepository.getById(1));
        task.setModified(LocalDateTime.now());
        taskRepository.update(task);

        for (User developer : sprint.getDevelopers()) {
            developer.getAssignedTasks().remove(task);
            developer.setModified(LocalDateTime.now());
            userRepository.update(developer);
        }

        sprint.setModified(LocalDateTime.now());
        sprintRepository.update(sprint);
        return task;
    }


    @Override
    public User addDeveloperToSprint(User loggedUser, Sprint sprint, User user) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(sprint.getProject().getOwner()) &&
                !sprint.getProject().getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the project team may add developers to tasks.");
        }

        validateSprintUser(sprint, user);
        if (sprint.getDevelopers().contains(user)) {
            throw new InvalidEntityDataException(
                    String.format("User with ID %d is already assigned to the Sprint", user.getId()));
        }

        sprint.getDevelopers().add(user);
        sprint.setModified(LocalDateTime.now());
        sprintRepository.update(sprint);

        for (Task task : sprint.getTasks()) {
            if (!task.getStatusTask().getStatusTask().equals("COMPLETED")) {
                task.getDevelopersAssigned().add(user);
                task.setModified(LocalDateTime.now());
                taskRepository.update(task);
                user.getAssignedTasks().add(task);
                user.setModified(LocalDateTime.now());
                userRepository.update(user);
            }
        }
        return user;
    }

    @Override
    public User removeDeveloperFromSprint(User loggedUser, Sprint sprint, User user) {

        if (!loggedUser.isAdmin() && !loggedUser.equals(sprint.getProject().getOwner()) &&
                !sprint.getProject().getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the project team may remove developers from tasks.");
        }
        if (!sprint.getDevelopers().contains(user)) {
            throw new InvalidEntityDataException("This user is not assigned to the sprint.");
        }
        sprint.getDevelopers().remove(user);
        sprint.setModified(LocalDateTime.now());
        sprintRepository.update(sprint);

        for (Task task : sprint.getTasks()) {
            if (!task.getStatusTask().getStatusTask().equals("COMPLETED")) {
                task.getDevelopersAssigned().remove(user);
                task.setModified(LocalDateTime.now());
                taskRepository.update(task);
                user.getAssignedTasks().remove(task);
                user.setModified(LocalDateTime.now());
                userRepository.update(user);
            }
        }
        return user;
    }

    @Override
    public long count() {
        return sprintRepository.count();
    }

    private void validateSprintUsersAndTasks(Sprint sprint) {
        for (User user : sprint.getDevelopers()) {
            validateSprintUser(sprint, user);
        }
        for (Task task : sprint.getTasks()) {
            validateSprintTask(sprint, task);
        }
    }

    private void validateSprintUser(Sprint sprint, User user) {
        if (!sprint.getProject().getDevelopers().contains(user)) {
            throw new InvalidEntityDataException(
                    String.format("User with ID %d in not from the project team.", user.getId()));
        }
    }

    private void validateSprintTask(Sprint sprint, Task task) {
        if (!task.getProject().equals(sprint.getProject())) {
            throw new InvalidEntityDataException(
                    String.format("Task ID %d and the Sprint ID %d are from different projects.",
                            task.getId(), sprint.getId()));
        }

        if (!task.getStatusTask().getStatusTask().equals("PLANNED")) {
            throw new InvalidEntityDataException("The task should be with status 'Planned' before adding to a sprint");
        }
    }

    private boolean taskResultExists(Task task) {
        return task.getSprint().getCompletedTaskResults().stream()
                .anyMatch(result -> result.getTask().equals(task));
    }

}


