package course.academy.service.impl;

import course.academy.dao.*;
import course.academy.service.*;

public class ServiceFactoryImpl implements ServiceFactory {

    @Override
    public UserService createUserService(UserRepository userRepository, StatusUserRepository statusUserRepository,
                                         RoleRepository roleRepository) {
        return new UserServiceImpl(userRepository, statusUserRepository, roleRepository);
    }

    @Override
    public TaskService createTaskService(TaskRepository taskRepository, ProjectRepository projectRepository, StatusTaskRepository statusTaskRepository) {
        return new TaskServiceImpl(taskRepository, projectRepository, statusTaskRepository);
    }

    @Override
    public TaskResultService createTaskResultService(TaskResultRepository taskResultRepository,
                                                     SprintRepository sprintRepository, UserRepository userRepository,
                                                     TaskRepository taskRepository, StatusTaskRepository statusTaskRepository) {
        return new TaskResultServiceImpl(taskResultRepository, sprintRepository, userRepository, taskRepository, statusTaskRepository);
    }

    @Override
    public SprintService createSprintService(SprintRepository sprintRepository, TaskRepository taskRepository,
                                             StatusTaskRepository statusTaskRepository, UserRepository userRepository, ProjectRepository projectRepository) {
        return new SprintServiceImpl(sprintRepository, taskRepository, statusTaskRepository, userRepository, projectRepository);
    }

    @Override
    public SprintResultService createSprintResultService(SprintResultRepository sprintResultRepository, ProjectRepository projectRepository) {
        return new SprintResultServiceImpl(sprintResultRepository, projectRepository);
    }

    @Override
    public ProjectService createProjectService(ProjectRepository projectRepository, UserRepository userRepository,
                                               TaskRepository taskRepository, StatusTaskRepository statusTaskRepository) {
        return new ProjectServiceImpl(projectRepository, userRepository, taskRepository, statusTaskRepository);
    }

    @Override
    public ProjectResultService createProjectResultService(ProjectResultRepository projectResultRepository, UserRepository userRepository) {
        return new ProjectResultServiceImpl(projectResultRepository, userRepository);
    }


}
