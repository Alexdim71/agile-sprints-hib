package course.academy.service.impl;

import course.academy.dao.*;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static course.academy.model.Project.*;
import static course.academy.utils.MessageHelpers.ENTITY_NOT_EXISTS;

@Service
class ProjectServiceImpl implements ProjectService {
    private final ProjectRepository projectRepository;
    private final UserRepository userRepository;
    private final TaskRepository taskRepository;
    private final StatusTaskRepository statusTaskRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository, UserRepository userRepository, TaskRepository taskRepository, StatusTaskRepository statusTaskRepository) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
        this.statusTaskRepository = statusTaskRepository;
    }

    @Override
    public Collection<Project> getAll() {
        return projectRepository.getAll();
    }

    @Override
    public Project getById(long id) {
        var project = projectRepository.getById(id);
        if (project == null) {
            throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "Project", id));
        }
        return project;
    }

    @Override
    public Project add(User loggedUser, Project project) {
        validateProject(project);
        if (!loggedUser.isAdmin()) {
            throw new UnauthorisedOperationException("Only an administrator is authorised to create projects.");
        }
        projectRepository.create(project);
        User owner = project.getOwner();
        Set<Project> projects = owner.getProjects();
        projects.add(project);
        owner.setProjects(projects);
        owner.setModified(LocalDateTime.now());
        userRepository.update(owner);
        return project;
    }

    @Override
    public Project update(User loggedUser, Project project, User owner) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(owner)) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may update projects");
        }
        validateProject(project);
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);

        User newOwner = project.getOwner();
        if (!newOwner.equals(owner)) {
            newOwner.getProjects().add(project);
            owner.getProjects().remove(project);
            newOwner.setModified(LocalDateTime.now());
            owner.setModified(LocalDateTime.now());
            userRepository.update(newOwner);
            userRepository.update(owner);
        }

        return project;
    }

    @Override
    public Project deleteById(User loggedUser, long id) {
        if (!loggedUser.isAdmin()) {
            throw new UnauthorisedOperationException("Only an administrator is authorised to delete projects.");
        }
        Project project = getById(id);
        User owner = project.getOwner();
        owner.getProjects().remove(project);
        owner.setModified(LocalDateTime.now());
        userRepository.update(owner);
        projectRepository.delete(id);
        return project;
    }

    @Override
    public long count() {
        return projectRepository.count();
    }

    @Override
    public User addDeveloperToProject(User loggedUser, Project project, User user) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(project.getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may add users to projects");
        }
        if (!user.hasRoleDeveloper()) {
            throw new InvalidEntityDataException("The user is not a developer. Please first assign role.");
        }
        if (project.getDevelopers().contains(user)) {
            throw new InvalidEntityDataException("The user is already assigned to this project.");
        }
        Set<User> developers = project.getDevelopers();
        developers.add(user);
        project.setDevelopers(developers);
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);
        return user;
    }

    @Override
    public User removeDeveloperFromProject(User loggedUser, Project project, User user) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(project.getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may add users to projects");
        }
        if (!project.getDevelopers().contains(user)) {
            throw new InvalidEntityDataException("The user is not assigned to this project.");
        }
        Set<User> developers = project.getDevelopers();
        developers.remove(user);
        project.setDevelopers(developers);
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);
        return user;
    }

    @Override
    public Sprint setCurrentSprint(User loggedUser, Project project, Sprint sprint) {
        if (!loggedUser.isAdmin() && !loggedUser.equals(project.getOwner()) &&
                !sprint.getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the sprint team may add sprints to projects.");
        }
//        The below check is tested as operational and intentionally skipped for test purposes.
//        if (sprint.getEndDate().isBefore(LocalDate.now())) {
//            throw new InvalidEntityDataException("This sprint is finished");
//        }
        if (!sprint.getProject().equals(project)) {
            throw new InvalidEntityDataException("The sprint is not assigned to this project.");
        }
        if (project.getCurrentSprint() != null && project.getCurrentSprint().equals(sprint)) {
            throw new InvalidEntityDataException("This sprint is already set as current.");
        }
        if (project.getPreviousSprintResults().stream().anyMatch(result -> result.getSprint().equals(sprint))) {
            throw new InvalidEntityDataException("This sprint is already completed.");
        }
        project.setCurrentSprint(sprint);
        for (Task task : sprint.getTasks()) {
            task.setStatusTask(statusTaskRepository.getById(2));
            task.setModified(LocalDateTime.now());
            taskRepository.update(task);
        }
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);
        return sprint;
    }

    @Override
    public Collection<Project> searchProject(String searchString) {
        String searchWord = searchString.toLowerCase();
        if (searchString.length() < 2) {
            throw new IllegalArgumentException("Please enter at least two symbols for search word");
        }
        return getAll().stream().filter(p ->
                p.getTags() == null ? p.getTitle().toLowerCase().contains(searchWord) : p.getTags().contains(",") ?
                        p.getTitle().toLowerCase().contains(searchWord) || Arrays.stream(p.getTags().split(",")).anyMatch(pr -> pr.equalsIgnoreCase(searchWord)) :
                        p.getTitle().toLowerCase().contains(searchWord) || p.getTags().equalsIgnoreCase(searchWord)
        ).collect(Collectors.toList());
    }

    @Override
    public Collection<Project> filterProjects(Optional<String> title, Optional<Date> startDate, Optional<Integer> ownerId, Optional<String> sortBy) {
        return projectRepository.filterProjects(title, startDate, ownerId, sortBy);
    }

    private void validateProject(Project project) {
        if (!project.getOwner().hasRoleOwner()) {
            throw new InvalidEntityDataException("The chosen user for owner has no role 'PRODUCT_OWNER'");
        }
        int titleLength = project.getTitle().length();
        if (titleLength < TITLE_MIN_LENGTH || titleLength > TITLE_MAX_LENGTH) {
            throw new InvalidEntityDataException(String.format("Title length must be between %d and %d characters.",
                    TITLE_MIN_LENGTH, TITLE_MAX_LENGTH));
        }

        if (project.getDescription() != null) {
            int descriptionLength = project.getDescription().length();
            if (descriptionLength < DESCRIPTION_MIN_LENGTH || descriptionLength > DESCRIPTION_MAX_LENGTH) {
                throw new InvalidEntityDataException(String.format("Project description must be between %d and %d characters.",
                        DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH));
            }
        }
    }

}


