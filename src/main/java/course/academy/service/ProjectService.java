package course.academy.service;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Project;
import course.academy.model.Sprint;
import course.academy.model.User;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

public interface ProjectService {
    Collection<Project> getAll();
    Project getById(long id);
    Project add(User loggedUser, Project project);
    Project update(User loggedUser, Project project, User owner);
    Project deleteById(User loggedUser, long id);
    long count();
    User addDeveloperToProject(User loggedUser, Project project, User user);
    User removeDeveloperFromProject(User loggedUser, Project project, User user);
    Sprint setCurrentSprint(User loggedUser, Project project, Sprint sprint);
    Collection<Project> searchProject (String searchString);
    Collection<Project> filterProjects (Optional<String> title, Optional<Date> startDate,
                                        Optional<Integer> ownerId, Optional<String> sortBy);
}
