package course.academy.service;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.SprintResult;
import course.academy.model.User;

import java.util.Collection;

public interface SprintResultService {
    Collection<SprintResult> getAll();
    SprintResult getById(long id) throws NonexistingEntityException;
    SprintResult add(User loggedUser, SprintResult sprintResult);
    SprintResult update(User loggedUser, SprintResult sprintResult);
    SprintResult deleteById(User loggedUser, long id);
    long count();
}
