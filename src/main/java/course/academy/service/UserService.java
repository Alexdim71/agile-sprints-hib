package course.academy.service;

import course.academy.model.*;

import java.util.Collection;
import java.util.Optional;

public interface  UserService {
    Collection<User> getAll();
    Collection<User> searchByName(String name);
    Optional<User> findByUsername (String username);
    Optional<User> findByEmail (String email);
    User getById(long id);
    User add(User user);
    User update(User loggedUser, User user);
    User deleteById(User user, long id);
    long count();
    void addUserRole(User loggedUser, User user, Role role);
    void removeUserRole(User loggedUser, User user, Role role);
    void setUserStatus(User loggedUser, User user, StatusUser status);
    StatusUser getUserStatus (User loggedUser, User user);
    void changePassword(User loggedUser, User user, String password);
    User updateByAdmin(User loggedUser, User user);
}
