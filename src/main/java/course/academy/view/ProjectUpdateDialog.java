package course.academy.view;


import course.academy.model.Project;
import course.academy.model.dto.ProjectDto;
import course.academy.utils.ParsingHelpers;

import java.time.LocalDate;
import java.util.Scanner;

import static course.academy.model.Project.*;


public class ProjectUpdateDialog {
    public static Scanner sc = new Scanner(System.in);

    public ProjectDto input(Project project) {
        var dto = new ProjectDto();

        while (true) {
            System.out.printf("New project title (press 'Enter' to skip), current title '%s'%n", project.getTitle());
            var ans = sc.nextLine();
            if (ans.isEmpty()) {
                dto.setTitle(project.getTitle());
                break;
            }
            int length = ans.length();
            if (length < TITLE_MIN_LENGTH || length > TITLE_MAX_LENGTH) {
                System.out.println((String.format("Project title must be with length between %d and %d characters",
                        TITLE_MIN_LENGTH, TITLE_MAX_LENGTH)));
                continue;
            }
            dto.setTitle(ans);
            break;
        }

        while (true) {
            System.out.printf("New Start date in format dd.mm.yyyy (press 'Enter' to skip), current start date '%s'%n",
                    project.getStartDate().format(ParsingHelpers.df));
            var ans = sc.nextLine();
            if (ans.isEmpty()) {
                dto.setStartDate(project.getStartDate());
                break;
            }
            LocalDate startDate = null;
            try {
                startDate = LocalDate.parse(ans, ParsingHelpers.df);
            } catch (Exception e) {
                System.out.println("Error: Invalid date - try again in the specified format.");
                continue;
            }
            dto.setStartDate(startDate);
            break;
        }

        while (true) {
            System.out.printf("New Owner ID (press 'Enter' to skip), current Owner ID:%d%n", project.getOwner().getId());
            var ans = sc.nextLine();
            if (ans.isEmpty()) {
                dto.setOwnerId(project.getOwner().getId());
                break;
            }
            long ownerId = 0;
            try {
                ownerId = Integer.parseInt(sc.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Error: please enter a positive integer value.");
                continue;
            }
            dto.setOwnerId(ownerId);
            break;
        }

        while (dto.getTags() == null) {
            System.out.printf("Tags, separate by ',' (press 'enter' to skip), current tags: '%s'%n", project.getTags());
            var ans = sc.nextLine();
            if (ans.isEmpty()) {
                dto.setTags(project.getTags());
                break;
            }
            dto.setTags(ans);
            break;
        }

        while (dto.getDescription() == null) {
            System.out.println("Enter description(press 'enter' to skip or type 'delete' to remove existing description)");
            var ans = sc.nextLine();
            if (ans.isEmpty()) {
                dto.setDescription(dto.getDescription());
                break;
            }
            if (ans.equalsIgnoreCase("delete")) {
                dto.setDescription(null);
                break;
            }
            if (ans.length() < DESCRIPTION_MIN_LENGTH || ans.length() > DESCRIPTION_MAX_LENGTH) {
                System.out.println((String.format("Project description must be between %d and %d characters.",
                        DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH)));
                continue;
            }
            dto.setDescription(ans);
            break;
        }
        project.getDevelopers().forEach(dev -> dto.getDeveloperIds().add(dev.getId()));
        return dto;
    }

}


