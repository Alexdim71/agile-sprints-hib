package course.academy.view;

import course.academy.model.SprintResult;
import course.academy.model.TaskResult;
import course.academy.model.dto.SprintResultUpdateDto;
import course.academy.model.dto.TaskResultUpdateDto;
import course.academy.utils.ParsingHelpers;

import java.util.Scanner;

import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MIN_LENGTH;
import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MIN_LENGTH;

public class SprintResultUpdateDialog {

    public static Scanner sc = new Scanner(System.in);

    public SprintResultUpdateDialog() {
    }

    public SprintResultUpdateDto input(SprintResult sprintResult) {
        var dto = new SprintResultUpdateDto();
        System.out.printf("Sprint Result for Sprint Id:%d, Finished on %s%n", sprintResult.getSprint().getId(), sprintResult.getSprint().getEndDate().format(ParsingHelpers.df));

        while (true) {
            System.out.println("New Results description (press 'enter' to skip, type 'delete' to delete existing description)");
            String ans1 = sc.nextLine();
            if (ans1.isEmpty()) {
                dto.setResultsDescription(dto.getResultsDescription());
                break;
            }
            if (ans1.equalsIgnoreCase("delete")) {
                dto.setResultsDescription(null);
                break;
            }
            if (ans1.length() < RESULTS_DESCRIPTION_MIN_LENGTH|| ans1.length() > RESULTS_DESCRIPTION_MAX_LENGTH) {
                System.out.println((String.format("Result description must be between %d and %d characters.",
                        RESULTS_DESCRIPTION_MIN_LENGTH, RESULTS_DESCRIPTION_MAX_LENGTH)));
                continue;
            }
            dto.setResultsDescription(ans1);
            break;
        }

        return dto;
    }

}


