package course.academy.view;

import java.util.Scanner;

import static course.academy.model.User.*;
import static course.academy.utils.MessageHelpers.INVALID_PASSWORD;

public class LoginDialog {
    public static Scanner sc = new Scanner(System.in);

    public String inputUsername() {
        String username = null;
        while (username == null) {
            System.out.println("Username: ");
            var ans = sc.nextLine();
            if (!ans.matches(checkUsername)) {
                System.out.println((String.format("Username must be between %d and %d characters and contain only word characters",
                        USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH)));
                continue;
            } else {
                username = ans;
            }
        }
        return username;
    }


    public String inputPassword() {
        String password = null;
        while (password == null) {
            System.out.println("Password: ");
            var ans = sc.nextLine();
            if (!ans.matches(checkPassword)) {
                System.out.println((String.format(INVALID_PASSWORD, USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH)));
            } else {
                password = ans;
            }
        }
        return password;
    }

}

