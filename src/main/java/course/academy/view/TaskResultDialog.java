package course.academy.view;

import course.academy.model.dto.TaskResultDto;

import java.util.Scanner;

import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MIN_LENGTH;
import static course.academy.utils.ParsingHelpers.userInputNumber;

public class TaskResultDialog implements EntityDialog<TaskResultDto> {
    public static Scanner sc = new Scanner(System.in);

    @Override
    public TaskResultDto input() {
        var dto = new TaskResultDto();

        String messageTask = "Enter Task ID: ";
        long taskId = userInputNumber(messageTask);
        dto.setTaskId(taskId);

        String messageEffort = "Enter Actual Effort: ";
        int effort = (int) userInputNumber(messageEffort);
        dto.setActualEffort(effort);

        String ans;
        while (dto.getResultDescription() == null) {
            System.out.println("Enter result description(optional, press 'enter' to skip): ");
            ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            } else {
                if (ans.length() < RESULT_DESCRIPTION_MIN_LENGTH || ans.length() > RESULT_DESCRIPTION_MAX_LENGTH) {
                    System.out.println((String.format("Task result description must be between %d and %d characters.",
                            RESULT_DESCRIPTION_MIN_LENGTH, RESULT_DESCRIPTION_MAX_LENGTH)));
                } else {
                    dto.setResultDescription(ans);
                }
            }
        }
        return dto;
    }

    public long update() {
        String messageUpdate = "Enter TaskResult ID to be updated: ";
        return userInputNumber(messageUpdate);
    }

    public long delete() {
        String messageDelete = "Enter TaskResult ID to be deleted: ";
        return userInputNumber(messageDelete);
    }

    public long printById() {
        String messagePrintId = "Enter TaskResult ID: ";
        return userInputNumber(messagePrintId);
    }

}


