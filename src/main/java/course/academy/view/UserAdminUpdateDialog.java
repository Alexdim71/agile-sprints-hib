package course.academy.view;

import course.academy.model.Role;
import course.academy.model.User;
import course.academy.model.dto.UserManagementDto;

import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import static course.academy.model.User.*;
import static course.academy.utils.ParsingHelpers.userInputNumber;

public class UserAdminUpdateDialog implements EntityDialog<UserManagementDto> {
    public static Scanner sc = new Scanner(System.in);

    private UserManagementDto dto;
    private User user;

    public UserAdminUpdateDialog() {
    }

    public UserAdminUpdateDialog(UserManagementDto dto, User user) {
        this.dto = dto;
        this.user = user;
    }


    public long update() {
        String messageUpdate = "Enter User ID to be updated: ";
        return userInputNumber(messageUpdate);
    }

    @Override
    public UserManagementDto input() {
        int ans1 = 0;
        while (ans1 == 0) {
            System.out.println(String.format("Update first name '%s', press 'enter' to skip): ", user.getFirstName()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                dto.setFirstName(user.getFirstName());
                break;
            }
            if (ans.length() < FIRST_NAME_MIN_LENGTH || ans.length() > FIRST_NAME_MAX_LENGTH) {
                System.out.println((String.format("First name must be with length between %d and %d characters",
                        FIRST_NAME_MIN_LENGTH, FIRST_NAME_MAX_LENGTH)));
            } else {
                dto.setFirstName(ans);
                ans1 = 1;
            }
        }

        int ans2 = 0;
        while (ans2 == 0) {
            System.out.println(String.format("Update last name '%s', press 'enter' to skip): ", dto.getLastName()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                dto.setLastName(user.getLastName());
                break;
            }
            if (ans.length() < LAST_NAME_MIN_LENGTH || ans.length() > LAST_NAME_MAX_LENGTH) {
                System.out.println((String.format("Last name must be with length between %d and %d characters",
                        LAST_NAME_MIN_LENGTH, LAST_NAME_MAX_LENGTH)));
            } else {
                dto.setLastName(ans);
                ans2 = 1;
            }
        }

        int ans3 = 0;
        while (ans3 == 0) {
            System.out.println(String.format("Update email '%s', press 'enter' to skip): ", dto.getEmail()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                dto.setEmail(user.getEmail());
                break;
            }
            if (!ans.matches(checkEmail)) {
                System.out.println("This is not a valid mail address.");
            } else {
                dto.setEmail(ans);
                ans3 = 1;
            }
        }

        int ans4 = 0;
        while (ans4 == 0) {
            System.out.println(String.format("Update contacts '%s', press 'enter' to skip): ", user.getContacts()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                dto.setContacts(user.getContacts());
                break;
            }
            if (ans.length() < CONTACTS_MIN_LENGTH || ans.length() > CONTACTS_MAX_LENGTH) {
                System.out.println((String.format("Field contact must be with length between %d and %d characters",
                        CONTACTS_MIN_LENGTH, CONTACTS_MAX_LENGTH)));
            } else {
                dto.setContacts(ans);
                ans4 = 1;
            }
        }

        int ans5 = 0;
        while (ans5 == 0) {
            System.out.println(String.format("Update User Status '%s' - enter a number (1-ACTIVE, 2-CHANGE_PASSWORD, 3-SUSPENDED, 4-DEACTIVATED), press 'enter' to skip): ",
                    user.getStatusUser().getStatusUser()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                dto.setStatusUserId(user.getStatusUser().getId());
                break;
            } else {
//                StatusUsers status = null;
//                try {
//                    status = StatusUsers.valueOf(ans.toUpperCase());
//                } catch (IllegalArgumentException e) {
//                    System.out.println("Please enter correct type of User Status");
//                    continue;
//                }
                int input = 0;
                try {
                    input = Integer.parseInt(sc.nextLine());
                } catch (NumberFormatException e) {
                    System.out.println("Invalid format - numbers from 1 to 4.");
                    continue;
                }
                if (input < 1 || input > 4) {
                    System.out.println("Invalid format - numbers from 1 to 4.");
                } else {
                    dto.setStatusUserId(input);
                    ans5 = 1;
                }
            }
        }
        int ans6 = 0;
        while (ans6 == 0) {
            System.out.println(String.format("Update User Roles %s - enter a number (1-DEVELOPER, 2-PRODUCT_OWNER, 3-ADMIN - please enter one or more, separated by ','), press 'enter' to skip): ",
                    user.getRoles().stream().map(Role::getName).collect(Collectors.toList())));
            String ans = sc.nextLine();
            Set<Long> roleIds = new HashSet<>();

            if (ans.isEmpty()) {
                for (Role role : user.getRoles()) {
                    roleIds.add(role.getId());
                    dto.setRoleIds(roleIds);
                }
                break;
            }

            var inputString = ans.split(",");
            for (String value : inputString)
                try {
                    long inputNumber = Long.parseLong(value);
                    roleIds.add(inputNumber);
                } catch (IllegalArgumentException e) {
                    System.out.println("Invalid format - numbers from 1 to 3.");
                    continue;
                }
            for (long value : roleIds) {
                if (value < 1 || value > 3) {
                    System.out.println("Invalid format - numbers from 1 to 3.");
                } else {
                    dto.setRoleIds(roleIds);
                    ans6 = 1;
                }
            }
        }
        return dto;
    }
}