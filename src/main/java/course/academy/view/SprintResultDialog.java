package course.academy.view;

import course.academy.model.dto.SprintResultDto;

import java.util.Scanner;

import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MIN_LENGTH;
import static course.academy.utils.ParsingHelpers.userInputNumber;

public class SprintResultDialog implements EntityDialog<SprintResultDto> {
    public static Scanner sc = new Scanner(System.in);

    @Override
    public SprintResultDto input() {
        var dto = new SprintResultDto();

        String messageSprint = "Enter Sprint ID: ";
        long sprintId = userInputNumber(messageSprint);
        dto.setSprintId(sprintId);


        String ans;
        while (dto.getResultsDescription() == null) {
            System.out.println("Enter results description(optional, press 'enter' to skip): ");
            ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            } else {
                if (ans.length() < RESULTS_DESCRIPTION_MIN_LENGTH || ans.length() > RESULTS_DESCRIPTION_MAX_LENGTH) {
                    System.out.println((String.format("Sprint results description must be between %d and %d characters.",
                            RESULTS_DESCRIPTION_MIN_LENGTH, RESULTS_DESCRIPTION_MAX_LENGTH)));
                } else {
                    dto.setResultsDescription(ans);
                }
            }
        }
        return dto;
    }

    public long update() {
        String messageUpdate = "Enter SprintResult ID to be updated: ";
        return userInputNumber(messageUpdate);
    }

    public long delete() {
        String messageDelete = "Enter SprintResult ID to be deleted: ";
        return userInputNumber(messageDelete);
    }

    public long printById() {
        String messagePrintId = "Enter SprintResult ID: ";
        return userInputNumber(messagePrintId);
    }

}


