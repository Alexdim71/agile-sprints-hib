package course.academy.view;

import course.academy.model.Task;
import course.academy.model.dto.TaskUpdateDto;

import java.util.Scanner;

import static course.academy.model.Task.TITLE_MAX_LENGTH;
import static course.academy.model.Task.TITLE_MIN_LENGTH;

public class TaskUpdateDialog {

    public static Scanner sc = new Scanner(System.in);

    public TaskUpdateDialog() {
    }

    public TaskUpdateDto input(Task task) {
        var dto = new TaskUpdateDto();

        while (true) {
            System.out.println("New Kind ID (1-RESEARCH, 2-DESIGN, 3-PROTOTYPING, 4-IMPLEMENTATION, 5-QA, 6-OPERATIONS, 7-BUG_FIXING, 8-DOCUMENTATION, 9-OTHER): ");
            System.out.printf("Current Kind: %s (press 'Enter' to skip)%n", task.getKind().getKind());
            String ans1 = sc.nextLine();
            if (ans1.isEmpty()) {
                dto.setKindId(task.getKind().getId());
                break;
            }
            int input = 0;
            try {
                input = Integer.parseInt(ans1);
            } catch (NumberFormatException e) {
                System.out.println("Invalid format - numbers from 1 to 9.");
                continue;
            }
            if (input < 1 || input > 9) {
                System.out.println("Invalid format - numbers from 1 to 9.");
                continue;
            }
            dto.setKindId(input);
            break;
        }

        while (true) {
            System.out.printf("New Task Title (press 'Enter' to skip), current value: %s%n", task.getTitle());
            var ans2 = sc.nextLine();
            if (ans2.isEmpty()) {
                dto.setTitle(task.getTitle());
                break;
            }
            int length = ans2.length();
            if (length < TITLE_MIN_LENGTH || length > TITLE_MAX_LENGTH) {
                System.out.printf("Task title must be with length between %d and %d characters",
                        TITLE_MIN_LENGTH, TITLE_MAX_LENGTH);
                continue;
            }
            dto.setTitle(ans2);
            break;
        }

        while (true) {
            System.out.printf("New Estimated Effort (press 'Enter' to skip), current value: %d%n", task.getEstimatedEffort());
            String ans3 = sc.nextLine();
            if (ans3.isEmpty()) {
                dto.setEstimatedEffort(task.getEstimatedEffort());
                break;
            }
            int input = 0;
            try {
                input = Integer.parseInt(ans3);
            } catch (NumberFormatException e) {
                System.out.println("Invalid format - positive numbers only.");
                continue;
            }
            if (input <= 0) {
                System.out.println("Invalid format - positive numbers only.");
                continue;
            }
            dto.setEstimatedEffort(input);
            break;
        }


        while (true) {
            System.out.printf("New Tags, separate by ',' (press 'Enter' to skip), current value: %s%n", task.getTags());
            var ans4 = sc.nextLine();
            if (ans4.isEmpty()) {
                dto.setTags(task.getTags());
                break;
            }
            dto.setTags(ans4);
            break;
        }

        while (true) {
            System.out.println("New description (press 'enter' to skip, type 'delete' to delete existing description)");
            String ans5 = sc.nextLine();
            if (ans5.isEmpty()) {
                dto.setDescription(task.getDescription());
                break;
            }
            if (ans5.equalsIgnoreCase("delete")) {
                dto.setDescription(null);
                break;
            }
            if (ans5.length() < Task.DESCRIPTION_MIN_LENGTH || ans5.length() > Task.DESCRIPTION_MAX_LENGTH) {
                System.out.println((String.format("Task description must be between %d and %d characters.",
                        Task.DESCRIPTION_MIN_LENGTH, Task.DESCRIPTION_MAX_LENGTH)));
                continue;
            }
            dto.setDescription(ans5);
            break;
        }

        return dto;
    }

}


