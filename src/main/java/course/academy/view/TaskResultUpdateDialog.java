package course.academy.view;

import course.academy.model.TaskResult;
import course.academy.model.dto.TaskResultUpdateDto;

import java.util.Scanner;

import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MIN_LENGTH;

public class TaskResultUpdateDialog {

    public static Scanner sc = new Scanner(System.in);

    public TaskResultUpdateDialog() {
    }

    public TaskResultUpdateDto input(TaskResult taskResult) {
        var dto = new TaskResultUpdateDto();
        System.out.printf("Task Result for Task Id:%d, Task Title: '%s'%n", taskResult.getTask().getId(), taskResult.getTask().getTitle());

        while (true) {
            System.out.printf("New Actual Effort (press 'Enter' to skip), current value: %d%n", taskResult.getActualEffort());
            String ans1 = sc.nextLine();
            if (ans1.isEmpty()) {
                dto.setActualEffort(taskResult.getActualEffort());
                break;
            }
            int input = 0;
            try {
                input = Integer.parseInt(ans1);
            } catch (NumberFormatException e) {
                System.out.println("Invalid format - positive numbers only.");
                continue;
            }
            if (input <= 0) {
                System.out.println("Invalid format - positive numbers only.");
                continue;
            }
            dto.setActualEffort(input);
            break;
        }

        while (true) {
            System.out.println("New Result description (press 'enter' to skip, type 'delete' to delete existing description)");
            String ans2 = sc.nextLine();
            if (ans2.isEmpty()) {
                dto.setResultDescription(dto.getResultDescription());
                break;
            }
            if (ans2.equalsIgnoreCase("delete")) {
                dto.setResultDescription(null);
                break;
            }
            if (ans2.length() < RESULT_DESCRIPTION_MIN_LENGTH|| ans2.length() > RESULT_DESCRIPTION_MAX_LENGTH) {
                System.out.println((String.format("Result description must be between %d and %d characters.",
                        RESULT_DESCRIPTION_MIN_LENGTH, RESULT_DESCRIPTION_MAX_LENGTH)));
                continue;
            }
            dto.setResultDescription(ans2);
            break;
        }

        return dto;
    }

}


