package course.academy.view;


import course.academy.model.dto.ProjectResultDto;
import course.academy.utils.ParsingHelpers;

import java.time.LocalDate;
import java.util.Scanner;

import static course.academy.model.ProjectResult.RESULTS_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.ProjectResult.RESULTS_DESCRIPTION_MIN_LENGTH;
import static course.academy.utils.ParsingHelpers.userInputNumber;

public class ProjectResultDialog implements EntityDialog<ProjectResultDto> {
    public static Scanner sc = new Scanner(System.in);

    @Override
    public ProjectResultDto input() {
        var dto = new ProjectResultDto();

        String messageProject = "Enter Project ID: ";
        long idProject = userInputNumber(messageProject);
        dto.setProjectId(idProject);

        while (dto.getEndDate() == null) {
            System.out.println("Enter End date in format dd.mm.yyyy: ");
            String ans;
            ans = sc.nextLine();
            LocalDate endDate = null;
            try {
                endDate = LocalDate.parse(ans, ParsingHelpers.df);
            } catch (Exception e) {
                System.out.println("Error: Invalid date - try again in the specified format.");
            }
            dto.setEndDate(endDate);
        }

        String ans;
        while (dto.getResultsDescription() == null) {
            System.out.println("Enter description(optional, press 'enter' to skip): ");
            ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            } else {
                if (ans.length() < RESULTS_DESCRIPTION_MIN_LENGTH || ans.length() > RESULTS_DESCRIPTION_MAX_LENGTH) {
                    System.out.println((String.format("Project description must be between %d and %d characters.",
                            RESULTS_DESCRIPTION_MIN_LENGTH, RESULTS_DESCRIPTION_MAX_LENGTH)));
                } else {
                    dto.setResultsDescription(ans);
                }
            }
        }
        return dto;
    }

    public long update() {
        String messageUpdate = "Enter ProjectResult ID to be updated: ";
        return userInputNumber(messageUpdate);
    }

    public long delete() {
        String messageDelete = "Enter ProjectResult ID to be deleted: ";
        return userInputNumber(messageDelete);
    }

    public long printById() {
        String messagePrintId = "Enter ProjectResult ID: ";
        return userInputNumber(messagePrintId);
    }

}


