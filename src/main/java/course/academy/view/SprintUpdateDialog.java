package course.academy.view;

import course.academy.model.Project;
import course.academy.model.Sprint;
import course.academy.model.dto.SprintUpdateDto;
import course.academy.utils.ParsingHelpers;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;


import static course.academy.utils.ParsingHelpers.df;

public class SprintUpdateDialog {

    public static Scanner sc = new Scanner(System.in);

    public SprintUpdateDialog() {
    }

    public SprintUpdateDto input(Sprint sprint) {
        var dto = new SprintUpdateDto();
        Project project = sprint.getProject();


        while (true) {
            System.out.printf("New start date in format dd.mm.yyyy (press 'Enter' to skip), currentDate = %s%n",
                    sprint.getStartDate().format(df));
            String ans1 = sc.nextLine();
            if (ans1.isEmpty()) {
                dto.setStartDate(sprint.getStartDate());
                break;
            }
            LocalDate startDate = null;
            try {
                startDate = LocalDate.parse(ans1, ParsingHelpers.df);
            } catch (Exception e) {
                System.out.println("Error: Invalid date - try again in the specified format.");
                continue;
            }
            dto.setStartDate(startDate);
            break;
        }

        while (true) {
            System.out.printf("New Duration (press 'Enter' to skip), current value: %s%n", sprint.getDuration());
            var ans2 = sc.nextLine();
            if (ans2.isEmpty()) {
                dto.setDuration(sprint.getDuration());
                break;
            }
            int input = 0;
            try {
                input = Integer.parseInt(ans2);
            } catch (NumberFormatException e) {
                System.out.println("Invalid format - positive numbers only.");
                continue;
            }
            if (input <= 0) {
                System.out.println("Invalid format - positive numbers only.");
                continue;
            }
            dto.setDuration(input);
            break;
        }

//        while (true) {
//            StringBuilder sb = new StringBuilder();
//            sb.append("Enter developer Ids (press 'Enter' to skip), separated by ',' current Ids: ");
//            sprint.getDevelopers().forEach(dev -> sb.append(dev.getId()).append(","));
//            sb.deleteCharAt(sb.length()-1);
//            System.out.println(sb);
//            System.out.printf("Choose from assigned developers to Project Id %d:%n", project.getId());
//            project.getDevelopers().forEach(dev -> System.out.printf("Id: %d, username: %s%n", dev.getId(), dev.getUsername()));
//            var ans3 = sc.nextLine();
//            if (ans3.isEmpty()) {
//                sprint.getDevelopers().forEach(dev -> dto.getDeveloperIds().add(dev.getId()));
//                break;
//            }
//            var input = ans3.split(",");
//            long devId = 0;
//            Set<Long> developers = new HashSet<>();
//            try {
//                for (String devString: input) {
//                    devId = Long.parseLong(devString);
//                    developers.add(devId);
//                }
//            } catch (NumberFormatException e) {
//                System.out.println("Please enter only positive numbers");
//                continue;
//            }
//            dto.setDeveloperIds(developers);
//            break;
//        }
        return dto;
    }

}


