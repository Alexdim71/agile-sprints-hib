package course.academy.view;

import course.academy.model.ProjectResult;
import course.academy.model.dto.ProjectResultUpdateDto;
import course.academy.utils.ParsingHelpers;

import java.time.LocalDate;
import java.util.Scanner;

import static course.academy.model.ProjectResult.RESULTS_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.ProjectResult.RESULTS_DESCRIPTION_MIN_LENGTH;
import static course.academy.utils.ParsingHelpers.df;

public class ProjectResultUpdateDialog {

    public static Scanner sc = new Scanner(System.in);

    public ProjectResultUpdateDialog() {
    }

    public ProjectResultUpdateDto input(ProjectResult projectResult) {
        var dto = new ProjectResultUpdateDto();
        System.out.printf("Project Result for Project Id:%d, Project Title: '%s'%n", projectResult.getProject().getId(), projectResult.getProject().getTitle());

        while (true) {
            System.out.printf("New end date in format dd.mm.yyyy (press 'Enter' to skip), currentDate = %s%n",
                    projectResult.getEndDate().format(df));
            String ans1 = sc.nextLine();
            if (ans1.isEmpty()) {
                dto.setEndDate(projectResult.getEndDate());
                break;
            }
            LocalDate endDate = null;
            try {
                endDate = LocalDate.parse(ans1, ParsingHelpers.df);
            } catch (Exception e) {
                System.out.println("Error: Invalid date - try again in the specified format.");
                continue;
            }
            dto.setEndDate(endDate);
            break;
        }

        while (true) {
            System.out.println("New Results Description (press 'enter' to skip, type 'delete' to delete existing description)");
            String ans2 = sc.nextLine();
            if (ans2.isEmpty()) {
                dto.setResultsDescription(dto.getResultsDescription());
                break;
            }
            if (ans2.equalsIgnoreCase("delete")) {
                dto.setResultsDescription(null);
                break;
            }
            if (ans2.length() < RESULTS_DESCRIPTION_MIN_LENGTH|| ans2.length() > RESULTS_DESCRIPTION_MAX_LENGTH) {
                System.out.println((String.format("Result description must be between %d and %d characters.",
                        RESULTS_DESCRIPTION_MIN_LENGTH, RESULTS_DESCRIPTION_MAX_LENGTH)));
                continue;
            }
            dto.setResultsDescription(ans2);
            break;
        }

        return dto;
    }

}


