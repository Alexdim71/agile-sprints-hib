package course.academy.view;

import course.academy.model.StatusTasks;
import course.academy.model.Task;
import course.academy.model.dto.TaskDto;

import java.util.Scanner;

import static course.academy.model.Task.TITLE_MAX_LENGTH;
import static course.academy.model.Task.TITLE_MIN_LENGTH;
import static course.academy.utils.ParsingHelpers.userInputNumber;

public class TaskDialog implements EntityDialog<TaskDto> {
    public static Scanner sc = new Scanner(System.in);

    @Override
    public TaskDto input() {
        var dto = new TaskDto();

        String messageProject = "Enter Project ID: ";
        long idProject = userInputNumber(messageProject);
        dto.setProjectId(idProject);

        while (true) {
            System.out.println("Enter Kind ID (1-RESEARCH, 2-DESIGN, 3-PROTOTYPING, 4-IMPLEMENTATION, 5-QA, 6-OPERATIONS, 7-BUG_FIXING, 8-DOCUMENTATION, 9-OTHER): ");
            long input = 0;
            try {
                input = Integer.parseInt(sc.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Invalid format - numbers from 1 to 9.");
                continue;
            }
            if (input < 1 || input > 9) {
                System.out.println("Invalid format - numbers from 1 to 9.");
                continue;
            }
            dto.setKindId(input);
            break;
        }


        String messageEffort = "Enter Estimated Effort: ";
        int effort = (int) userInputNumber(messageEffort);
        dto.setEstimatedEffort(effort);

        while (dto.getTitle() == null) {
            System.out.println("Task title: ");
            var ans = sc.nextLine();
            int length = ans.length();
            if (length < TITLE_MIN_LENGTH || length > TITLE_MAX_LENGTH) {
                System.out.println((String.format("Task title must be with length between %d and %d characters",
                        TITLE_MIN_LENGTH, TITLE_MAX_LENGTH)));
            } else {
                dto.setTitle(ans);
            }
        }

        while (dto.getTags() == null) {
            System.out.println("Tags, separate by ',': ");
            var ans = sc.nextLine();
            dto.setTags(ans);
        }

        String ans;
        while (dto.getDescription() == null) {
            System.out.println("Enter description(optional, press 'enter' to skip): ");
            ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            } else {
                if (ans.length() < Task.DESCRIPTION_MIN_LENGTH || ans.length() > Task.DESCRIPTION_MAX_LENGTH) {
                    System.out.println((String.format("Task description must be between %d and %d characters.",
                            Task.DESCRIPTION_MIN_LENGTH, Task.DESCRIPTION_MAX_LENGTH)));
                } else {
                    dto.setDescription(ans);
                }
            }
        }
        return dto;
    }

    public long update() {
        String messageUpdate = "Enter Task ID to be updated: ";
        return userInputNumber(messageUpdate);
    }

    public long delete() {
        String messageDelete = "Enter Task ID to be deleted: ";
        return userInputNumber(messageDelete);
    }

    public long printById() {
        String messagePrintId = "Enter Task ID: ";
        return userInputNumber(messagePrintId);
    }

    public StatusTasks enterStatusTask() {
        int ans = 0;
        StatusTasks status = null;
        while (ans == 0) {
            System.out.println("Update Task Status (PLANNED, ACTIVE, COMPLETED)");
            String input = sc.nextLine();
            try {
                status = StatusTasks.valueOf(input.toUpperCase());
            } catch (IllegalArgumentException e) {
                System.out.println("Please enter correct type of User Status");
                continue;
            }
            ans = 1;
        }
        return status;
    }

    public String inputSearchString() {
        System.out.println("Task search word (if empty, all tasks are printed): ");
        return sc.nextLine();
    }

}


