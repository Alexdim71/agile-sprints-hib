package course.academy.view;

import course.academy.exception.NonexistingEntityException;
import course.academy.model.Project;
import course.academy.model.dto.SprintDto;
import course.academy.service.ProjectService;
import course.academy.utils.ParsingHelpers;

import java.time.LocalDate;
import java.util.*;

import static course.academy.utils.ParsingHelpers.userInputNumber;

public class SprintDialog {
    public static Scanner sc = new Scanner(System.in);

    private final ProjectService projectService;

    public SprintDialog(ProjectService projectService) {
        this.projectService = projectService;
    }

    public SprintDto input() {
        var dto = new SprintDto();

        String messageProject = "Enter Project ID: ";
        long idProject = userInputNumber(messageProject);
        dto.setProjectId(idProject);
        Project project = null;
        try {
            project = projectService.getById(idProject);
        } catch (NonexistingEntityException e) {
            System.out.println(e.getMessage());
        }

        while (dto.getStartDate() == null) {
            System.out.println("Start date in format dd.mm.yyyy: ");
            String ans;
            ans = sc.nextLine();
            LocalDate startDate = null;
            try {
                startDate = LocalDate.parse(ans, ParsingHelpers.df);
            } catch (Exception e) {
                System.out.println("Error: Invalid date - try again in the specified format.");
            }
            dto.setStartDate(startDate);
        }

        String messageDuration = "Enter duration";
        int duration = (int) userInputNumber(messageDuration);
        dto.setDuration(duration);

        String ans;
        while (dto.getDeveloperIds() == null) {
            System.out.println("Enter developer IDs (positive integers), separated by ',' (optional, press 'enter' to skip and add later), choose from:");
            project.getDevelopers().forEach(dev -> System.out.printf("Id: %d, username: %s%n", dev.getId(), dev.getUsername()));
            ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            }
            Set<Long> developers = new HashSet<>();
            var input = ans.split(",");
            long devInt = 0;
            try {
                for (String devString: input) {
                        devInt = Long.parseLong(devString);
                        developers.add(devInt);
                    }
            } catch (NumberFormatException e) {
                System.out.println("Please enter only positive numbers");
                continue;
            }
            dto.setDeveloperIds(developers);
        }
        return dto;
    }


    public long update() {
        String messageUpdate = "Enter Sprint ID to be updated: ";
        return userInputNumber(messageUpdate);
    }

    public long delete() {
        String messageDelete = "Enter Sprint ID to be deleted: ";
        return userInputNumber(messageDelete);
    }

    public long printById() {
        String messagePrintId = "Enter Sprint ID: ";
        return userInputNumber(messagePrintId);
    }

    public long enterSprint() {
        String messageRemoveDeveloper = "Enter Sprint ID: ";
        return userInputNumber(messageRemoveDeveloper);
    }

    public long addTaskMessage() {
        String messageAddTask = "Enter Task ID to be added: ";
        return userInputNumber(messageAddTask);
    }

    public long removeTaskMessage() {
        String messageRemoveTask = "Enter Task ID to be removed: ";
        return userInputNumber(messageRemoveTask);
    }

    public long addDeveloperMessage() {
        String messageAddDeveloper = "Enter Developer ID to be added: ";
        return userInputNumber(messageAddDeveloper);
    }

    public long removeDeveloperMessage() {
        String messageRemoveDeveloper = "Enter Developer ID to be removed: ";
        return userInputNumber(messageRemoveDeveloper);
    }

}


