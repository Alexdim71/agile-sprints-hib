package course.academy.view;

import course.academy.model.dto.UserManagementDto;

public interface EntityDialog<E> {
    E input();
}
