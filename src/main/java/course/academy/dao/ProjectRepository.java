package course.academy.dao;

import course.academy.model.Project;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

public interface ProjectRepository extends CRUDRepository<Project> {

    Collection<Project> filterProjects (Optional<String> title, Optional<Date> startDate,
                                        Optional<Integer> ownerId, Optional<String> sortBy);
}
