package course.academy.dao;

import course.academy.model.StatusTask;

public interface StatusTaskRepository extends CRUDRepository <StatusTask> {
}
