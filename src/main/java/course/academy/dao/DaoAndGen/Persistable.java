package course.academy.dao.DaoAndGen;

public interface Persistable {
    void load();

    void save();

}
