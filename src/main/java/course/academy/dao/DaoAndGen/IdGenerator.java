package course.academy.dao.DaoAndGen;

public interface IdGenerator<K> {
    K getNextId();
    K getCurrentId();
    void reset(K newInitialValue);
}
