package course.academy.dao.DaoAndGen;

import course.academy.dao.*;
import org.hibernate.SessionFactory;

public interface DaoFactory {

    UserRepository createUserRepository();
    UserRepository createUserRepositoryJdbc(SessionFactory sessionFactory);
    TaskRepository createTaskRepository();
    TaskResultRepository createTaskResultRepository();
    SprintRepository createSprintRepository();
    SprintResultRepository createSprintResultRepository();
    ProjectRepository createProjectRepository();
    ProjectResultRepository createProjectResultRepository();

    TaskRepository createTaskFileRepository(String dbFileName);

}
