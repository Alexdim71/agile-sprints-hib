package course.academy.dao;

import course.academy.model.TaskResult;

public interface TaskResultRepository extends CRUDRepository<TaskResult> {
}
