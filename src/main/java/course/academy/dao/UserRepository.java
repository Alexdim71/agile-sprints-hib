package course.academy.dao;


import course.academy.model.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CRUDRepository<User> {

    List<User> searchByName(String name);

}
