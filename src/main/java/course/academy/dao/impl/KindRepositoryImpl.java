package course.academy.dao.impl;


import course.academy.dao.KindRepository;
import course.academy.model.Kind;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class KindRepositoryImpl extends AbstractCRUDRepository<Kind> implements KindRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public KindRepositoryImpl(SessionFactory sessionFactory) {
        super(Kind.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

}