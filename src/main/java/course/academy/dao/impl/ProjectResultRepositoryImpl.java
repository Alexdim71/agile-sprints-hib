package course.academy.dao.impl;



import course.academy.dao.ProjectResultRepository;
import course.academy.model.ProjectResult;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class ProjectResultRepositoryImpl extends AbstractCRUDRepository<ProjectResult> implements ProjectResultRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ProjectResultRepositoryImpl(SessionFactory sessionFactory) {
        super(ProjectResult.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

}