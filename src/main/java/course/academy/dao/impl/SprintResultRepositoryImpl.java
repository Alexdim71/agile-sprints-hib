package course.academy.dao.impl;


import course.academy.dao.SprintResultRepository;
import course.academy.model.SprintResult;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class SprintResultRepositoryImpl extends AbstractCRUDRepository<SprintResult> implements SprintResultRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public SprintResultRepositoryImpl(SessionFactory sessionFactory) {
        super(SprintResult.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

}