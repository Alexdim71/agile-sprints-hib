package course.academy.dao.impl;


import course.academy.dao.ProjectRepository;

import course.academy.exception.InvalidEntityDataException;
import course.academy.model.Project;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;


@Repository
public class ProjectRepositoryImpl extends AbstractCRUDRepository<Project> implements ProjectRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ProjectRepositoryImpl(SessionFactory sessionFactory) {
        super(Project.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Collection<Project> filterProjects(Optional<String> title, Optional<Date> startDate,
                                              Optional<Integer> ownerId, Optional<String> sortBy) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Project");
            queryString.append("from Project");
            List<String> params = new ArrayList<>();
            Map<String, Object> props = new HashMap<>();

            title.ifPresent(value -> {
                params.add(" title like :title");
                props.put("title", "%" + value + "%");
            });

            startDate.ifPresent(value -> {
                params.add(" startDate >= :startDate");
                props.put("startDate", value);
            });

            ownerId.ifPresent(value -> {
                params.add(" owner.id >= :ownerId");
                props.put("ownerId", value);
            });

            if (!params.isEmpty()) {
                queryString.append(" where").append(String.join(" and", params));
            }

            sortBy.ifPresent(value -> generateSortedString(value));

            if (params.isEmpty() && sortBy.isEmpty()) {
                throw new InvalidEntityDataException("Please enter at least one filter or sort parameter");
            }

            Query<Project> query = session.createQuery(queryString.toString(), Project.class);
            query.setProperties(props);
            return query.list();
        }
    }

    private String generateSortedString(String value) {
        StringBuilder sb = new StringBuilder(" order by");
        var sort = value.split("_");
        switch (sort[0]) {
            case "title":
                sb.append("title");
                break;
            case "startDate":
                sb.append(" startDate");
                break;
            case "ownerId":
                sb.append("owner.id");
                break;
            default:
                return "";
        }
        return sb.toString();
    }

    }