package course.academy.dao.impl;

import course.academy.dao.TaskResultRepository;
import course.academy.model.TaskResult;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class TaskResultRepositoryImpl extends AbstractCRUDRepository<TaskResult> implements TaskResultRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public TaskResultRepositoryImpl(SessionFactory sessionFactory) {
        super(TaskResult.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

}