package course.academy.dao.impl;


import course.academy.dao.UserRepository;
import course.academy.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends AbstractCRUDRepository<User> implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> searchByName(String name) {

        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username like :search or " +
                    "email like :search or firstName like :search or lastName like :search", User.class);
            query.setParameter("search", "%" + name + "%");
            return query.list();
        }

    }
}
