package course.academy.dao.impl;


import course.academy.dao.SprintRepository;
import course.academy.model.Sprint;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class SprintRepositoryImpl extends AbstractCRUDRepository<Sprint> implements SprintRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public SprintRepositoryImpl(SessionFactory sessionFactory) {
        super(Sprint.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

}