package course.academy.dao.impl;


import course.academy.dao.StatusUserRepository;
import course.academy.model.StatusUser;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class StatusUserRepositoryImpl extends AbstractCRUDRepository<StatusUser> implements StatusUserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public StatusUserRepositoryImpl(SessionFactory sessionFactory) {
        super(StatusUser.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

}