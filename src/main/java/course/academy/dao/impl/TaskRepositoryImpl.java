package course.academy.dao.impl;


import course.academy.dao.TaskRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.model.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;


@Repository
public class TaskRepositoryImpl extends AbstractCRUDRepository<Task> implements TaskRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public TaskRepositoryImpl(SessionFactory sessionFactory) {
        super(Task.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Task> filterTasks(Optional<String> title, Optional<Long> projectId, Optional<String> kind,
                                  Optional<String> status, Optional<Integer> minEstimatedEffort, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {

            StringBuilder queryString = new StringBuilder();
            queryString.append("from Task");
            List<String> params = new ArrayList<>();
            Map<String, Object> props = new HashMap<>();

            title.ifPresent(value -> {
                params.add(" title like :title");
                props.put("title", "%" + value + "%");
            });

            projectId.ifPresent(value -> {
                params.add(" project.id = :id");
                props.put("id", value);
            });

            kind.ifPresent(value -> {
                params.add(" kind.kind = :kind");
                props.put("kind", value);
            });

            status.ifPresent(value -> {
                params.add(" statusTask.statusTask like :status");
                props.put("status", value);
            });

            minEstimatedEffort.ifPresent(value -> {
                params.add(" estimatedEffort >= :effort");
                props.put("effort", value);
            });

            if (!params.isEmpty()) {
                queryString.append(" where").append(String.join(" and", params));
            }

            sort.ifPresent(value -> queryString.append(generateSortString(value)));

            if (params.isEmpty() && sort.isEmpty()) {
                throw new InvalidEntityDataException("Please enter at least one filter or sort parameter");
            }

            Query<Task> query = session.createQuery(queryString.toString(), Task.class);
            query.setProperties(props);
            return query.list();
        }
    }

    private String generateSortString(String value) {
        StringBuilder sb = new StringBuilder();
        sb.append(" order by");
        String[] sort = value.split("_");
        switch (sort[0]) {
            case "title":
                sb.append(" title");
                break;
            case "projectId":
                sb.append(" project.id");
                break;
            case "estimatedEffort":
                sb.append(" estimatedEffort");
                break;
            default: return "";
        }
        if (sort.length > 1 && sort[1].equals("desc")) {
            sb.append(" desc");
        }
        return sb.toString();
    }


}