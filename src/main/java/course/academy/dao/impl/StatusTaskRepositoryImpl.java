package course.academy.dao.impl;


import course.academy.dao.StatusTaskRepository;
import course.academy.model.StatusTask;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class StatusTaskRepositoryImpl extends AbstractCRUDRepository<StatusTask> implements StatusTaskRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public StatusTaskRepositoryImpl(SessionFactory sessionFactory) {
        super(StatusTask.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

}