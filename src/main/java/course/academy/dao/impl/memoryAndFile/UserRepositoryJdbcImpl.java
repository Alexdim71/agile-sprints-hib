//package course.academy.dao.impl.memoryAndFile;
//
//import course.academy.dao.UserRepository;
//import course.academy.exception.EntityPersistenceException;
//import course.academy.exception.NonexistingEntityException;
//import course.academy.model.User;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.List;
//
//import static course.academy.AgileApplication.sessionFactory;
//
//
//public class UserRepositoryJdbcImpl implements UserRepository {
//    public static final String SELECT_ALL =
//            "select * from users;";
//    public static final String INSERT_NEW_USER =
//            "insert into `users` (first_name, last_name, email, username, password, contacts) values (?, ?, ?, ?, ?, ?);";
//    public static final String FIND_BY_ID =
//            "select * from users where id = ?;";
//
//    @Autowired
//    public UserRepositoryJdbcImpl(SessionFactory sessionFactory) {
//    }
//
//
//    @Override
//    public Collection<User> findAll() {
//
//        try (Session session = sessionFactory.openSession()) {
//            Query<User> query = session.createQuery("from users", User.class);
//            return query.list();
//        }
//    }
//
////        try (var stmt = connection.prepareStatement(SELECT_ALL)) {
////            var rs = stmt.executeQuery();
////            return toUsers(rs);
////        } catch (SQLException ex) {
//////            log.error("Error creating connection to DB", ex);
////            throw new EntityPersistenceException("Error executing SQL query: " + SELECT_ALL, ex);
////        }
////    }
//
//    @Override
//    public User findById(Long id) {
//        return null;
//    }
//
//    @Override
//    public User create(User entity) {
//        return null;
//    }
//
//    @Override
//    public User update(User entity) throws NonexistingEntityException {
//        return null;
//    }
//
//    @Override
//    public User deleteById(Long id) throws NonexistingEntityException {
//        return null;
//    }
//
//    @Override
//    public long count() {
//        return 0;
//    }
////
////    @Override
////    public User findById(Long id) {
////
////        try (var stmt = connection.prepareStatement(FIND_BY_ID)) {
////            stmt.setLong(1,id);
////            var rs = stmt.executeQuery();
////            return toUsers(rs).get(0);
////        } catch (SQLException ex) {
//////            log.error("Error creating connection to DB", ex);
////            throw new EntityPersistenceException("Error executing SQL query: " + SELECT_ALL, ex);
////        }
////    }
////
////
////    @Override
////    public User create(User entity) {
////        try (var stmt = connection.prepareStatement(INSERT_NEW_USER, Statement.RETURN_GENERATED_KEYS)) {
////
////            stmt.setString(1, entity.getFirstName());
////            stmt.setString(2, entity.getLastName());
////            stmt.setString(3, entity.getEmail());
////            stmt.setString(4, entity.getUsername());
////            stmt.setString(5, entity.getPassword());
//////            stmt.setString(6, entity.getRoles().toString());
////            stmt.setString(6, entity.getContacts());
//////            stmt.setString(8, entity.getStatusUser().toString());
//////            stmt.setString(9, entity.getCreated().toString());
//////            stmt.setString(10, entity.getModified().toString());
////
////            connection.setAutoCommit(false);
////            var affectedRows = stmt.executeUpdate();
////            // more updates here ...
////            connection.commit();
////            connection.setAutoCommit(true);
////
////            // 6. Check results and Get generated primary ke
////            if (affectedRows == 0) {
////                throw new EntityPersistenceException("Creating user failed, no rows affected.");
////            }
////            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
////                if (generatedKeys.next()) {
////                    entity.setId(generatedKeys.getLong(1));
////                    return entity;
////                } else {
////                    throw new EntityPersistenceException("Creating user failed, no ID obtained.");
////                }
////            }
////        } catch (SQLException ex) {
////            try {
////                connection.rollback();
////            } catch (SQLException e) {
////                throw new EntityPersistenceException("Error rolling back SQL query: " + INSERT_NEW_USER, ex);
////            }
//////            log.error("Error creating connection to DB", ex);
////            throw new EntityPersistenceException("Error executing SQL query: " + INSERT_NEW_USER, ex);
////        }
////    }
////
////
////    @Override
////    public User update(User entity) throws NonexistingEntityException {
////        return null;
////    }
////
////    @Override
////    public User deleteById(Long id) throws NonexistingEntityException {
////        return null;
////    }
////
////    @Override
////    public long count() {
////        return findAll().size();
////    }
////
////    public List<User> toUsers(ResultSet rs) throws SQLException {
////        List<User> results = new ArrayList<>();
////        while (rs.next()) {
////            results.add(new User(
////                    rs.getLong(1),
////                    rs.getString("first_name"),
////                    rs.getString("last_name"),
////                    rs.getString("email"),
////                    rs.getString("username"),
////                    rs.getString("password")
////            ));
////        }
////        return results;
////    }
//}
