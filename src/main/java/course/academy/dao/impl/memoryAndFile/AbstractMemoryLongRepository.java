package course.academy.dao.impl.memoryAndFile;

import course.academy.dao.DaoAndGen.Repository;
import course.academy.exception.NonexistingEntityException;
import course.academy.model.Identifiable;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class AbstractMemoryLongRepository<V extends Identifiable<Long>> implements Repository<Long, V> {
    private long nextId = 0;
    Map<Long, V> entities = new HashMap<>();

    @Override
    public Collection<V> findAll() {
        return entities.values();
    }

    @Override
    public V findById(Long id) {
        return entities.get(id);
    }

    @Override
    public V create(V entity) {
        entity.setId(++nextId);
        entities.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public V update(V entity) throws NonexistingEntityException {
        V old = findById(entity.getId());
        if (old == null) {
            throw new NonexistingEntityException(String.format("%s with id=%d does not exist",
                    entity.getClass().getSimpleName(), entity.getId()));
        }
        entities.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public V deleteById(Long id) throws NonexistingEntityException {
        V entity = findById(id);
        if (entity == null) {
            throw new NonexistingEntityException(String.format("%s with id=%d does not exist",
                    entity.getClass().getSimpleName(), entity.getId()));
        }
        entities.remove(id);
        return entity;
    }

    @Override
    public long count() {
        return entities.size();
    }
}

