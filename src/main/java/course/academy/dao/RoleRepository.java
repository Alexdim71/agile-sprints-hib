package course.academy.dao;

import course.academy.model.Role;

public interface RoleRepository extends CRUDRepository <Role> {
}
