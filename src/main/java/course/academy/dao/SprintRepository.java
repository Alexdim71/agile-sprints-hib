package course.academy.dao;

import course.academy.model.Sprint;

public interface SprintRepository extends CRUDRepository<Sprint> {
}
