package course.academy.dao;

import course.academy.dao.DaoAndGen.PersistableRepository;
import course.academy.model.Task;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Optional;

public interface TaskRepository extends CRUDRepository<Task> {

    List<Task> filterTasks (Optional<String> title, Optional<Long> projectId, Optional<String> kind,
                            Optional<String> status, Optional<Integer> minEstimatedEffort, Optional<String> sort);

}
