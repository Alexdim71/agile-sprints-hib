package course.academy.dao;

import course.academy.model.Kind;

public interface KindRepository extends CRUDRepository <Kind> {
}
