package course.academy.dao;


import course.academy.model.StatusUser;

public interface StatusUserRepository extends CRUDRepository <StatusUser> {
}
