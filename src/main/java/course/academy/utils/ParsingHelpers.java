package course.academy.utils;

import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class ParsingHelpers {

    public static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
    public static DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");


    public static long userInputNumber(String message) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println(message);
            long ans = 0;
            try {
                ans = Long.parseLong(sc.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Invalid format - positive numbers only.");
                continue;
            }
            if (ans <= 0) {
                System.out.println("Invalid format - positive numbers only.");
                continue;
            }
            return ans;
        }
    }
}
