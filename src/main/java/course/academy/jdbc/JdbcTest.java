package course.academy.jdbc;


//import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

//@Slf4j
public class JdbcTest {
    public static final String SELECT_USERS =
            "select * from `users`;";

    public void run() throws IOException, ClassNotFoundException, SQLException {
        Properties props = new Properties();
        String dbConfigPath = JdbcTest.class.getClassLoader()
                .getResource("jdbc.properties").getPath();
        props.load(new FileInputStream(dbConfigPath));
//        log.info("jdbc.properties: {}", props);

        // 1. Load DB Driver
        try {
            Class.forName(props.getProperty("driver"));
        } catch (ClassNotFoundException ex) {
//            log.error("DB driver class not found", ex);
            throw ex;
        }
//        log.info("DB driver loaded successfully: {}", props.getProperty("driver"));

    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {
        var demo = new JdbcTest();
        demo.run();
    }

}